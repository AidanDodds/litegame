#include <lg_litegame.h>

#define _SDL_main_h
#include <SDL/SDL.h>

#include <math.h>

const float pi2 = 6.28318530718f;

#pragma comment( lib, "SDL.lib" )

static SDL_Surface *sdl_surf = NULL;
static lg_cCanvas screen;
static lg_sMap2d  map;

static SDL_Surface *texture = NULL;

struct sPlayer
{
	float x, y;
	float angle;
};
sPlayer player;

void app_init( void )
{
	if ( SDL_Init( SDL_INIT_VIDEO ) != 0 )
		exit( -1 );
	sdl_surf = SDL_SetVideoMode( 640, 480, 32, 0 );
	if ( sdl_surf == NULL )
		exit( -2 );
	if (! lg_canvas::create( screen, 320, 240 ) )
		exit( -3 );
}

bool app_tick( void )
{
	static bool active = true;

	lg_canvas::blitx2( screen, (u32*)sdl_surf->pixels, 640, 480 );
	screen.rect( 0,   0, 320, 120, 0x1f1f1f );
	screen.rect( 0, 120, 320, 120, 0x3b3b3b );
	SDL_Flip( sdl_surf );

	SDL_Event event;
	while ( SDL_PollEvent( &event ) )
	{
		if ( event.type == SDL_QUIT )
			active = false;

		if ( event.type == SDL_MOUSEBUTTONDOWN )
		{
			float x = player.x / 8;
			float y = player.y / 8;
			float nx = sinf( player.angle );
			float ny = cosf( player.angle );
			lg_map2d::setWall( map, x+nx*5, y+ny*5, true );
			lg_map2d::preprocess( map );
		}

		if ( event.type == SDL_KEYDOWN )
		{
			switch ( event.key.keysym.sym )
			{
			case ( SDLK_ESCAPE ):
			case ( SDLK_q ):
				active = false;
				break;
			}
		}
	}

	return active;
}

void drawMap( void )
{
	for ( uint32 i=0; i<map.w*map.h; i++ )
	{
		uint32 x = i % map.w;
		uint32 y = i / map.w;
		
		int col = 0x3585a;

		if ( lg_map2d::isWall( map, x, y ) )
			screen.plot( x, y, col );
		else
			screen.plot( x, y, 0x0 );
	}

	{
		float x = player.x / 8;
		float y = player.y / 8;

		float nx = sinf( player.angle );
		float ny = cosf( player.angle );

		screen.plot( x, y, 0xff00 );
//		screen.circle( x, y, 3, 0x00FF00 );
//		screen.line  ( x, y, x+nx*4, y+ny*4, 0x00FF00 );
	}
}

void mapBuild( void )
{
	int ts = 8;

	lg_map2d::create( map, 32, 32, ts );
	for ( int i=0; i<map.w; i++ )
	{
		lg_map2d::setWall( map, i,       0, true );
		lg_map2d::setWall( map, i, map.h-1, true );
	}

	for ( int i=0; i<map.h; i++ )
	{
		lg_map2d::setWall( map,       0, i, true );
		lg_map2d::setWall( map, map.w-1, i, true );
	}

	for ( int i=0; i<100; i++ )
	{
		lg_map2d::setWall( map, rand()%map.w, rand()%map.h, true );
	}

	lg_map2d::preprocess( map );
}

void updateLogic( void )
{
	float  speed = 0.7f;
	float rspeed = 0.07f;

	int nKeys = 0;
	Uint8 *keys = SDL_GetKeyState( &nKeys );
	
	float nx = sinf( player.angle ) * speed;
	float ny = cosf( player.angle ) * speed;

	if ( keys[ SDLK_w ] )
	{
		player.x += nx;
		player.y += ny;
	}
	if ( keys[ SDLK_s ] )
	{
		player.x -= nx;
		player.y -= ny;
	}
	if ( keys[ SDLK_a ] )
	{
		player.x += ny * 0.6f;
		player.y -= nx * 0.6f;
	}
	if ( keys[ SDLK_d ] )
	{
		player.x -= ny * 0.6f;
		player.y += nx * 0.6f;
	}

	float x = player.x;
	float y = player.y;

	lg_map2d::aabb_t aabb = { x-2.0f, y-2.0f, x+2.0f, y+2.0f, 0.0f, 0.0f };
	if ( lg_map2d::collide( map, aabb ) )
	{
		player.x += aabb.rx;
		player.y += aabb.ry;
	}
}

// plain scanline draw
void drawScanline1( int x, float span, float dist, int tx )
{
	float ts = 64.0f / span;
	float ti = 0.0f;
	
	uint32 *tex = ((uint32*)texture->pixels) + (tx * 64);

	span *= 0.5f;
	
	int sx = 120 - span;
	int ex = 120 + span;

	for ( int y=sx; y<ex; y++ )
	{
		uint32 colour = tex[ (int)ti & 0x3f ];
		screen.plot( x, y, colour );
		ti += ts;
	}
}

// draw scanline with blend fog
void drawScanline2( int x, float span, float dist, int tx )
{
	float ts = 64.0f / span;
	float ti = 0.0f;

	uint32 *tex = ((uint32*)texture->pixels) + (tx*64);

	span *= 0.5f;
	
	int sx = 120 - span;
	int ex = 120 + span;

	int df = (dist - 32) * 6;
	if ( df >= 255 ) df = 255;
	if ( df <  0   ) df = 0;
	
	df = 255 - df;

	for ( int y=sx; y<ex; y++ )
	{
		uint32 colour = tex[ (int)ti & 0x3f ];
		uint32 pack = 0;

		pack |= (( (colour&0x0000ff) * df) >> 8) & 0x0000ff;
		pack |= (( (colour&0x00ff00) * df) >> 8) & 0x00ff00;
		pack |= (( (colour&0xff0000) * df) >> 8) & 0xff0000;

		screen.plot( x, y, pack );

		ti += ts;
	}
}

void raycastMap( void )
{
	float it = 1.0f / 8.0f;

	float x = player.x;
	float y = player.y;
	float nx = sinf( player.angle );
	float ny = cosf( player.angle );

	float fov = 70;

	lg_map2d::rayHit_t hit;

	int sx = 0;
	for ( float i=-160; sx<320; sx++, i++ )
	{
		float dx = nx - (ny*i)*0.003f;
		float dy = ny + (nx*i)*0.003f;

		lg_map2d::raycast( map, x*it, y*it, (x+dx)*it, (y+dy)*it, hit );
	
		// hit to player dir
		dx = (hit.hx*8) - x;
		dy = (hit.hy*8) - y;

		float pd = (dx*nx + dy*ny);

		float size = (2048.f + 1024.f) / pd;

		int tx = 0;

		switch ( hit.face )
		{
		case ( 0 ): 
		case ( 1 ):
			tx = (int)(hit.hy*64) % 64;
			drawScanline1( 160 + (int)i, size, pd, tx );
			break;
		case ( 2 ):
		case ( 3 ):	
			tx = (int)(hit.hx*64) % 64;
			drawScanline1( 160 + (int)i, size, pd, tx );
			break;
		}
	}
}

void loadTexture( void )
{
	texture = SDL_LoadBMP( "textures/greystone.bmp" );
	if ( texture == nullptr )
		exit( -5 );
}

int main( int argc, char **args )
{
	app_init( );
	mapBuild( );
	loadTexture( );
	
	SDL_ShowCursor( 0 );
	SDL_WM_GrabInput( SDL_GRAB_ON );

	// setup the player
	player.x = 320 / 2;
	player.y = 240 / 2;
	player.angle = 0.0f;

	//
	int time = SDL_GetTicks( );

	// main loop
	while ( app_tick( ) )
	{
		int rx = 0, ry = 0;
		SDL_GetRelativeMouseState( &rx, &ry );
		player.angle -= (float)rx * 0.003f;

		raycastMap( );
//		drawMap( );

		int diff = SDL_GetTicks() - time;

		if ( diff > 25 )
		{
			updateLogic( );
			time += 25;
		}
		else
			SDL_Delay( 0 );
	}

	lg_map2d::release( map );
	lg_canvas::release( screen );
	SDL_Quit( );
	return 0;
}