#define _SDL_main_h
#include <SDL/SDL.h>
#include <lg_litegame.h>

#pragma comment( lib, "SDL.lib" )

static SDL_Surface *sdl_surf = NULL;

static lg_cCanvas screen;
static lg_cSprite sprite;

void app_init( void )
{
	if ( SDL_Init( SDL_INIT_VIDEO ) != 0 )
		exit( -1 );
	sdl_surf = SDL_SetVideoMode( 640, 480, 32, 0 );
	if ( sdl_surf == NULL )
		exit( -2 );
	if (! lg_canvas::create( screen, 320, 240 ) )
		exit( -3 );
}

bool app_tick( void )
{
	static bool active = true;

	SDL_Event event;
	while ( SDL_PollEvent( &event ) )
	{
		if ( event.type == SDL_QUIT )
			active = false;

		if ( event.type == SDL_KEYDOWN )
		{
			switch ( event.key.keysym.sym )
			{
			case ( SDLK_ESCAPE ):
			case ( SDLK_q ):
				active = false;
				break;
			}
		}
	}

	return active;
}

void game_tick( void )
{
	lg_canvas::blitx2( screen, (u32*)sdl_surf->pixels, 640, 480 );
	SDL_Flip( sdl_surf );
	screen.clear( 0x102040 );

	lg_sSpriteDrawInfo drawInfo;
	sprite.getDrawInfo( drawInfo );

    vec2i pos  = vec2i( 160, 120 );
          pos -= drawInfo.offset;
          
    screen.line( 160, 115, 160, 125, 0xa0a0a0 );
    screen.line( 155, 120, 165, 120, 0xa0a0a0 );

	screen.maskBlit( *drawInfo.image, pos, drawInfo.frame );
    
	sprite.tick( );
}

class cHandler
    : public lg_cSpriteHandler
{
public:
    
	virtual void spriteHandler( lg_cSprite *spr, byte signal )
    {
        assert( spr != nullptr );

        if ( signal == 1 )
            screen.rect( 160, 120, 8, 8, 0xFFFFFF );
    }

};

int main( int argc, char **args )
{
	lg_module::initAll( );

	app_init( );
	int time = SDL_GetTicks( );

    cHandler handler;

	// create the sprite
    if (! lg_sprite::create( lg_cStringRef( "miniNinja" ), sprite ) )
        return false;

    // set the sprite handler
    sprite.handler = &handler;
    
    if (! sprite.play( lg_cStringRef( "idle" ), 0 ) )
        return false;

    const int rate = 1000 / 60;

    int nKeys = 0;
    uint8_t *keys = SDL_GetKeyState( &nKeys );

	// main loop
	while ( app_tick( ) )
	{
		int diff = SDL_GetTicks() - time;
		if ( diff > rate )
		{
			game_tick( );
			time += rate;
		}
		else
			SDL_Delay( 0 );

        if ( keys[ SDLK_r ] )
            if (! sprite.play( lg_cStringRef( "run" ), 0 ) )
                return false;

        if ( keys[ SDLK_i ] )
            if (! sprite.play( lg_cStringRef( "idle" ), 0 ) )
                return false;

        if ( keys[ SDLK_j ] )
            if (! sprite.play( lg_cStringRef( "jump" ), 0 ) )
                return false;
        
        if ( keys[ SDLK_s ] )
            if (! sprite.play( lg_cStringRef( "skid" ), 0 ) )
                return false;

        if ( keys[ SDLK_f ] )
            if (! sprite.play( lg_cStringRef( "fall" ), 0 ) )
                return false;

        if ( keys[ SDLK_l ] )
            if (! sprite.play( lg_cStringRef( "slide" ), 0 ) )
                return false;
	}
	
	lg_sprite::release( sprite );
	lg_canvas::release( screen );
	SDL_Quit( );
	return 0;
}