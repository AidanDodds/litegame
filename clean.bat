REM executables

del /s *.exe

REM VISUAL C++

del /s *.pdb
del /s *.sdf
del /s *.lib
del /s *.obj
del /s *.ilk
del /s *.suo
del /s *.log
del /s *.tlog
del /s *.idb
del /s *.lastbuildstate
del /s *.ipch
del /s *.manifest
del /s *.exp
del /s *.ncb
del /s *.cache
del /s *.rc
del /s *.res
del /s *.unsuccessfulbuild


REM LTSPICE

del /s *.raw
del /s *.plt
del /s *.fft

pause
