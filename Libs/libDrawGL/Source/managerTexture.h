#pragma once

#include "types.h"
#include "../../libUtility/source/lg_image.h"

typedef unsigned int texture_t;

//
// 
//
class cManTexture
{

    // hidden data
    struct sData *data;

public:

    cManTexture( void );

    bool start( void );
    bool stop ( void );

    //
    // create a texture
    //

    bool create
        ( texture_t &id, const mut::vec2i & size );

    //
    // load a texture
    //

    bool load
        ( texture_t &id, const char *path );

    //
    // re-upload a modified texture
    //

    bool refresh
        ( texture_t id );

    //
    // bind a texture
    //

    bool bind
        ( texture_t id, int slot );

    //
    // construct a alpha channel from colour key
    //

    bool setKeyColour
        ( texture_t id, u32 key );

    //
    // get pixel data from a texture
    //
    bool getImage
        ( texture_t id, lg_cImage **image );

    //
    //
    //
    bool setState
        ( void );

};
