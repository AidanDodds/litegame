#pragma once

#include "types.h"
#include "../../libMut/mut.h"

struct sCamera
{
    mut::mat4 viewMatrix;
    mut::mat4 projMatrix;

    float elevation;
    float rotation;
    mut::vec3 pos;

    sCamera( void )
    {
        mut::identity( viewMatrix );
        mut::identity( projMatrix );
        pos = mut::vec3( 0.f, 0.f, 0.f );
        elevation = 0.f;
        rotation = 0.f;
    };

    // fov in radians
    void setProject3d( float aspect, float fov );

    void setProject2d( float w, float h );

    // set the camera given a view direction and position
    void set( const mut::vec3 &viewDir, const mut::vec3 &viewPos );

    // move the camera first person style
    void move( const mut::vec3 &dir );

    // point camera at a location
    void lookAt( const mut::vec3 &pos );

    // 
    void scroll( const mut::vec2 &a );

    //
    void update( void );

/*
    // find ray from the camera into the world
    bool screenToRay( const mut::vec2 &scoord, ray3 & ray );
 */

};