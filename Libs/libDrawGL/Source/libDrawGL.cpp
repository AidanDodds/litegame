
#include "../../libUtility/source/lg_resource.h"
#include "../Libs/glee/GLee.h"

#include "libDrawGL.h"

#include <stdio.h>

//
struct sRenderData
{
    sRenderData( )
        : texture()
        , shader()
        , draw()
    { }

    cManTexture texture;
    cManShader  shader;
    cManDraw    draw;
};

//
cLibDrawGL::cLibDrawGL( void )
    : data( nullptr )
{
}

//
bool cLibDrawGL::start( void )
{
    lg_assert( data == nullptr );
    data = new sRenderData( );
    lg_assert( data != nullptr );
    
    glDisable( GL_CULL_FACE );
    glEnable( GL_TEXTURE_2D );

    return glGetError() == GL_NO_ERROR;
}

//
bool cLibDrawGL::stop ( void )
{
    return true;
}

//
cManTexture & cLibDrawGL::texture( void )
{
    lg_assert( data != nullptr );
    return data->texture;
}

//
cManShader & cLibDrawGL::shader( void )
{
    lg_assert( data != nullptr );
    return data->shader;
}

//
cManDraw & cLibDrawGL::draw( void )
{
    lg_assert( data != nullptr );
    return data->draw;
}