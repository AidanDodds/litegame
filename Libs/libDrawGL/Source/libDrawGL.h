#pragma once

#include "../../libMut/mut.h"

#include "types.h"
#include "camera.h"

#include "managerShader.h"
#include "managerTexture.h"
#include "managerDraw.h"

//
// rendering subsystem
//
class cLibDrawGL
{

    // hidden constructors
    cLibDrawGL( const cLibDrawGL & );
    cLibDrawGL( void );

    // hidden data implementation
    struct sRenderData * data;

public:
    
    //
    // singleton interface
    //

    bool start( void );
    bool stop ( void );

    static cLibDrawGL & inst( void )
    {
        static cLibDrawGL instance;
        return instance;
    }
    
    //
    // subsystem managers
    //
    
    cManTexture & texture( );
    cManShader  & shader ( );
    cManDraw    & draw   ( );
    
};