#include "managerTexture.h"

#include "../../libUtility/source/lg_resource.h"

#include "../Libs/glee/GLee.h"

//
struct sData
{
    int dummy;
};

// per texture data
struct sTexture
{
    sTexture( void )
        : image ()
        , glid  (-1 )
        , state ( 0 )
    { }
    
    ~sTexture()
    {
        image.unload( );
    }

    lg_cImage image;
    GLuint    glid;
    int       state;

};

// shader resource manager
typedef util::lg_cResource<sTexture,64> cTextureResource;

// texture slot array
static const u32 slotLookup[] =
{
    GL_TEXTURE0,
    GL_TEXTURE1,
    GL_TEXTURE2,
    GL_TEXTURE3
};

//
cManTexture::cManTexture( void )
    : data( nullptr )
{
}

//
bool cManTexture::start( void )
{
    return true;
}

//
bool cManTexture::stop( void )
{
    return true;
}

//
bool cManTexture::refresh( texture_t id )
{
    sTexture *tex = cTextureResource::inst().find( id );
    if ( tex == nullptr )
        return false;

    // make this our current texture
    glBindTexture( GL_TEXTURE_2D, tex->glid );

    // upload pixels into the texture
    glTexImage2D
    (
        GL_TEXTURE_2D,
        0,
        GL_RGBA,
        tex->image.getSize().x,
        tex->image.getSize().y,
        0,
        // load in as GL_RGBA
        GL_BGRA,
        GL_UNSIGNED_INT_8_8_8_8_REV,
        (GLvoid*) tex->image.getPixels()
    );
    if ( glGetError() != GL_NO_ERROR )
        return false;

    return glGetError() == GL_NO_ERROR;
}

//
bool cManTexture::load( texture_t &id, const char *path )
{
    id = -1;

    sTexture * tex = new sTexture;
    lg_assert( tex != nullptr );

    // load the image itself
    if (! tex->image.load( path ) )
        goto onError;
    
    // generate a name for the texture
    glGenTextures( 1, &(tex->glid) );
    if ( tex->glid == -1 )
        goto onError;
    
    // add to the texture resource list
    id = cTextureResource::inst().add( tex );
    if ( id == -1 )
        goto onError;

    // upload texture to opengl
    if (! refresh( id ) )
    {
        cTextureResource::inst().release( id );
        goto onError;
    }

    return true;

    // error handler
onError:
    lg_assert( tex != nullptr );
    delete tex;
    id = -1;
    return false;
}

//
bool cManTexture::create( texture_t &id, const mut::vec2i & size )
{
    sTexture * tex = new sTexture;
    lg_assert( tex != nullptr );
    
    // create an image of this size
    if (! tex->image.create( size ) )
        goto onError;

    // generate a name for the texture
    glGenTextures( 1, &(tex->glid) );
    if ( tex->glid == -1 )
        goto onError;

    // add to the resource manager
    id = cTextureResource::inst().add( tex );
    if ( id == -1 )
        goto onError;

    return true;

    // error handler
onError:
    lg_assert( tex != nullptr );
    tex->image.unload( );
    delete tex;
    id = -1;
    return false;
}

//
bool cManTexture::bind( texture_t id, int slot )
{
    sTexture *tex = cTextureResource::inst().find( id );
    if ( tex == nullptr )
        return false;
    
    // set the active texture slot
    lg_assert( slot >= 0 && slot < 4 );
    glActiveTexture( slotLookup[ slot ] );
    
    // enable texturing
//    glEnable( GL_TEXTURE_2D );
    
    // bind this texture
	glBindTexture( GL_TEXTURE_2D, tex->glid );

    // set texture slot state
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR ); 
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    // texture wrapping
    if ( true )
    {
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    }

    // alpha blending
    if ( false )
    {
        glEnable( GL_BLEND );
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
        glBlendEquation( GL_FUNC_ADD );
    }
    else
    {
//        glDisable( GL_BLEND );
    }
    
    return glGetError() == GL_NO_ERROR;
}

//
bool cManTexture::setKeyColour( texture_t id, u32 key )
{
    sTexture *tex = cTextureResource::inst().find( id );
    if ( tex == nullptr )
        return false;

    // mask out alpha from colour key
    key &= 0xffffff;

    // get image data
    const mut::vec2i & size = tex->image.getSize( );
    u32 *pix = tex->image.getPixels();
    lg_assert( pix != nullptr );

    // for each pixel
    for ( int i=0; i<size.x*size.y; i++ )
    {
        // get exact pixel
        u32 & p = pix[i];

        // mask out alpha channel
        p &= 0xffffff;

        // if colour key matches
        if ( p == key )
            // set alpha channel
            p |= 0xff000000;
    }

    // re-upload the texture
    refresh( id );

    // success
    return true;
}