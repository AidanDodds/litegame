#include "types.h"
#include "render.h"
#include "framework.h"
#include "demo.h"


int main( void )
{
    cFramework &framework = cFramework::inst( );
    cRender &render = cRender::inst( );
    cDemo &demo = cDemo::inst( );

    // fake exception handler
    for ( ; ; )
    {
        // init sdl framework
        if (! framework.start( ) )
            break;
        // create an opengl window
        if (! framework.CreateGLWindow( 640, 480, false ) )
            break;
    
        // init opengl framework
        if (! render.start( ) )
            break;

        if (! demo.start( ) )
            break;

        // main loop
        while (! framework.willQuit( ) )
        {
            render.draw_clear( );

            demo.tick( );

            framework.flip( );
        }

        break;
    }

    demo.stop( );
    render.stop( );
    framework.stop( );

    return 0;
}