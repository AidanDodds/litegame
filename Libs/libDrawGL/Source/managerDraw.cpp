#include "managerDraw.h"

#include "../Libs/glee/GLee.h"

//
struct sData
{
    int dummy;
};

// 
cManDraw::cManDraw( void )
    : data( nullptr )
{ }

//
bool cManDraw::start( void )
{
    data = new sData;
    return true;
}

//
bool cManDraw::stop( void )
{
    delete data;
    data = nullptr;
    return true;
}

//
void cManDraw::vbuffer( cVertexBuffer *buffer )
{
    lg_assert( buffer != nullptr );

    // we must have vertex position
    lg_assert( buffer->vData != nullptr );
    lg_assert( buffer->iData != nullptr );

    // enable vertex data
    glEnableVertexAttribArray( 0 );
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*) buffer->vData );

    // uv coordinate data
    if ( buffer->tData != nullptr )
    {
        glEnableVertexAttribArray( 1 );
        glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*) buffer->tData );
    }

    // add colour data
    if ( buffer->cData != nullptr )
    {
        glEnableVertexAttribArray( 2 );
        glVertexAttribPointer( 2, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*) buffer->cData );
    }

    // 
    glDrawElements( GL_TRIANGLES, buffer->nIndices, GL_UNSIGNED_INT, buffer->iData );
}

//
void cManDraw::clear( void )
{
    glClearColor( .2f, .4f, .7f, .0f );
    glClearDepth( 1000.f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}

//
void cManDraw::depthBuffer( bool state )
{
    if ( state )
    {
        
    	glEnable( GL_DEPTH_TEST );
//        glEnable( GL_DEPTH_BUFFER );
    }
    else
    {
    	glDisable( GL_DEPTH_TEST );
//        glDisable( GL_DEPTH_BUFFER );
    }
}

// 
void cManDraw::quad
(
    // position
    const mut::vec2 & v1,
    // quad size
    const mut::vec2 & v2,
    // texture coordinate
    const mut::vec2 & t1,
    const mut::vec2 & t2
)
{
    static mut::vec3 vertex[4];
    static mut::vec3 colour[4];
    static mut::vec2 tcoord[4];
    static byte index[4] = { 1, 0, 2, 3 };

    // depth value
    float d = 0.5f;

    mut::vec2 v3 = v2 + v1;

    // set the vertex coordinates
    vertex[0] = mut::vec3( v1.x, v1.y, d );
    vertex[1] = mut::vec3( v3.x, v1.y, d );
    vertex[2] = mut::vec3( v3.x, v3.y, d );
    vertex[3] = mut::vec3( v1.x, v3.y, d );

    // set vertex colour
    colour[0] = mut::vec3( 1.0f, 1.0f, 1.0f );
    colour[1] = mut::vec3( 1.0f, 1.0f, 1.0f );
    colour[2] = mut::vec3( 1.0f, 1.0f, 1.0f );
    colour[3] = mut::vec3( 1.0f, 1.0f, 1.0f );
    
    // set texture coordinates colour
    tcoord[0] = mut::vec2( t1.x, t1.y );
    tcoord[1] = mut::vec2( t2.x, t1.x );
    tcoord[2] = mut::vec2( t2.x, t2.y );
    tcoord[3] = mut::vec2( t1.x, t2.y );
    
    glEnableVertexAttribArray( 0 );
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*) vertex );
    
    glEnableVertexAttribArray( 1 );
    glVertexAttribPointer( 2, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*) tcoord );

    glEnableVertexAttribArray( 2 );
    glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*) colour );

    
    glDrawElements( GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_BYTE, index );
}