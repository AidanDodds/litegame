#pragma once

#include "../../libUtility/source/lg_types.h"
#include "../../libMut/mut.h"

/*
typedef unsigned int  u32;
typedef unsigned char byte;


// ray
typedef float ray3[ 6 ];

*/

#ifndef assert
// simple assert
# ifdef _DEBUG
#  define lg_assert( X, ... ) { if (! (X) ) __asm { int 3 } }
# else
#  define lg_assert( X, ... )
# endif
#endif