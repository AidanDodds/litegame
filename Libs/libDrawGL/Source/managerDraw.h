#pragma once

#include "../../libMut/mut.h"
#include "types.h"

// texture resource ID
typedef unsigned int texture_t;

//
// primative vertex buffer
//
struct cVertexBuffer
{
    // colour data
    int        nColours;
    mut::vec3 *cData;

    // vertex data
    int        nVertices;
    mut::vec3 *vData;

    // index arrays
    int        nIndices;
    int       *iData;

    // texture coordinates
    int        nUVs;
    mut::vec2 *tData;
};

//
// 
//
class cManDraw
{

    // hidden data
    struct sData *data;

public:
    
    cManDraw( void );

    bool start( void );
    bool stop ( void );

    //
    // clear the screen
    //

    void clear( void );

    //
    // draw a host side vertex buffer
    //

    void vbuffer
        ( cVertexBuffer *buffer );

    //
    // set GL draw state
    //

    void depthBuffer
        ( bool state );
    
    //
    // draw a quad
    //

    void cManDraw::quad
        (
            // screen space coordinates
            const mut::vec2 & v1,
            const mut::vec2 & v2,
            // texture coordinate
            const mut::vec2 & t1,
            const mut::vec2 & t2
        );

};
