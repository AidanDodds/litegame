#pragma once

#include "../../libMut/mut.h"
#include "types.h"

// shader resource ID
typedef unsigned int shader_t;

// uniform location ID
typedef unsigned int uniform_t;

//
// 
//
class cManShader
{

    // hidden data
    struct sData *data;

public:
    
    cManShader( void );

    bool start( void );
    bool stop ( void );
    
    //
    // reload all shaders
    //

    bool reload
        ( void );

    //
    // compile a shader
    //

    bool compile
        ( const char *vsh, const char *fsh, shader_t & handle );

    //
    // load and compile a shader from file
    //

    bool loadAndCompile
        ( const char *filePath, shader_t & handle );

    //
    // bind a shader
    //

    bool bind
        ( shader_t id );

    //
    // get the location of a uniform
    //

    bool findUniform
        ( shader_t handle, const char *name, uniform_t &loc );

    //
    // find a ';' separated list of uniforms
    //

    bool findUniforms
        ( shader_t handle, const char *names, uniform_t *list, int count );

    //
    // set a uniform in the currently bound shader
    //
    
    bool setUniform
        ( uniform_t loc, const float );

    bool setUniform
        ( uniform_t loc, const int );

    bool setUniform
        ( uniform_t loc, const mut::mat3 & );

    bool setUniform
        ( uniform_t loc, const mut::mat4 & );

    bool setUniform
        ( uniform_t loc, const mut::vec3 & );

};