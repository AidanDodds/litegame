#include "managerShader.h"

#include "../../libUtility/source/lg_resource.h"
#include "../../libUtility/source/lg_file.h"
#include "../../libUtility/source/lg_stream.h"
#include "../../libUtility/source/lg_parser.h"

#include "../Libs/glee/GLee.h"

#include <stdio.h>
#include <string.h>

// shader program container
struct sShader
{
    GLuint vshader;
    GLuint fshader;
    GLuint program;
};

// shader resource manager
typedef util::lg_cResource<sShader,64> cShaderResource;

// GL program type
enum
{
    SHADER_VERTEX   = 0,
    SHADER_FRAGMENT ,
    SHADER_PROGRAM
};

/**
    Helper function to dump any errors from OpenGL
    when compiling shaders.
 */
static
bool dumpShaderError( GLint shader, int type )
{
    GLint logSize = 0;
    char *log = nullptr;
    // switch based on shader type
    switch ( type )
    {
    case ( SHADER_VERTEX   ):
    case ( SHADER_FRAGMENT ):
        {
	        glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &logSize );
	        if ( logSize <= 0 )
		        return false;
	        log = new char [ logSize ];
            glGetShaderInfoLog( shader, logSize, NULL, log );
        }
        break;
    case ( SHADER_PROGRAM ):
        {
            glGetProgramiv( shader, GL_INFO_LOG_LENGTH, &logSize );
            if ( logSize <= 0 )
                return false;
            log = new char [ logSize ];
            glGetProgramInfoLog( shader, logSize, NULL, log );
        }
        break;
    };
    // write trailing zero
    log[ logSize-1 ] = '\0';

    // for now just dump to printf
    printf_s( "%s", log );

    delete [] log;
    return glGetError() == GL_NO_ERROR;
}

//
cManShader::cManShader( void )
    : data( nullptr )
{
}

//
bool cManShader::start( void )
{
    return true;
}

//
bool cManShader::stop( void )
{
    return true;
}

// 
bool cManShader::compile( const char *vsh, const char *fsh, shader_t &handle )
{
    GLint status = 0;
    
    // compile fragment shader
    GLint fsobj = glCreateShader( GL_FRAGMENT_SHADER );
    glShaderSource( fsobj, 1, (const GLchar **)&fsh, nullptr );
    glCompileShader( fsobj );
    glGetShaderiv( fsobj, GL_COMPILE_STATUS, &status );
    if (! status )
    {
        dumpShaderError( fsobj, SHADER_FRAGMENT );
        return false;
    }

    // compile vertex shader
    GLint vsobj = glCreateShader( GL_VERTEX_SHADER );
    glShaderSource( vsobj, 1, (const GLchar **)&vsh, nullptr );
    glCompileShader( vsobj );
    glGetShaderiv( vsobj, GL_COMPILE_STATUS, &status );
    if (! status )
    {
        dumpShaderError( vsobj, SHADER_VERTEX );
        return false;
    }

    // create overall shader program
    GLint shobj = glCreateProgram( );
    glAttachShader( shobj, vsobj );
    glAttachShader( shobj, fsobj );

    // bind fixed attribute locations
	glBindAttribLocation( shobj, 0, "vPosition" );
	glBindAttribLocation( shobj, 1, "vTexCoord" );
	glBindAttribLocation( shobj, 2, "vColour"   );

    // try to link the programs
	glLinkProgram( shobj );
    glGetProgramiv( shobj, GL_LINK_STATUS, &status );
    if (! status )
    {
        dumpShaderError( vsobj, SHADER_PROGRAM );
        return false;
    }

    // create a new shader object
    sShader *shader = new sShader( );
    // copy over the shader values
    shader->program = shobj;
    shader->fshader = fsobj;
    shader->vshader = vsobj;
    // add to the resource manager
    shader_t id = cShaderResource::inst().add( shader );
    lg_assert( id >= 0 );

    // output the handle
    handle = id;
    
    return glGetError() == GL_NO_ERROR;
}

//
bool cManShader::loadAndCompile( const char *filePath, shader_t & handle )
{
    /* load shader file */
    lg_cFile file;
    if (! file.load( filePath ) )
        return false;

    /* parser and line buffer */
    static char line[ 128 ] = { '\0' };
    lg_cParser parser( (void*)file.getData() );

    /* shader buffers */
    lg_cStream vstream( 2048 );
    lg_cStream fstream( 2048 );

    /* parse vertex shader */
    bool inCode = false;
    while ( true )
    {
        if (! parser.readLine( line, sizeof( line ) ) )
            return false;
        
        // check for shader end
        if ( strncmp( line, "</shader_vertex>", 16 ) == 0 )
            break;

        // put into shader buffer
        if ( inCode )
            vstream.printf( "%s\r\n", line );

        // check for shader start
        if ( strncmp( line, "<shader_vertex>", 15 ) == 0 )
        {
            vstream.printf( "#line %d\r\n", parser.getLineNumber() );
            inCode = true;
        }
    }
    
    /* parse fragment shader */
    inCode = false;
    while ( true )
    {
        if (! parser.readLine( line, sizeof( line ) ) )
            return false;
        
        // check for shader end
        if ( strncmp( line, "</shader_fragment>", 18 ) == 0 )
            break;

        // put into shader buffer
        if ( inCode )
            fstream.printf( "%s\r\n", line );

        // check for shader start
        if ( strncmp( line, "<shader_fragment>", 17 ) == 0 )
        {
            fstream.printf( "#line %d\r\n", parser.getLineNumber() );
            inCode = true;
        }
    }

    // send to compilation stage
    return compile( vstream.get(), fstream.get(), handle );
}

//
bool cManShader::bind( shader_t handle )
{
    sShader *shader = cShaderResource::inst().find( handle );
    if ( shader == nullptr )
        return false;

    glUseProgram( shader->program );

    return glGetError() == GL_NO_ERROR;
}

//
bool cManShader::findUniform( shader_t handle, const char *name, uniform_t &loc )
{
    sShader *shader = cShaderResource::inst().find( handle );
    if ( shader == nullptr )
        return false;

    loc = glGetUniformLocation( shader->program, (const GLchar*) name );

    // name does not orrespond to an active uniform variable
    if ( loc == -1 )
        return false;

    return glGetError( ) == GL_NO_ERROR;
}

//
bool cManShader::findUniforms
    ( shader_t handle, const char *names, uniform_t * list, int count )
{
    sShader *shader = cShaderResource::inst().find( handle );
    if ( shader == nullptr )
        return false;
    // temp name buffer
    static char uname[32];
    // extract all names
    for ( int i=0,j=0; *names != '\0'; )
    {
        // get next character
        char ch = *(names++);
        // while we dont have a terminal character
        if ( ch != ';' )
        {
            // dont overflow the temp buffer
            if ( i >= (sizeof( uname )-1) )
                return false;
            // append character
            uname[i++] = ch;
            continue;
        }
        // append null terminator
        uname[i] = '\0';
        i = 0;
        // check we have 
        if ( j >= count )
            break;
        // find uniform location
        u32 loc = glGetUniformLocation( shader->program, (const GLchar*) uname );
        if ( loc == -1 )
            return false;
        // add uniform to list
        list[j++] = loc;
    }
    // success
    return true;
}

//
bool cManShader::setUniform( uniform_t loc, const float value )
{
    glUniform1f( loc, value );
    return glGetError( ) == GL_NO_ERROR;
}

//
bool cManShader::setUniform( uniform_t loc, const int value )
{
    glUniform1i( loc, value );
    return glGetError( ) == GL_NO_ERROR;
}

//
bool cManShader::setUniform( uniform_t loc, const mut::mat3 &mat )
{    
    glUniformMatrix3fv( loc, 1, GL_FALSE, &(mat.e[0]) );
    return glGetError( ) == GL_NO_ERROR;
}

//
bool cManShader::setUniform( uniform_t loc, const mut::mat4 &mat )
{    
    glUniformMatrix4fv( loc, 1, GL_FALSE, &(mat.e[0]) );
    return glGetError( ) == GL_NO_ERROR;
}

//
bool cManShader::setUniform( uniform_t loc, const mut::vec3 &v )
{
    glUniform3f( loc, v.x, v.y, v.z );
    return glGetError( ) == GL_NO_ERROR;
}
