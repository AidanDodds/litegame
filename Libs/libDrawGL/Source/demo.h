#pragma once


// rendering subsystem
class cDemo
{
    cDemo( const cDemo & );
    cDemo( void );

    // hidden data implementation
    struct sDemoData * data;

public:
    
    //
    // singleton interface
    //

    bool start( void );
    bool stop ( void );

    static cDemo & inst( void )
    {
        static cDemo instance;
        return instance;
    }

    //
    // instance methods
    //

    // per frame tick
    bool tick( void );

};