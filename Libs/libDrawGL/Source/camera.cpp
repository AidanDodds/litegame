#include "camera.h"

//
void sCamera::setProject3d( float aspect, float fov )
{
    mut::perspective( projMatrix, mut::pi * 1.4f, 1.333f, .1f, 1000.f );
    mut::identity( viewMatrix );
}

//
void sCamera::setProject2d( float w, float h )
{
    mut::identity( projMatrix );

    float *e = projMatrix.e;

    e[ 0] = 2.0f / w;
    e[ 5] =-2.0f / h;
    e[12] =-1.f;
    e[13] = 1.f;
}

// set the camera given a view direction and position
void sCamera::set( const mut::vec3 &az, const mut::vec3 &p )
{
    // normalize z componant
    mut::vec3 z = mut::normalize( az );

    // calculate y vector
//  mut::vec3 d = mut::vec3( 0.f, 1.f, 0.f );
//  mut::vec3 y = d - (d * dir) * dir;
    mut::vec3 y = mut::vec3( -z.y * z.x, 1 - z.y * z.y, -z.y * z.z );

    // calculate x vector
    mut::vec3 x = mut::cross( y, z );

    // pack these vectors into the view matrix
    viewMatrix = mut::mat4( x, y, z, p );
}

// move the camera first person style
void sCamera::move( const mut::vec3 &dir )
{
    pos = pos + ( mut::getXVector( viewMatrix ) * dir.x );
    pos = pos + ( mut::getYVector( viewMatrix ) * dir.y );
    pos = pos + ( mut::getZVector( viewMatrix ) * dir.z );
}

void sCamera::scroll( const mut::vec2 &d )
{
    rotation  -= d.x;
    elevation += d.y;
}

void sCamera::update( void )
{
	mut::mat4 rx; mut::rotateX( rx, elevation );
	mut::mat4 ry; mut::rotateY( ry, rotation  );
    
    mut::identity ( viewMatrix );
	mut::translate( viewMatrix, pos );
	
	// multiply by elevation before rotation
	viewMatrix = mut::multiply( ry, viewMatrix );
	viewMatrix = mut::multiply( rx, viewMatrix );

//    mut::identity( viewMatrix );
}