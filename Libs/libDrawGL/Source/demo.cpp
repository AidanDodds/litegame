#include "demo.h"
#include "types.h"
#include "render.h"
#include "camera.h"

#define GLM_FORCE_RADIANS
#include "glm\vec4.hpp"
#include "glm\gtc\type_ptr.hpp"

static
const char *vsh =
    "                                       "
    "uniform mat4 uProj;                    "
    "                                       "
    "attribute vec3 vPosition;              "
    "attribute vec3 vColour;                "
    "                                       "
    "varying vec3 col;                      "
    "                                       "
    "void main()                            "
    "{                                      "
    "    gl_Position =                      "
    "       uProj * vec4( vPosition, 1.f ); "
    "    col =                              "
    "       vColour;                        "
    "}                                      ";

static
const char *fsh =
    "varying vec3 col;                      "
    "                                       "
    "void main()                            "
    "{                                      "
    "    gl_FragColor =                     "
    "        vec4( col, 0.0f );             "
    "}                                      ";

struct sDemoData
{
    sCamera     camera;
    resource_t  shader;
    uniform_t   uProj;
};

cDemo::cDemo( void )
    : data( nullptr )
{ }

bool cDemo::start( void )
{
    assert( data == nullptr );
    data = new sDemoData( );
    assert( data != nullptr );

    // create a basic fragment shader
    cRender &render = cRender::inst( );
    if (! render.shader_compile( vsh, fsh, data->shader ) )
        return false;

    data->camera.projMatrix = glm::mat4();
    data->camera.setProjection( 640.f / 480.f, glm::half_pi<float>( ) );


    if (! render.shader_getUniformLocation( data->shader, "uProj", data->uProj ) )
        return false;

    return true;
}

bool cDemo::stop( void )
{
    return true;
}

bool cDemo::tick( void )
{
    cRender &render = cRender::inst( );

    if (! render.shader_bind( data->shader ) )
        return false;

//    glm::vec4 colour = glm::vec4( 0.0f, 1.0f, 0.f, 0.f );
    
    if (! render.shader_setUniform
        (
            data->shader, 
            data->uProj, 
            data->camera.projMatrix
        ) )
        return false;

    // rejects fragment with final z outside of 0.f -> 1.f
    // it seems the projection matrix projects z coordinates into this range
    glm::vec3 vertex[] = 
        { glm::vec3( 0.f, 1.f,-2.f ),
          glm::vec3(-1.f,-1.f,-0.f ),
          glm::vec3( 1.f,-1.f,-0.f ) };

    glm::vec3 color[] = 
        { glm::vec3( 1.f, 0.f, 0.f ),
          glm::vec3( 0.f, 1.f, 0.f ),
          glm::vec3( 0.f, 0.f, 1.f ) };
    
    int index[] = { 1, 2, 3 };

    // primative vertex buffer
    cVertexBuffer buffer =
    {
        3, color,
        3, vertex,
        3, index
    };

    render.draw_buffer( & buffer );

    return true;
}
