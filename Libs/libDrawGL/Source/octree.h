#pragma once
#include "types.h"
#include "render.h"

// voxel chunk
// 16^3 units
struct sChunk
{
    static const int nSize = 16;
    byte data[ nSize * nSize * nSize ];

    // vertex buffer
    cVertexBuffer *buffer;
};

