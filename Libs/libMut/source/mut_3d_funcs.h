/*
 *     _____          __  .__       ____ ___   __  .__.__  .__  __           ___________           .__   __   .__  __   
 *    /     \ _____ _/  |_|  |__   |    |   \_/  |_|__|  | |__|/  |_ ___.__. \__    ___/___   ____ |  | |  | _|__|/  |_ 
 *   /  \ /  \\__  \\   __\  |  \  |    |   /\   __\  |  | |  \   __<   |  |   |    | /  _ \ /  _ \|  | |  |/ /  \   __\
 *  /    Y    \/ __ \|  | |   Y  \ |    |  /  |  | |  |  |_|  ||  |  \___  |   |    |(  <_> |  <_> )  |_|    <|  ||  |  
 *  \____|__  (____  /__| |___|  / |______/   |__| |__|____/__||__|  / ____|   |____| \____/ \____/|____/__|_ \__||__|  
 *          \/     \/          \/                                    \/                                      \/        
 *
 */
#include "mut_3d_types.h"
#include "mut_3d_ops.h"
#include "mut_1d_funcs.h"

namespace mut
{
    /**
        Generate an identity matrix
     */
    static inline
    void identity( mut::mat4 &m )
    {
        float *e = m.e;
        e[ 0]=1.f; e[ 1]=0.f; e[ 2]=0.f; e[ 3]=0.f; 
        e[ 4]=0.f; e[ 5]=1.f; e[ 6]=0.f; e[ 7]=0.f; 
        e[ 8]=0.f; e[ 9]=0.f; e[10]=1.f; e[11]=0.f; 
        e[12]=0.f; e[13]=0.f; e[14]=0.f; e[15]=1.f; 
    }

    /**
        Generate a 3d perspective matrix
     */
    static inline
    void perspective( mut::mat4 &d, float fovy, float aspect, float znear, float zfar )
    {
		float r = (fovy / 2) * 57.295779f;
		float deltaZ = zfar - znear;
		float s = mut::sin(r);

		if (deltaZ == 0 || s == 0 || aspect == 0)
			return;

		float cotangent = mut::cos(r) / s;

		float i =  cotangent / aspect;
		float j =  cotangent;
		float k = -(zfar + znear) / deltaZ;
		float l = -1;
		float m = -2 * znear * zfar / deltaZ;
		float n =  0;

        float *e = d.e;
		e[0] = i; e[4] = 0; e[ 8] = 0; e[12] = 0;
		e[1] = 0; e[5] =-j; e[ 9] = 0; e[13] = 0;
		e[2] = 0; e[6] = 0; e[10] = k; e[14] = l;
		e[3] = 0; e[7] = 0; e[11] = m; e[15] = n;
    };

    /**
        Transpose a matrix
     */
    static inline
    void transpose( mut::mat4 &d )
    {
//		___ 0x1 0x2 0x3
//		0x4 ___ 0x6 0x7
//		0x8 0x9 ___ 0xb
//		0xc 0xd 0xe ___

        float *e = d.e;
		float  t = 0.0f;
		t=e[0x1]; e[0x1]=e[0x4]; e[0x4]=t; // 1 -> 4
		t=e[0x2]; e[0x2]=e[0x8]; e[0x8]=t; // 2 -> 8
		t=e[0x3]; e[0x3]=e[0xc]; e[0xc]=t; // 3 -> c
		t=e[0x6]; e[0x6]=e[0x9]; e[0x9]=t; // 6 -> 9
		t=e[0x7]; e[0x7]=e[0xd]; e[0xd]=t; // 7 -> d
		t=e[0xb]; e[0xb]=e[0xe]; e[0xe]=t; // b -> e
    }

    /**
        Build an x-axis rotation matrix
     */
    static
	void rotateX( mut::mat4 &d, float a )
	{
        float c = mut::cos( a );
        float s = mut::sin( a );
        float *e = d.e;
		e[0x0]=1; e[0x1]=0; e[0x2]=0; e[0x3]=0;
		e[0x4]=0; e[0x5]=c; e[0x6]=s; e[0x7]=0;
		e[0x8]=0; e[0x9]=-s;e[0xa]=c; e[0xb]=0;
		e[0xc]=0; e[0xd]=0; e[0xe]=0; e[0xf]=1;
	}

    /**
        Build an y-axis rotation matrix
     */
    static
	void rotateY( mut::mat4 &d, float a )
	{
        float c = mut::cos( a );
        float s = mut::sin( a );
        float *e = d.e;
		e[0x0]=c; e[0x1]=0; e[0x2]=-s;e[0x3]=0;
		e[0x4]=0; e[0x5]=1; e[0x6]=0; e[0x7]=0;
		e[0x8]=s; e[0x9]=0; e[0xa]=c; e[0xb]=0;
		e[0xc]=0; e[0xd]=0; e[0xe]=0; e[0xf]=1;
	}

    /**
        Build an z-axis rotation matrix
     */
    static
	void rotateZ( mut::mat4 &d, float a )
	{
        float c = mut::cos( a );
        float s = mut::sin( a );
        float *e = d.e;
		e[0x0]=c; e[0x1]=s; e[0x2]=0; e[0x3]=0;
		e[0x4]=-s;e[0x5]=c; e[0x6]=0; e[0x7]=0;
		e[0x8]=0; e[0x9]=0; e[0xa]=1; e[0xb]=0;
		e[0xc]=0; e[0xd]=0; e[0xe]=0; e[0xf]=1;
	}

    /**
        Get x vector of a matrix
     */
    static inline
	mut::vec3 getXVector( const mut::mat4 &d )
	{
        const float *e = d.e;
		return mut::vec3( e[0], e[1], e[2] );
	}

    /**
        Get y vector of a matrix
     */
    static inline
	mut::vec3 getYVector( const mut::mat4 &d )
	{
        const float *e = d.e;
		return mut::vec3( e[4], e[5], e[6] );
	}

    /**
        Get z vector of a matrix
     */
    static inline
	mut::vec3 getZVector( const mut::mat4 &d )
	{
        const float *e = d.e;
		return mut::vec3( e[8], e[9], e[10] );
	}

    /**
        Build an arbirary 3d rotation matrix
     */
    static
    void rotate3( mut::mat4 &d, float a, const mut::vec3 & v )
    {
        float c = mut::cos( a );
        float s = mut::sin( a );

        float x = v.x; float y = v.y; float z = v.z;

        float xx = x * x;
        float xy = x * y;
        float xz = x * z;
        float yy = y * y;
        float yz = y * z;
        float zz = z * z;

        // build rotation matrix
        float *m = d.e;
        m[ 0] = xx * (1 - c) + c;
        m[ 1] = xy * (1 - c) - z * s;
        m[ 2] = xz * (1 - c) + y * s;
        m[ 3] = 0;
        m[ 4] = xy * (1 - c) + z * s;
        m[ 5] = yy * (1 - c) + c;
        m[ 6] = yz * (1 - c) - x * s;
        m[ 7] = 0;
        m[ 8] = xz * (1 - c) - y * s;
        m[ 9] = yz * (1 - c) + x * s;
        m[10] = zz * (1 - c) + c;
        m[11] = 0;
        m[12] = 0;
        m[13] = 0;
        m[14] = 0;
        m[15] = 1;
    }

    /**
        Multiply matrices ( d = a * b )
     */
	static
    void multiply( mut::mat4 &d, const mut::mat4 &a, const mut::mat4 &b )
	{
		const float *m1Ptr = & a.e[0];
		const float *m2Ptr = & b.e[0];
		      float *dmPtr = & d.e[0];
		
		for ( int i = 0; i < 4; i++ ) 
		{
			for ( int j = 0; j < 4; j++ )
			{
				*dmPtr = m1Ptr[0] * m2Ptr[ 0 * 4 + j ]
					   + m1Ptr[1] * m2Ptr[ 1 * 4 + j ]
					   + m1Ptr[2] * m2Ptr[ 2 * 4 + j ]
					   + m1Ptr[3] * m2Ptr[ 3 * 4 + j ];
				dmPtr++;
			}
			m1Ptr += 4;
		}
	}

    /**
        Multiply matrices ( d = a * b )
     */
	static
    mut::mat4 multiply( const mut::mat4 &a, const mut::mat4 &b )
	{
		mut::mat4 d;

		const float *m1Ptr = & a.e[0];
		const float *m2Ptr = & b.e[0];
		      float *dmPtr = & d.e[0];
		
		for ( int i = 0; i < 4; i++ ) 
		{
			for ( int j = 0; j < 4; j++ )
			{
				*dmPtr = m1Ptr[0] * m2Ptr[ 0 * 4 + j ]
					   + m1Ptr[1] * m2Ptr[ 1 * 4 + j ]
					   + m1Ptr[2] * m2Ptr[ 2 * 4 + j ]
					   + m1Ptr[3] * m2Ptr[ 3 * 4 + j ];
				dmPtr++;
			}
			m1Ptr += 4;
		}
		return d;
	}

    /**
        Translate a matrix
     */
    static
    void translate( mut::mat4 &d, const mut::vec3 &a )
    {
        d.e[ 3] = a.x;
        d.e[ 7] = a.y;
        d.e[11] = a.z;
    }

    /**
        Swap two vec3 objects
     */
    static inline
    void swap( mut::vec3 &a, mut::vec3 &b )
    {
        float x = a.x; a.x = b.x; b.x = x;
        float y = a.y; a.y = b.y; b.y = y;
        float z = a.z; a.z = b.z; b.z = z;
    }
    
    /**
        Create a unit length vector
     */
    static inline
    vec3 normalize( const mut::vec3 &a )
    {
        float rl = mut::fast_rsqrtf( a.x*a.x + a.y*a.y + a.z*a.z );
        return mut::vec3
        (
            a.x * rl,
            a.y * rl,
            a.z * rl
        );
    }

    /**
        Find the cross product of two vectors
     */
    static inline
    mut::vec3 cross( const mut::vec3 &a, const mut::vec3 &b )
    {
        return mut::vec3
		(
			a.y * b.z - b.y * a.z,
			a.z * b.x - b.z * a.x,
			a.x * b.y - b.x * a.y
		);
    }

    /**

     */
    static inline
    mut::vec3 operator + ( const mut::vec3 &a, const mut::vec3 &b )
    {
        return mut::vec3
        (
            a.x + b.x,
            a.y + b.y,
            a.z + b.z
        );
    }

    /**

     */
    static inline
    mut::vec3 operator * ( const mut::vec3 &a, const mut::mat4 &b )
    {
        return mut::vec3
        (
            a.x*b.e[0] + a.y*b.e[1] + a.z*b.e[ 2],
            a.x*b.e[4] + a.y*b.e[5] + a.z*b.e[ 6],
            a.x*b.e[8] + a.y*b.e[9] + a.z*b.e[10]
        );
    }
     
    /**

     */
    static inline
    mut::vec3 operator * ( const mut::vec3 &a, const float s )
    {
        return mut::vec3
        (
            a.x * s,
            a.y * s,
            a.z * s
        );
    }

};