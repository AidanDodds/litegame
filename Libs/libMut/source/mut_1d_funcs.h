#pragma once

namespace mut
{
    //
    // integer part
    //
    static inline
    int ipartv( const float x )
    {
        return (int)x;
    }

    //
    // clamp to range
    //
    template < typename T >
    static inline
    T clampv( const T min, const T cur, const T max )
    {
        if ( cur < min ) return min;
        if ( cur > max ) return max;
                         return cur;
    }

    //
    // absolute value   
    // 
    template < typename T >
    static inline
    T absv( const T a )
    {
        if ( a < 0 )
            return -a;
        return  a;
    }

    //
    // minimum of dual absolute values
    //
    template < typename T >
    static inline
    T absminv( T a, T b )
    {
        if ( a < 0 ) a = -a;
        if ( b < 0 ) b = -b;
        if ( a < b ) return a;
        else         return b;
    }

    //
    // select once of two values with the smallest magnitude
    //
    template < typename T >
    static inline
    T minmagv( T a, T b )
    {
        T i = a;
        T j = b;
        if ( a < 0 ) a = -a;
        if ( b < 0 ) b = -b;
        if ( a < b ) return i;
        else         return j;
    }

    //
    // minimal value
    //
    template < typename T >
    static inline
    T minv( const T a, const T b )
    {
        if ( a < b ) return a;
        else         return b;
    }

    //
    // maximal value
    //
    template < typename T >
    static inline
    T maxv( const T a, const T b )
    {
        if ( a > b ) return a;
        else         return b;
    }

    //
    // take sign of a value
    //
    template < typename T >
    static inline
    T signv( const T a )
    {
        if ( a < 0 )
            return (T)(-1);
            return (T)( 1);
    }

    //
    // fast reciprical square route
    //
    extern
    float fast_rsqrtf( float in );

    //
    // fast reciprical square route
    //
    static inline
    float fast_sqrtf( float in )
    {
        return 1.f / fast_rsqrtf( in );
    }

    //
    // sin and cosine
    //
    extern float sin( float a );
    extern float cos( float a );
    extern float tan( float a );
    
    //
    // linear interpolation
    //
    static inline
    float interp_linear( float a, float b, float x )
    {
        return a*(1.f-x) + b*x;
    }

    //
    // cosine interpolation
    //
    static inline
    float interp_cosine( float a, float b, float x )
    {
              x -= .5f;
        float z  = mut::absv<float>( x ); // (x<0.f) ? -x : x;
              x  = x - x * z + .25f;
        return a*(1.f-x) + b*x;
    }

    //
    // cubic interpolation
    // 0     1 --- 2     3
    //
    static inline
    float interp_cubic( float v[4], float x )
    {
        float &a=v[0], &b=v[1], &c=v[2], &d=v[3];

        float p = (d-c) - (a-b);
        return p*x*x*x + ((a-b)-p)*x*x + (c-a)*x + b;
    }

    //
    // cubic interpolation
    // 0     1 --- 2     3
    //
    static inline
    float interp_cubic_2( float v[4], float x )
    {
        float &a=v[0], &b=v[1], &c=v[2], &d=v[3];

        float x2 = x*x;
        float a0 = d-c - a+b;
        float a1 = a-b-a0;
        float a2 = c-a;
        float a3 = b;
   
        return a0*x*x2 + a1*x2 + a2*x + a3;
    }

    //
    // hermite iterpolation
    // 0     1 --- 2     3
    // 
    static inline
    float interp_hermite( float v[4], float x, float tension, float bias )
    {
        float &a=v[0], &b=v[1], &c=v[2], &d=v[3];

        float x2 = x * x;
        float x3 = x * x2;
        float xb = (1+bias)*(1-tension) * .5f;
        return (2*x3-3*x2+1)*b + ((x3-2*x2+x)*(a+c) + (x3-x2)*(b+d))*xb + (3*x2-2*x3)*c;
    }
    
    //
    // akima interpolation
    // 0    1    2 --- 3     4     5
    static inline
    float interp_akima( float v[6], float mu )
    {
        float num1 =
            absv( (v[4]-v[3]) - (v[3]-v[2]) ) * (v[2]-v[1]) +
            absv( (v[2]-v[1]) - (v[1]-v[0]) ) * (v[3]-v[2]);
        float den1 =
            absv( (v[4]-v[3]) - (v[3]-v[2]) ) +
            absv( (v[2]-v[1]) - (v[1]-v[0]) );
        float num2 = 
            absv( (v[5]-v[4]) - (v[4]-v[3]) ) * (v[3]-v[2]) +
            absv( (v[3]-v[2]) - (v[2]-v[1]) ) * (v[4]-v[3]);
        float den2 = 
            absv( (v[5]-v[4]) - (v[4]-v[3]) ) +
            absv( (v[3]-v[2]) - (v[2]-v[1]) );
    
        float t1 = 0.f;
        if ( den1 > 0.00001f )
            t1 = num1 / den1;
    
        float t2 = 0.f;
        if ( den2 > 0.00001f )
            t2 = num2 / den2;
    
        float C = 3*(v[3]-v[2]) - 2*t1 - t2;
        float D = t1 + t2 - 2*(v[3]-v[2]);
    
        return v[2] + (t1 + (C + D*mu)*mu)*mu;
    }

    // 
    // ease in and ease out curve
    // range 0 -> 1
    // 
    static inline
    float smoothstep( float x )
    {
        return x * x * (3 - 2 * x);
    }

}; /* namespace mut */
