/*
 *     _____          __  .__       ____ ___   __  .__.__  .__  __           ___________           .__   __   .__  __   
 *    /     \ _____ _/  |_|  |__   |    |   \_/  |_|__|  | |__|/  |_ ___.__. \__    ___/___   ____ |  | |  | _|__|/  |_ 
 *   /  \ /  \\__  \\   __\  |  \  |    |   /\   __\  |  | |  \   __<   |  |   |    | /  _ \ /  _ \|  | |  |/ /  \   __\
 *  /    Y    \/ __ \|  | |   Y  \ |    |  /  |  | |  |  |_|  ||  |  \___  |   |    |(  <_> |  <_> )  |_|    <|  ||  |  
 *  \____|__  (____  /__| |___|  / |______/   |__| |__|____/__||__|  / ____|   |____| \____/ \____/|____/__|_ \__||__|  
 *          \/     \/          \/                                    \/                                      \/        
 *
 */
#pragma once

namespace mut
{
    // forward declares
    struct vec2;
    struct ray2;
    struct box2;
    struct mat2;
    struct edge2;
    struct circle2;
    struct plane2;

    //
    // 2d vector
    //
    struct vec2
    {
        vec2( void )
        {}

        vec2( const vec2 & a )
            : x( a.x ), y( a.y )
        {}

        vec2( float ax, float ay )
            : x(ax), y(ay)
        {}

        // vector from angle
        vec2( float a );

        void operator += ( const vec2 &a )
        { x += a.x; y += a.y; }
    
        void operator -= ( const vec2 &a )
        { x -= a.x; y -= a.y; }
    
        void operator *= ( const float a )
        { x *= a; y *= a; }

	    float x, y;
    };

    // 
    // 2d ray
    // 
    struct ray2
    {
        ray2( void )
        {}

        ray2( const vec2 &ap, const vec2 &ad )
            : p( ap ), d( ad )
        {}

	    vec2 p, d;
    };

    // 
    // 2d box
    //
    struct box2
    {
        box2( void )
        {}

        box2( float x1, float y1, float x2, float y2 )
            : minp( x1, y1 )
            , maxp( x2, y2 )
        {}

        box2( const vec2 &a, const vec2 &b )
            : minp( a )
            , maxp( b )
        {}

	    vec2 minp, maxp;
    };

    //
    // 2d matrix
    //
    struct mat2
    {
        mat2( void )
        {}
        
        mat2( float a, float b, float c, float d )
        { 
            e[0]=a; e[1]=b; e[2]=c; e[3]=d;
        }

        float e[ 4 ];
    };

    //
    // 2d edge
    //
    struct edge2
    {
        edge2( void )
        {}

        edge2( const vec2 &aa, const vec2 &ab )
            : a( aa ), b( ab )
        {}

        vec2 a, b;
    };

    //
    // 
    //
    struct circle2
    {
        circle2( void )
        {}

        circle2( const vec2 &ac, float ar )
            : c( ac )
            , r( ar )
        {}

        // find circumcircle
        circle2( const vec2 &a, const vec2 &b, const vec2 &c );

        vec2  c; // center
        float r; // radius
    };

    //
    // 2d plane
    //
    struct plane2
    {
        plane2( void )
        {}

        plane2( const vec2 &an, const float ad )
            : normal( an ), d( ad )
        {}

        vec2  normal;   // normal
        float d;        // distance

        //
        // in mut_2d_ctor.h
        //
        
        // create a plane from two points
        plane2( const vec2 &a, const vec2 &b );

    };

};
