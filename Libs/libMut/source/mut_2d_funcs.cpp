/*
 *     _____          __  .__       ____ ___   __  .__.__  .__  __           ___________           .__   __   .__  __   
 *    /     \ _____ _/  |_|  |__   |    |   \_/  |_|__|  | |__|/  |_ ___.__. \__    ___/___   ____ |  | |  | _|__|/  |_ 
 *   /  \ /  \\__  \\   __\  |  \  |    |   /\   __\  |  | |  \   __<   |  |   |    | /  _ \ /  _ \|  | |  |/ /  \   __\
 *  /    Y    \/ __ \|  | |   Y  \ |    |  /  |  | |  |  |_|  ||  |  \___  |   |    |(  <_> |  <_> )  |_|    <|  ||  |  
 *  \____|__  (____  /__| |___|  / |______/   |__| |__|____/__||__|  / ____|   |____| \____/ \____/|____/__|_ \__||__|  
 *          \/     \/          \/                                    \/                                      \/        
 *
 */
#include "mut_1d_types.h"
#include "mut_2d_funcs.h"

// ray box intersection test
// 0 = no hit
// 1 = xaxis hit
// 2 = yaxis hit
int mut::intersect( const ray2 &ray, const box2 &box, vec2 &hit )
{
    float dx, dy;
    vec2  tx, ty;

    // find quadrant
    switch ( mut::quadrantCode( ray.d ) )
    {
    case ( 0 ): // --
        // distance to box planes
        dx = ray.p.x - box.maxp.x;
        dy = ray.p.y - box.maxp.y;
        // find plane intersection points
        tx = vec2( ray.p.x - (dy / ray.d.y) * ray.d.x, box.maxp.y );
        ty = vec2( box.maxp.x, ray.p.y - (dx / ray.d.x) * ray.d.y );
        break;
        
    case ( 1 ): // +-
        // distance to box planes
        dx = box.minp.x - ray.p.x;
        dy = ray.p.y - box.maxp.y;
        // find plane intersection points
        tx = vec2( ray.p.x - (dy / ray.d.y) * ray.d.x, box.maxp.y );
        ty = vec2( box.minp.x, ray.p.y + (dx / ray.d.x) * ray.d.y );
        break;

    case ( 2 ): // -+
        // distance to box planes
        dx = ray.p.x - box.maxp.x;
        dy = box.minp.y - ray.p.y;
        // find plane intersection points
        tx = vec2( ray.p.x + (dy / ray.d.y) * ray.d.x, box.minp.y );
        ty = vec2( box.maxp.x, ray.p.y - (dx / ray.d.x) * ray.d.y );
        break;

    case ( 3 ): // ++
        // distance to box planes
        dx = box.minp.x - ray.p.x;
        dy = box.minp.y - ray.p.y;
        // find plane intersection points
        tx = vec2( ray.p.x + (dy / ray.d.y) * ray.d.x, box.minp.y );
        ty = vec2( box.minp.x, ray.p.y + (dx / ray.d.x) * ray.d.y );
        break;
    }
    // check for box intersection
    if ( tx.x >= box.minp.x && tx.x <= box.maxp.x )
    {
        hit = tx;
        return 1;
    }
    // check for box intersection
    if ( ty.y >= box.minp.y && ty.y <= box.maxp.y )
    {
        hit = ty;
        return 2;
    }
    // not overlap
    return 0;
}

// CS line clip codes
const int INSIDE    = 0;
const int LEFT      = 1;
const int RIGHT     = 2;
const int BOTTOM    = 4;
const int TOP       = 8;

// out code computation for the CS line clip algorythm
static inline
mut::u32 outCode( const mut::box2 &a, const mut::vec2 &b )
{
    mut::u32 code = INSIDE;
    if      (b.x < a.minp.x) code |= LEFT;
    else if (b.x > a.maxp.x) code |= RIGHT;
    if      (b.y < a.minp.y) code |= BOTTOM;
    else if (b.y > a.maxp.y) code |= TOP;
    return code;
}

// clip and edge to the inside of a box
bool mut::clip( const box2 &a, const edge2 &b, edge2 &out )
{
    // 
    float x0 = b.a.x; float y0 = b.a.y;
    float x1 = b.b.x; float y1 = b.b.y;
    // 
    u32 outcode0 = outCode( a, b.a );
    u32 outcode1 = outCode( a, b.b );
    bool accept = false;
    // 
    while ( true )
    {
        if (!(outcode0 | outcode1))
        {
            accept = true;
            break;
        }
        else if (outcode0 & outcode1)
            break;
        else
        {
            float x, y;
            u32 outcodeOut = outcode0 ? outcode0 : outcode1;

            if (outcodeOut & TOP   )
            {
                x = x0 + (x1 - x0) * (a.maxp.y - y0) / (y1 - y0);
                y = a.maxp.y;
            }
            else if (outcodeOut & BOTTOM)
            {
                x = x0 + (x1 - x0) * (a.minp.y - y0) / (y1 - y0);
                y = a.minp.y;
            }
            else if (outcodeOut & RIGHT )
            {
                y = y0 + (y1 - y0) * (a.maxp.x - x0) / (x1 - x0);
                x = a.maxp.x;
            }
            else if (outcodeOut & LEFT  )
            {
                y = y0 + (y1 - y0) * (a.minp.x - x0) / (x1 - x0);
                x = a.minp.x;
            }

            if (outcodeOut == outcode0)
            {
                x0 = x;
                y0 = y;
                outcode0 = outCode( a, vec2( x0, y0 ) );
            }
            else
            {
                x1       = x;
                y1       = y;
                outcode1 = outCode( a, vec2( x1, y1 ) );
            }
        }
    }
    if ( accept )
    {
        out = edge2( vec2( x0, y0 ), vec2( x1, y1 ) );
        return true;
    }
    return false;
}