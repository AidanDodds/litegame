/*
 *     _____          __  .__       ____ ___   __  .__.__  .__  __           ___________           .__   __   .__  __   
 *    /     \ _____ _/  |_|  |__   |    |   \_/  |_|__|  | |__|/  |_ ___.__. \__    ___/___   ____ |  | |  | _|__|/  |_ 
 *   /  \ /  \\__  \\   __\  |  \  |    |   /\   __\  |  | |  \   __<   |  |   |    | /  _ \ /  _ \|  | |  |/ /  \   __\
 *  /    Y    \/ __ \|  | |   Y  \ |    |  /  |  | |  |  |_|  ||  |  \___  |   |    |(  <_> |  <_> )  |_|    <|  ||  |  
 *  \____|__  (____  /__| |___|  / |______/   |__| |__|____/__||__|  / ____|   |____| \____/ \____/|____/__|_ \__||__|  
 *          \/     \/          \/                                    \/                                      \/        
 *
 */
#pragma once

namespace mut
{
    //
    // 3d vector
    //
    struct vec3
    {
        vec3( void )
        {}

        vec3( const vec3 & a )
            : x( a.x ), y( a.y )
        {}

        vec3( float ax, float ay, float az )
            : x(ax), y(ay), z(az)
        {}

	    float x, y, z;
    };

    // 
    // 3d ray
    // 
    struct ray3
    {
        ray3( void )
        {}

        ray3( const vec3 &ap, const vec3 &ad )
            : p( ap ), d( ad )
        {}

	    vec3 p, d;
    };

    // 
    // 3d box
    //
    struct box3
    {
        box3( void )
        {}

        box3( const vec3 &a, const vec3 &b )
            : minp( a ), maxp( b )
        {}

	    vec3 minp, maxp;
    };

    //
    // 3d matrix
    //
    struct mat3
    {
        mat3( void )
        { }

        float e[ 9 ];
    };

    //
    //
    //
    struct mat4
    {
        mat4( void )
        {}

        // matrix from vectors
        mat4( 
            const mut::vec3 x, 
            const mut::vec3 y, 
            const mut::vec3 z )
        {
            e[ 0] = x.x; e[ 1] = x.y; e[ 2] = x.z; e[ 3] = 0.f;
            e[ 4] = y.x; e[ 5] = y.y; e[ 6] = y.z; e[ 7] = 0.f;
            e[ 8] = z.x; e[ 8] = z.y; e[ 9] = z.z; e[10] = 0.f;
            e[11] = 0.f; e[12] = 0.f; e[13] = 0.f; e[14] = 1.f;
        }

        // matrix from vectors
        mat4( 
            const mut::vec3 x, 
            const mut::vec3 y, 
            const mut::vec3 z,
            const mut::vec3 p )
        {
            e[ 0] = x.x; e[ 1] = x.y; e[ 2] = x.z; e[ 3] = 0.f;
            e[ 4] = y.x; e[ 5] = y.y; e[ 6] = y.z; e[ 7] = 0.f;
            e[ 8] = z.x; e[ 8] = z.y; e[ 9] = z.z; e[10] = 0.f;
            e[11] = p.x; e[12] = p.y; e[13] = 0.f; e[14] = 1.f;
        }

        float e[16];
    };

    //
    //
    //
    struct plane3
    {
        vec3  normal;
        float d;
    };

    //
    //
    //
    struct sphere3
    {
        vec3  c;
        float r;
    };

};
