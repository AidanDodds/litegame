/*
 *     _____          __  .__       ____ ___   __  .__.__  .__  __           ___________           .__   __   .__  __   
 *    /     \ _____ _/  |_|  |__   |    |   \_/  |_|__|  | |__|/  |_ ___.__. \__    ___/___   ____ |  | |  | _|__|/  |_ 
 *   /  \ /  \\__  \\   __\  |  \  |    |   /\   __\  |  | |  \   __<   |  |   |    | /  _ \ /  _ \|  | |  |/ /  \   __\
 *  /    Y    \/ __ \|  | |   Y  \ |    |  /  |  | |  |  |_|  ||  |  \___  |   |    |(  <_> |  <_> )  |_|    <|  ||  |  
 *  \____|__  (____  /__| |___|  / |______/   |__| |__|____/__||__|  / ____|   |____| \____/ \____/|____/__|_ \__||__|  
 *          \/     \/          \/                                    \/                                      \/        
 *
 */
#pragma once

// config
#define USE_1D 1
#define USE_2D 1
#define USE_3D 1

// common
#include "source/mut_common.h"
#include "source/mut_int_types.h"

// 1d stuff
#if USE_1D

    #include "source/mut_1d_funcs.h"

#endif

// 2d stuff
#if USE_2D

    // contains the basic type definitions
    # include "source/mut_2d_types.h"

    // contains overridden operators
    # include "source/mut_2d_ops.h"

    // contains functions for operating on basic types
    # include "source/mut_2d_funcs.h"

    // contains specialist constructors for constructing
    // basic types from one another
    # include "source/mut_2d_ctor.h"

#endif

// 3d stuff
#if USE_3D

    // contains the basic type definitions
    # include "source/mut_3d_types.h"

    // contains overridden operators
    # include "source/mut_3d_ops.h"

    // contains functions for operating on basic types
    # include "source/mut_3d_funcs.h"

    // contains specialist constructors for constructing
    // basic types from one another
    # include "source/mut_3d_ctor.h"

#endif

// random noise generators
#include "source/mut_rand.h"