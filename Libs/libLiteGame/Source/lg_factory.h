#pragma once

#include "../../libUtility/source/lg_stringRef.h"

#include "lg_entity.h"

namespace lg_factory
{

    // callbacks used by the factory
    typedef lg_cEntity* (*cbCreate  )( void         );
    typedef void        (*cbDestroy )( lg_cEntity * );

    struct sTraverseParams
    {
        sTraverseParams( void )
            : flags( 0 )
            , area( )
        { }

        int   flags;
//      const lg_cStringRef & name;
        recti area;
    };

    typedef void (*cbTraverse)( lg_cEntity *, const sTraverseParams & );

    class cCollector
    {
    public:

        virtual bool add     ( lg_cEntity *entity ) = 0;

        // traverse all items in the collection
        virtual void traverse( cbTraverse callback, const sTraverseParams &param ) = 0;
        
        // free all items in the collection
        virtual void purge   ( void ) = 0;

        // release all dead entities
        virtual void clean   ( void ) = 0;
    };

	// info : set the function that is called, allowing a module
	//		  to receive an entity when it is created.  this is
	//		  essential for storing entities as they are created.
	extern void setCollector
	(
		cCollector &collector
	);

	// info : add a creator to the factory
	extern void addCreator
	(
	    const lg_sRTTI &type   ,
		cbCreate        create ,
		cbDestroy       destroy
	);

	// info : create an entity at a given position
	extern bool create
	(
	    const lg_sRTTI &type,
		mut::vec2       pos
	);

    // create an entity and place a reference in ref
    extern bool create
    (
        const lg_sRTTI &type,
        mut::vec2       pos,
        lg_cEntityRef  &ref
    );

    // info : destroy an entity, or should we store
	extern void destroy
	(
		lg_cEntity *e
	);

    // factory as a module
    namespace module
    {
        bool onStart( void );
        bool onTick ( void );
        void onStop ( void );
    };

}; // namespace factory

typedef lg_factory::cCollector      lg_cCollector;
typedef lg_factory::cbTraverse      lg_cbTraverse;
typedef lg_factory::sTraverseParams lg_sTraverseParams;
