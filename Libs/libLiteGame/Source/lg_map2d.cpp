#include "lg_map2d.h"

#include "../../libUtility/source/lg_assert.h"
#include "../../libMut/mut.h"

// easy power of two macro
#define POT( X ) ( 1 << (X) )

const int cQLower = POT( 0 );	// collision codes
const int cQUpper = POT( 1 );
const int cQLeft  = POT( 2 );
const int cQRight = POT( 3 );
const int cWall   = POT( 4 );	// tile is a collider

// index a map in a clean way
static byte & _index( const lg_map2d::info_t &map, s32 x, s32 y )
{
	bool inMap = x>=0 && y>=0 && x<(s32)map.w && y<(s32)map.h;
	lg_assert( inMap, "index out of bounds" );
	return	map.mask[ x + y * map.w ];
}

// check if a tile is a collider
static bool _isWall( const lg_map2d::info_t &map, int x, int y )
{
	return (_index( map, x, y ) & cWall) > 0;
}

// preprocess a map with collision flags per-tile
void lg_map2d::preprocess( lg_map2d::info_t &map )
{
	lg_assert( map.mask != nullptr, "tile data doesnt exist" );

	// mask out all push data
	for ( u32 i=0; i<map.w * map.h; i++ )
		// push data is in lower four bits
		map.mask[ i ] &= 0xF0;

	// vertical process
	for ( u32 x=0; x < map.w; x++ )
	{
		// check if is wall
		int prev = _isWall( map, x, 0 );

		//
		for ( u32 y=1; y < map.h; y++ )
		{
			// sample the current tile
			int cur = _isWall( map, x, y );
			// check for a wall gradient
			switch (cur - prev)
			{
			case ( -1 ): _index( map, x, y-1 ) |= cQLower; break;
			case (  1 ): _index( map, x, y   ) |= cQUpper; break;
			}
			// 
			prev = cur;
		}
	}

	// horozontal process
	for ( u32 y=0; y < map.h; y++ )
	{
		// check if is wall
		int prev = _isWall( map, 0, y );
		//
		for ( u32 x=1; x < map.w; x++ )
		{
			// sample the current tile
			int cur = _isWall( map, x, y );
			// check for a wall gradient
			switch (cur - prev)
			{
			case ( -1 ): _index( map, x-1, y ) |= cQRight; break;
			case (  1 ): _index( map, x  , y ) |= cQLeft ; break;
			}
			// 
			prev = cur;
		}
	}
}

//
bool lg_map2d::collide( const lg_map2d::info_t &map, lg_map2d::aabb_t &aabb )
{
	float _guard_ = 100000.0f;

	lg_assert( map.mask != nullptr, "tile data doesnt exist" );
	lg_assert( aabb.x2>aabb.x1 || aabb.y2>aabb.y1, "invalid aabb" );

	// XXX: todo: convert all collision code to map space (not in tile units)

	// simplify the tile size variable
	float ts = map.tileSize;
	
	// convert into clipped map space
	s32 x1 = mut::clampv<s32>( 0, (s32)( aabb.x1 / ts ), map.w - 1 );
	s32 x2 = mut::clampv<s32>( 0, (s32)( aabb.x2 / ts ), map.w - 1 );
	s32 y1 = mut::clampv<s32>( 0, (s32)( aabb.y1 / ts ), map.h - 1 );
	s32 y2 = mut::clampv<s32>( 0, (s32)( aabb.y2 / ts ), map.h - 1 );
	
	// aabb displacement vector
	float dp_x = _guard_;
	float dp_y = _guard_;

	// 2D loop over the tiles covered by the AABB
	for ( s32 y=y1; y<=y2; y++ )
		for ( s32 x=x1; x<=x2; x++ )
		{
			// extract this tile
			byte tile = _index( map, x, y );

			// skip if not a blocking tile
			if ( (tile & cWall) == 0 )
				continue;

			// 
			float quad[4] = { _guard_, _guard_, _guard_, _guard_ };

			// find all of the splits
			if ( (tile & cQUpper) != 0 ) quad[0] = (float) (y   *ts) - aabb.y2;
			if ( (tile & cQLower) != 0 ) quad[1] = (float)((y+1)*ts) - aabb.y1;
			if ( (tile & cQLeft ) != 0 ) quad[2] = (float) (x   *ts) - aabb.x2;
			if ( (tile & cQRight) != 0 ) quad[3] = (float)((x+1)*ts) - aabb.x1;

			// select each quadrant from comparison of their abs min
			quad[0] = mut::minmagv<float>( quad[0], quad[1] );
			quad[2] = mut::minmagv<float>( quad[2], quad[3] );

			// select the smallest split axis from this tile
            if ( mut::absv<float>( quad[2] ) < mut::absv<float>( quad[0] ) )
			{
				// x - axis
				// save if smaller then our current split
				if ( mut::absv<float>( quad[2] ) < mut::absv<float>( dp_x ) )
					dp_x = quad[2];
			}
			else
			{
				// y - axis
				// save if smaller then our current split
				if ( mut::absv<float>( quad[0] ) < mut::absv<float>( dp_y ) )
					dp_y = quad[0];
			}
		}

	// resolve a split on any axis
	if ( dp_x != _guard_ ) aabb.rx = dp_x; else aabb.rx = 0.0f;
	if ( dp_y != _guard_ ) aabb.ry = dp_y; else aabb.ry = 0.0f;

	// did we have a split
	return (dp_x != _guard_) || (dp_y != _guard_);
}

// 
void lg_map2d::create
    ( lg_map2d::info_t &map, const mut::vec2i &size, u32 tileSize )
{
	lg_assert( tileSize > 0, "avoid later division by zero" );

	// find the map area
	u32 area = size.x * size.y;
	lg_assert( area > 0, "invalid map area" );

	// allocate the tile mask
	map.mask = new byte[ area ];
	lg_assert( map.mask, "unable to allocate memory" );

	// clear the mask
	for ( u32 i=0; i<area; i++ )
		map.mask[ i ] = 0;

	// save related map info
	map.w = size.x;
	map.h = size.y;
	map.tileSize = tileSize;
}

// release memory taken up by the map
void lg_map2d::release
    ( lg_map2d::info_t &map )
{
	lg_assert( map.mask != nullptr, "check map has been allocated" );

	// release tile data
	delete [] map.mask;
	map.mask = nullptr;

	// clear variables for good mesure
	map.w = 0;
	map.h = 0;
	map.tileSize = 0;
}

// raycast needs this function to report that out of map returns true
bool lg_map2d::isWall
    ( lg_map2d::info_t &map, const mut::vec2i &p )
{
	if ( p.x<0 || p.y<0 || p.x>=(s32)map.w || p.y>=(s32)map.h )
		return true;

	return _isWall( map, p.x, p.y );
}

void lg_map2d::setWall
    ( lg_map2d::info_t &map, const mut::vec2i &p, bool add )
{
	if ( p.x<0 || p.y<0 || p.x>=(s32)map.w || p.y>=(s32)map.h )
		return;

	// lookup the specific tile
	byte &tile = _index( map, p.x, p.y );

	// add or remove the wall as desired
	if ( add == true )	tile |=  cWall;
	else				tile &= ~cWall;
}


// can cause gaps and misses when dx and dy are both negative?
bool lg_map2d::raycast
    ( lg_map2d::info_t &map, const mut::vec2 &a, const mut::vec2 &b, lg_map2d::rayHit_t &hit )
{
    float x1 = a.x; float y1 = a.y;
    float x2 = b.x; float y2 = b.y;

	// ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- SETUP
	// starting positions
	float x_px = x1, x_py = y1;
	float y_px = x1, y_py = y1;
	// direction vector
	float dx = x2-x1, dy = y2-y1;

	float hx1 = 0, hy1 = 0;
	float hx2 = 0, hy2 = 0;

	int    ix = 0, iy  = 0;

	int _1x, _1y;
	int _2x, _2y;

	// ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- Y AXIS INTERSECTIONS
	if ( dy > 0.0f )
	{
		//
		float yp = 0;
		if ( dy > 0.f ) yp = dx / dy;
		// grid align
		y_px += ( mut::ipartv( y_py + 1 ) - y_py ) * yp;
		iy    = (int)(y1 + 1);

		for ( ;; )
		{
			// check for hit
			if ( lg_map2d::isWall( map, mut::vec2i( (int)y_px, iy ) ) )
			{
				_1x = (int)y_px;
				_1y = iy;
				// save the hit point
				hx1  = y_px;
				hy1  = (float)iy;
				y_py = (float)iy;
				break;
			}
			// step
			y_px += yp;
			iy   += 1;
		}
	}
	else // if ( dy < 0.0f )
	{	
		// check this when dx < 0.0f also

		float yp = dx / dy;
		// grid align
		y_px -= (y_py - mut::ipartv( y_py ) ) * yp;
		iy    = (int)(y_py);
		for ( ;; )
		{
			// check for hit
			if ( lg_map2d::isWall( map, mut::vec2i( (int)y_px, iy-1 ) ) )
			{
				_1x = (int)y_px;
				_1y = iy-1;
				// save the hit point
				y_py = (float)iy;
				hx1  = y_px;
				hy1  = (float)iy;
				break;
			}
			// step
			y_px -= yp;
			iy   -= 1;
		}
	}

	// ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- X AXIS INTERSECTIONS
	if ( dx > 0.0f )
	{	
		float xp = 0;
		if ( dx > 0 ) xp = dy / dx;
		// grid align
		x_py += ( mut::ipartv( x_px + 1 ) - x_px ) * xp; 
		ix    = (int)(x_px+1);
		for ( ;; )
		{
			// check for hit
			if ( lg_map2d::isWall( map, mut::vec2i( ix, (int)x_py ) ) )
			{
				_2x = ix;
				_2y = (int)x_py;
				// save the hit point
				x_px = (float)ix;
				hx2  = (float)ix;
				hy2  = x_py;
				break;
			}
			// step
			ix   += 1;
			x_py += xp;
		}
	}
	else // if ( dx < 0.0f )
	{
		// check this when dy < 0.0f also

		float xp = dy / dx; // two negs can make a positive here
		// grid align
		x_py -= (x_px - mut::ipartv( x_px )) * xp;
		ix    = (int)x_px;
		for ( ;; )
		{
			// check for hit
			if ( lg_map2d::isWall( map, mut::vec2i( ix-1, (int)x_py ) ) )
			{
				_2x = ix-1;
				_2y = (int)x_py;
				// save the hit point
				x_px = (float)ix;
				hx2  = x_px;
				hy2  = x_py;
				break;
			}
			// step
			ix   -= 1;
			x_py -= xp;
		}
	}

	// ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- INTERSECTION SELECTION
	float dx1 = (x_px-x1) * (x_px-x1);
	float dy1 = (x_py-y1) * (x_py-y1);
	float dx2 = (y_px-x1) * (y_px-x1);
	float dy2 = (y_py-y1) * (y_py-y1);

	//
	if ( (dx1+dy1) < (dx2+dy2) )
	{
		hit.hx = hx2;
		hit.hy = hy2;

		hit.tx = _2x;
		hit.ty = _2y;

		hit.face = 0 + ( dx > 0 );
	}
	else
	{
		hit.hx = hx1;
		hit.hy = hy1;

		hit.tx = _1x;
		hit.ty = _1y;
		
		hit.face = 2 + ( dy > 0 );
	}

	return true;
}

// check a line of sight between two points
extern bool lg_map2d::lineOfSight
    ( lg_map2d::info_t &map, const mut::vec2 &a, const mut::vec2 &b )
{
    float x1 = a.x; float y1 = a.y;
    float x2 = b.x; float y2 = b.y;

	lg_map2d::rayHit_t hit;
	lg_map2d::raycast( map, a, b, hit );

    if ( y1 < y2 )
	{
		if ( x1 < x2 )
		{
			if ( (hit.hy >= y1) &&
				 (hit.hy <= y2) &&
				 (hit.hx >= x1) &&
				 (hit.hx <= x2) )
				return false;
		}
		else
		{
			if ( (hit.hy >= y1) &&
				 (hit.hy <= y2) &&
				 (hit.hx >= x2) &&
				 (hit.hx <= x1) )
				return false;
		}
	}
	else
	{
		if ( x1 < x2 )
		{
			if ( (hit.hy >= y2) &&
				 (hit.hy <= y1) &&
				 (hit.hx >= x1) &&
				 (hit.hx <= x2) )
				return false;
		}
		else
		{
			if ( (hit.hy >= y2) &&
				 (hit.hy <= y1) &&
				 (hit.hx >= x2) &&
				 (hit.hx <= x1) )
				return false;
		}
	}
	return true;
}