#pragma once

#include "../../libUtility/source/lg_types.h"
#include "../../libUtility/source/lg_stringRef.h"
#include "../../libUtility/source/lg_file.h"

#include "../../libMut/mut.h"

#include "lg_spritePlan.h"

namespace lg_sprite
{
	// this class is designed to be derived from by the
	// cEntity class making it easy to register new sprite
	// handlers
	class cHandler
	{
	public:

		virtual void spriteHandler( class cSprite *spr, byte signal ) = 0;

	};

	// 
	struct sDrawInfo
	{
        // this pointer doesnt seem great :(
        // how to make into reference?
		lg_cImage  *image;
		mut::vec2i  offset;
		recti       frame;
	};

	// this class stores an instance of a sprite
	class cSprite
	{
	public:

        cSprite( void )
            : plan( )
            , code( nullptr )
            , hotspot( 0, 0 )
            , offset ( 0, 0 )
            , blocked( false )
            , frame  ( 0 )
            , delay  ( 0 )
            , pc     ( 0 )
            , handler( nullptr )
            , speed  ( 5 )
        { }

		// play a specific animation sequence
		bool play( const lg_cStringRef &anim_name, int angle );
	    
		// 
		void tick( void );
		
		// 
		bool      visible;  // 
        bool      flip;     // flip in x axis
		cHandler *handler;	// the sprite callback handler
        int       speed;

        // set the current sprite basis
        bool setPlan( lg_sSpritePlan * plan );

		// get the current render state
		bool getDrawInfo( sDrawInfo & info );

/*
        lg_cImage *getImage( void )
        {
            if ( plan == nullptr )
                return nullptr;
            return &plan->image;
        }
*/

	private:

		// the sprite state
		byte  *code;    // program
		byte   pc;      // program counter
		byte   frame;   // current animation frame
        byte   sheet;   
		byte   delay;   // delay before next opcode runs
        
        mut::vec2i hotspot;
		mut::vec2i offset;
		bool blocked;

        // the master plan for this sprite
        lg_sSpritePlan * plan;

	};

    // create a sprite
	bool create ( const lg_cStringRef &name, cSprite & spr );
	bool release( const cSprite & sprite );

    // sprite module
    namespace module
    {
        bool onStart( void );
        bool onTick ( void );
        void onStop ( void );
    };

}; // namespace lg_sprite

typedef lg_sprite::cSprite   lg_cSprite;
typedef lg_sprite::cHandler  lg_cSpriteHandler;
typedef lg_sprite::sDrawInfo lg_sSpriteDrawInfo;