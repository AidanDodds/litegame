#include <stdio.h>

#include "../../libMut/mut.h"

#include "../../libUtility/source/lg_log.h"
#include "../../libUtility/source/lg_stack.h"
#include "../../libUtility/source/lg_dir.h"

#include "lg_sprite.h"
#include "lg_factory.h"

namespace lg_sprite
{

// create a sprite
bool create( const lg_cStringRef & name, cSprite & spr )
{
    lg_sSpritePlan * t = lg_spritePlan::find( name );
    if ( t == nullptr )
    	return false;

    // pass plan to the sprite
    return spr.setPlan( t );
}

bool release( const cSprite & sprite )
{
	return false;
}

bool cSprite::setPlan( lg_sSpritePlan * aPlan )
{
    if ( aPlan == nullptr )
        return false;

    plan = aPlan;
    // default the the first animation
    code    = aPlan->anim[0]->code;
    offset  = aPlan->offset;
    blocked = false;
    hotspot = mut::vec2i( 0, 0 );
    delay   = 0;
    frame   = 0;
    visible = true;
    flip    = false;

    return true;
}

bool cSprite::play( const lg_cStringRef & name, int angle )
{
    if ( plan == nullptr )
        return false;

    // 
    lg_sSpriteAnim * anim = nullptr;

	// search for the requested animation
    for ( int i=0; i<plan->anim.size( ); i++ )
    {
        anim = plan->anim[i];
        // anim is invalid
        if ( anim == nullptr )
            continue;
        // found so break
        if ( anim->name == name )
            if ( anim->angle == angle )
                break;
        // didnt find any so wipe
        anim = nullptr;
    }
    // return false if we couldnt find the animation
    if ( anim == nullptr )
        return false;

    if ( anim->code == nullptr )
        return false;

    // is this the animation we are already playing
    if ( anim->code == code )
        return true;

	// set spr.code to anim bytecode
    code    = anim->code;
    pc      = 0;
    delay   = 0;
    frame   = 0;
    sheet   = 0;
    blocked = false;
    visible = true;

    // execute the code till we have synced with the first frame
    tick( );

    return true;
}

void cSprite::tick( void )
{
    // are we in a delay phase
    if ( delay > 0 )
    {
        delay--;
        return;
    }
    
    // q = infinite loop breaker
    int q = 100;
    while ( delay <= 0 && (--q>0) )
    {

        // execute the next opcode
	    switch ( code[ pc++ ] )
	    {

	    // delay before moving on to next frame
        case ( lg_spritePlan::OP_DELAY ):
		    delay += code[ pc++ ];
            break;
            
	    // set the sprite offset
	    case ( lg_spritePlan::OP_OFFSET ):
		    offset.x = code[ pc++ ];
		    offset.y = code[ pc++ ];
		    break;

	    // cause a signal interupt
	    case ( lg_spritePlan::OP_SIGNAL ):
            // make sure we have a handler before we call it
		    if ( handler != nullptr )
            {
                byte *_code = code;
			    handler->spriteHandler( this, code[ pc++ ] );
                if ( code != _code )
                    return;
            }
            // we still have to skip over the operand
            else pc++;
		    break;
       
        // loop code
        case ( lg_spritePlan::OP_LOOP ):
            pc = 0;
            break;

	    // end of animation, do nothing
	    case ( lg_spritePlan::OP_END ):
            // unblock once animation has ended
		    blocked = false;
            // undo the previous pc++
            pc--;
            // make sure we have a handler before we call it
		    if ( handler != nullptr )
            {
                byte *_code = code;
			    handler->spriteHandler( this, 0xff );
                if ( code != _code )
                    return;

                delay += speed;
            }
		    break;

        // set the current frame
        case ( lg_spritePlan::OP_FRAME ):
		    frame  = code[ pc++ ];
            delay += speed;
		    break;

        case ( lg_spritePlan::OP_NEXT ):
            frame += 1;
            delay += speed;
            break;

        case ( lg_spritePlan::OP_SPEED ):
            speed  = code[ pc++ ];
            // dont let speed be 0
            speed += (speed == 0);
            break;

        case ( lg_spritePlan::OP_SHEET ):
            sheet = code[ pc++ ];
            break;

	    default:
            lg_assert(! "unknown animation opcode" );
            break;

	    } // switch ( )
     } // while ( )
}

bool cSprite::getDrawInfo( sDrawInfo & info )
{
	if ( plan == nullptr )
		return false;

	// get out the image data
    lg_assert( sheet >= 0 && sheet < plan->sheet.size() );
	info.image = plan->sheet[ sheet ];
    lg_assert( info.image != nullptr );

	// save the sprite offset
	info.offset = offset;

	//
	mut::vec2i imgSize = info.image->getSize( );

	int xtiles = imgSize.x / plan->frame.x;
	lg_assert( xtiles != 0 );
	
	// 
	info.frame.x1 = (frame % xtiles) * plan->frame.x;
	info.frame.y1 = (frame / xtiles) * plan->frame.y;

	// add in frame size
	info.frame.x2 = info.frame.x1 + plan->frame.x;
	info.frame.y2 = info.frame.y1 + plan->frame.y;
	
	// take intersection with overall sprite sheet
//	info.frame.overlap( blah );

	return true;
}

// lg_sprite::module::
namespace module
{
    static
    void loadSpritePlans( char *filePath )
    {
	    lg_cFile file( filePath );
	    if (! file.isOpen( ) )
		    return;

	    lg_log::print( "  loading '%s'", filePath );
	    lg_spritePlan::load( file );

	    return;
    }

    // sprite as a module
    bool module::onStart( void )
    {
        lg_dir::search( "sprites\\*.spr", loadSpritePlans );
	    return true;
    }

    bool module::onTick ( void )
    {
        return true;
    }

    void module::onStop ( void )
    {
    }

}; /* module */
}; /* lg_sprite */