#pragma once

#include "../../libUtility/source/lg_types.h"
#include "../../libUtility/source/lg_stringRef.h"

namespace lg_rtti
{

// name : struct sRTTI
// info : Run Time Type Information structure this
//		  structure helps up make fast comparisons
//		  and queries about an entities type, and
//		  the classes it derives from.
struct sRTTI
{
	sRTTI( const char *aName )
        : _rtti_name  ( aName   )
        , _rtti_parent( nullptr )
	{ }

	sRTTI( const char *aName, const sRTTI *aParent )
        : _rtti_name  ( aName   )
        , _rtti_parent( aParent )
	{ }
	
    //
    bool equals( const sRTTI &type ) const 
    {
		const sRTTI *node = this;
		while ( node != nullptr )
		{
			if ( type._rtti_name == node->_rtti_name )
				return true;
			node = node->_rtti_parent;
		}
		return false;
    }

	// return true if this RTTI is derived from the argument RTTI instance.
	bool operator == ( const sRTTI &type ) const
	{
        return equals( type );
	}

    //
    bool operator != ( const sRTTI &type ) const 
    {
        return !equals( type );
    }
	
	// get the name of this RTTI class as a cstring
	const lg_cStringRef &getName( void ) const
	{
		return _rtti_name;
	}

private:

	// this types name as a static string
	lg_cStringRef  _rtti_name;
	// the parent class that we have inherited from
	const sRTTI   *_rtti_parent;

};

}; // namespace lg_rtti

typedef lg_rtti::sRTTI lg_sRTTI;

// info : define two methods for implementing an RTTI
//		  interface within a class
#define _typeInfo( CLASS, PARENT )				\
	static const lg_sRTTI& RTTI( void )			\
	{											\
		static lg_sRTTI rtti( #CLASS, PARENT );	\
		return rtti;							\
	}											\
	virtual const lg_sRTTI& type( void )		\
	{											\
		return RTTI();							\
	}
