#include "lg_entity.h"
#include "lg_factory.h"

void lg_entity::update
(
	cEntity *entity,
	bool    &disposed
)
{
	disposed = false;

	if ( entity->references <= 0 )
	{
		// inform this entity that it will be destroyed
		entity->onDestroy( );

		// the factory should reclaim its memory
		lg_factory::destroy( entity );

		// signal that it was disposed of
		disposed = true;
	}
}