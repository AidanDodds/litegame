#include "../../libUtility/source/lg_log.h"
#include "../../libUtility/source/lg_memory.h"

#include "../liteGame.h"

#include "lg_factory.h"
#include "lg_sprite.h"

namespace lg_liteGame
{

    extern
    bool start( void )
    {	
        bool ok  = lg_memory ::start( );
             ok &= lg_factory::module::onStart( );
             ok &= lg_sprite ::module::onStart( );

	    return ok;
    }

    extern
    bool tick( void )
    {
        bool ok  = lg_factory::module::onTick( );
             ok &= lg_sprite ::module::onTick( );
        
        return ok;
    }
    
    extern
    void stop( void )
    {
	    lg_sprite ::module::onStop( );
	    lg_factory::module::onStop( );
        lg_memory ::stop( );
    }

}; // namespace liteGame