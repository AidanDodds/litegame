
#include "../../libUtility/source/lg_stringRef.h"
#include "../../libUtility/source/lg_log.h"
#include "../../libUtility/source/lg_memory.h"

#include "lg_factory.h"

namespace lg_factory
{

// info : this stores information about a single
//		  entity creator and can also form a linked
//		  list as part of the hash map
struct sCreator
{
	lg_cStringRef          name;
	// create and destroy entity
	lg_factory::cbCreate   create;
	lg_factory::cbDestroy  destroy;
	// buckets in the hash map form a linked list when they collide
	sCreator              *next;
};

// hash map of creator objects
static sCreator      * _map[ 256 ];
static lg_cCollector * _collector = nullptr;

// info : wrap a hash value into the range of
//		  the hash map 0 to 255
static u32 wrapIndex( u32 hash )
{
	// wrap into range
	hash ^= hash >>  8;
	hash ^= hash >>	16;
	hash ^= hash >>	24;
	// limit to range
    return hash & 0xFF;
}

static
sCreator* findCreator
(
	const lg_sRTTI &type
)
{
    const lg_cStringRef &name = type.getName();

	// find the hash map index
	u32 index = wrapIndex( name.hash() );

	// look for the creator with this name
	sCreator *obj = _map[ index ];
	while ( obj != nullptr )
	{
		if ( obj->name == name )
			return obj;
	}

	// no creator found with this name
	return nullptr;
}

// add a creator to the factory
extern
void addCreator
(
	const lg_sRTTI         &type  ,
	lg_factory::cbCreate   create ,
	lg_factory::cbDestroy  destroy
)
{
	// allocate a new creator structure
	sCreator * obj = lg_memory_new<sCreator>( true );
    obj->name      = type.getName();
	obj->create    = create;
	obj->destroy   = destroy;

	// find the hash map index
	u32 index = wrapIndex( obj->name.hash() );

	// insert into the hash map
	obj->next = _map[ index ];
	_map[ index ] = obj;
}

// internal create function
static
lg_cEntity * _create
(
	const lg_sRTTI &type,
	mut::vec2       pos 
)
{
	lg_assert( _collector != nullptr );

	// find the creator that has been named
	sCreator *creator = findCreator( type );
	if ( creator == nullptr )
		return nullptr;

	// ask the creator to construct this entity for us
	lg_cEntity *entity = creator->create( );

	// add one initial reference, since we assume that this entity is interested in its own
	// lifetime.  an entity must release its own reference when it wants to die.
	entity->references = 1;

    // set the position nievely to the created position.  onCreate may asjust this if needed.
    entity->pos = pos;

	// call on create before we add this entity to the collection so that it may update
    // its position as the collector may use spatial techniques
	if (! entity->onCreate( ) )
    {
        creator->destroy( entity );
        return nullptr;
    }

	// pass this entity to the collector
	if (! _collector->add( entity ) )
    {
        creator->destroy( entity );
        return nullptr;
    }

	return entity;
}

// create an entity at a given position
extern
bool create
(
	const lg_sRTTI &type,
	mut::vec2       pos
)
{
	return _create( type, pos ) != nullptr;
}

// create an entity at a given position
extern
bool create
(
	const lg_sRTTI &type,
	mut::vec2       pos ,
    lg_cEntityRef  &ref
)
{
	lg_cEntity *e = _create( type, pos );
    if ( e == nullptr )
        return false;
    // create an entity reference
    ref = lg_entity::sReference( e );
	return true;
}

//
extern
void setCollector
(
    cCollector &aCollector
)
{
	_collector = & aCollector;
}

// destroy
extern
void destroy( lg_cEntity *e )
{
	lg_assert( e != nullptr );

    sCreator *ec = findCreator( e->type() );
    lg_assert( ec != nullptr );

    // 
    e->onDestroy( );

	if ( ec->destroy )
		// pass this entity to the creator for destruction
		ec->destroy( e );
	
}

// lg_factory::module::
namespace module
{
    bool module::onStart( void )
    {
        return true;
    }

    bool module::onTick( void )
    {
        return false;
    }

    void module::onStop( void )
    {

    }
}; /* namespace module */ 

}; // namespace lg_factory
