#pragma once

#include "../../libUtility/source/lg_assert.h"
#include "../../libMut/mut.h"

#include "lg_rtti.h"

// 
namespace lg_entity
{

	//
	class cEntity
	{
	public:
        // static  const lg_sRTTI & RTTI( void );
        // virtual const lg_sRTTI & type( void );
		_typeInfo( cEntity, nullptr );

		// constructors and destructors
		cEntity( void ) : 
			pos( ),
			references(0)
		{ }

		virtual ~cEntity( ) { }

		// entity event handlers
		virtual bool onCreate ( void )  			  = 0;
		virtual void onTick   ( void )                = 0;
        virtual void onCollide( cEntity &other )	  = 0;
        virtual void onDestroy( void )                = 0;
	
		// get the exported entity state
		//   this idea comes from Game Programming Gems
		//   each class takes care to export a state, ie:
		//	- angry
		//	- hurt
		//	- hidden, etc...
		virtual int onGetState( void ) = 0;
	    
		// 
		int references;

		// entity location
		mut::vec2 pos;
	};

	// info : a helper class to manage entity references in a safe way.
	struct sReference
	{
		sReference( cEntity *e )
			: entity( e )
		{
            if ( entity != nullptr )
    			entity->references++;
		}

		sReference( const sReference &e )
            : entity( e.entity )
		{
			if ( entity )
				entity->references++;
		}

		sReference( void )
            : entity( nullptr )
        { }

		~sReference( )
		{
            if ( entity != nullptr )
    			entity->references--;
		}

        // gain a reference to an entity
        void aquire( cEntity *e )
        {
            release( );
            entity = e;
            if ( e != nullptr )
                e->references++;
        }

        // give up this reference
        void release( void )
        {
            if ( entity != nullptr )
                entity->references--;
            entity = nullptr;
        }

        // copy this reference
		void clone( sReference &ref )
        {
            ref.aquire( entity );
        }

        cEntity * get( void )
        {
            return entity;
        }

        // index this reference to get the entity object
		cEntity * operator -> ( void )
		{
            lg_assert( entity != nullptr );
			return entity;
		}

	private:
        // the entity object
		cEntity *entity;
	};

	// update the entity
	extern void update
	(
		cEntity *entity,
		bool    &dispose
	);

};

typedef lg_entity::cEntity    lg_cEntity;
typedef lg_entity::sReference lg_cEntityRef;