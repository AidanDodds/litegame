#pragma once

#include "lg_entity.h"
#include "lg_factory.h"

namespace lg_collector
{

    struct sBasicCollectorSearch
    {
        sBasicCollectorSearch( const lg_sRTTI &aType )
            : type( aType )
            , head( 0     )
        { }

        const lg_sRTTI &type;
        u32             head;
    };

    class cBasicCollector
        : public lg_cCollector
    {
    public:

        cBasicCollector( void )
            : items( 0 )
        { }

        virtual ~cBasicCollector( void )
        { }

        // 
        // lg_cCollector interface
        //

        virtual void traverse( lg_cbTraverse callback, const lg_sTraverseParams &param );
        virtual bool add     ( lg_cEntity *entity );
        virtual void purge   ( void );
        virtual void clean   ( void );

        // 
        // lg_cBasicCollector
        // 

        // tick all entities in the collection
        void tickAll        ( void );

        // run a search for a entity type
        lg_cEntity * find   ( sBasicCollectorSearch &search, bool restart );

        // sort objects by their z values
        void zSort          ( void );

        // return number of items in the collection
        u32 size( void )
        {
            return items;
        }

    private:

        static const int nMaxItems = 128;

        // simple entity list
        lg_cEntity* list[ nMaxItems ];
        int         items;

    };

}; // lg_collector

typedef lg_collector::cBasicCollector       lg_cBasicCollector;
typedef lg_collector::sBasicCollectorSearch lg_sBasicCollectorSearch;