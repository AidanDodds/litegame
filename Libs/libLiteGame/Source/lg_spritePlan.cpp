#include <stdio.h>

// 
#include "../../libUtility/source/lg_types.h"
#include "../../libUtility/source/lg_parser.h"
#include "../../libUtility/source/lg_stack.h"
#include "../../libUtility/source/lg_log.h"
#include "../../libUtility/source/lg_memory.h"

#include "lg_sprite.h"
#include "lg_spritePlan.h"

namespace lg_spritePlan
{

    static lg_cStack<lg_sSpritePlan> plans;

static
bool isValid( const lg_sSpritePlan * plan )
{
    bool valid = true;

    valid &= (plan->sheet.size() > 0);
    valid &= (plan->anim .size() > 0);
    valid &= (plan->name.get() != nullptr);
    return valid;
}

static
bool parse_info( lg_cParser &parser, lg_sSpritePlan * plan )
{
    while ( parser.isOk( ) )
    {
        if ( parser.found( "name" ) )
        {
            parser.read( plan->name );
            continue;
        }
        if ( parser.found( "mask" ) )
        {
            parser.read( plan->mask );
            continue;
        }
        if ( parser.found( "sheet" ) )
        {
            static char path[ 64 ];
            if (! parser.read( path, sizeof( path ) ) )
                return false;

            // create a new image class
            lg_cImage * image = lg_memory_new<lg_cImage>( true );
            if (! image->load( path ) )
                return false;

            // parser the frame
            parser.read( plan->frame.x );
            parser.expect( "," );
            parser.read( plan->frame.y );

            plan->sheet.push( image );

            continue;
        }
        break;
    }
    return parser.isOk( );
}

static
bool parse_anim( lg_cParser &parser, lg_sSpritePlan * plan )
{
    // create animation structure
    sAnim * anim = lg_memory_new<sAnim>( true );

	// info
    parser.expect( ":" );
	parser.read( anim->name );
    parser.expect( ":" );
	parser.read( anim->angle );
    
    // close erm
    parser.expect( ">" );

    // 
    int ix = 0;
    byte * code = anim->code;;

	// loop until the end of the 
	while ( parser.isOk() )
	{
        // dont overflow the code buffer
        if ( ix >= sizeof( anim->code ) )
        {
            parser.markFail();
            continue;
        }

		if ( parser.found( "offset" ) )
		{
            code[ix++] = lg_spritePlan::OP_OFFSET;
			code[ix++] = parser.readint();
			parser.expect( "," );
			code[ix++] = parser.readint();
		}
        else
		if ( parser.found( "speed" ) )
		{
            code[ix++] = lg_spritePlan::OP_SPEED;
			code[ix++] = parser.readint();
		}
        else
		if ( parser.found( "signal" ) )
		{
            code[ix++] = lg_spritePlan::OP_SIGNAL;
			code[ix++] = parser.readint();
        }
        else
		if ( parser.found( "frame" ) )
		{
            code[ix++] = lg_spritePlan::OP_FRAME;
			code[ix++] = parser.readint();
		}
        else
		if ( parser.found( "loop" ) )
        {
            code[ix++] = lg_spritePlan::OP_LOOP;
            break;
        }
        else
        if ( parser.found( "end" ) )
        {
            code[ix++] = lg_spritePlan::OP_END;
            break;
        }
        else
        if ( parser.found( "next" ) )
        {
            code[ix++] = lg_spritePlan::OP_NEXT;
        }
        else
		if ( parser.found( "sheet" ) )
		{
            code[ix++] = lg_spritePlan::OP_SHEET;
			code[ix++] = parser.readint();
		}

        // UNKNOWN ANIMATION INSTRUCTION
        else
            parser.markFail( );

	}

    // add this animation to the sprite plan
    if ( parser.isOk( ) )
        plan->anim.push( anim );

    return parser.isOk();
}

bool load( const lg_cFile &file )
{
    lg_assert( file.isOpen( ) );

    lg_cParser parser( file.getData() );

    sPlan * plan = lg_memory_new<sPlan>( true );

	// expect the header
	parser.expect( "<sprite>" );

    while ( !parser.atEOF( ) && parser.isOk() )
    {
        if ( parser.found( "<info>" ) )
			if (! parse_info( parser, plan ) )
				parser.markFail( );

		if ( parser.found( "<anim" ) )
			if (! parse_anim( parser, plan ) )
				parser.markFail( );
    }

    // check if the plan is valid
    if ( !isValid( plan ) )
        parser.markFail();

    // 
    if ( parser.isOk() )
    {
        //
        for ( int i=0; i<plan->sheet.size(); i++ )
            plan->sheet[i]->setMask( plan->mask );

        // add to sprite template list
        add( plan );
		return true;
    }
    else
	{
        lg_log::print( "   error @ line %d", parser.getLineNumber( ) );
		return false;
	}
}

// add a sprite template to the template list
bool add( sPlan *t )
{
    lg_assert( t != nullptr );
    plans.push( t );
    return true;
}

// locate a sprite in the template list
sPlan * find( const lg_cStringRef & ref )
{
    for ( int i=0; i<plans.size( ); i++ )
    {
        sPlan *t = plans[i];
        lg_assert( t != nullptr );

        if ( t->name == ref )
            return t;
    }
    return nullptr;
}

};