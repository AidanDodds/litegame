#include "lg_basicCollector.h"

bool lg_cBasicCollector::add
(
    lg_cEntity *entity 
)
{
    if ( items >= cBasicCollector::nMaxItems )
        return false;

    list[ items++ ] = entity;

    return true;
}

void lg_cBasicCollector::traverse
(
    lg_cbTraverse callback,
    const lg_sTraverseParams &param
)
{
    for ( int i=0; i<items; i++ )
    {
        lg_cEntity *entity = list[ i ];
        lg_assert( entity != nullptr );

        // is this entity alive
        if ( entity->references > 0 )
            // pass this entity to the callback
            callback( entity, param );
    }
}

void lg_cBasicCollector::purge
(
    void
)
{
    for ( int i=0; i<items; i++ )
    {
        lg_cEntity *entity = list[i];
        lg_assert( entity != nullptr );

        // delete this entity
        lg_factory::destroy( entity );
        list[i] = nullptr;
    }

    items = 0;
}

void lg_cBasicCollector::clean
(
    void
)
{
    int j=0; // insertion point

    for ( int i=0; i<items; i++ )
    {
        lg_cEntity *entity = list[i];
        lg_assert( entity != nullptr );

        // can this entity be removed
        if ( entity->references == 0 )
        {
            // delete this entity
            lg_factory::destroy( entity );
            list[i] = nullptr;
        }
        else
        {
            if ( j < i )
                // move this item in the list
                list[j] = list[i];
            j++;
        }
    }

    // save new item count
    items = j;
}

void lg_cBasicCollector::tickAll( void )
{
    for ( int i=0; i<items; i++ )
    {
        lg_cEntity *entity = list[i];
        lg_assert( entity != nullptr );
        
        // is this entity alive
        if ( entity->references > 0 )
            // pass this entity to the callback
            entity->onTick( );
    }
}

// run a search for a entity type
lg_cEntity * lg_cBasicCollector::find
(
    lg_sBasicCollectorSearch &search,
    bool                      restart
)
{
    int i=0;

    if (! restart )
    {
        i =  search.head;
    }

    for ( ; i<items; i++ )
    {
        lg_cEntity *entity = list[ i ];
        lg_assert( entity != nullptr );
        
        // is this entity alive
        if ( entity->references > 0 )
        {
            // find an entity that matches
            if ( entity->type() == search.type )
            {
                // save the next pointer
                search.head = i+1;

                // return the found entity
                return entity;
            }
        }
    }

    search.head = 0;
    return nullptr;
}

void lg_cBasicCollector::zSort( void )
{
    for ( int i=0; i<items-1; )
    {
		// things we expect
		lg_assert( list[i  ] != nullptr );
		lg_assert( list[i+1] != nullptr );

        // find depth values
        float za = list[i  ]->pos.y;
        float zb = list[i+1]->pos.y;
        
        if ( za > zb )
        {
            // swap the two elements
			lg_cEntity *tmp   = list[i  ];
			        list[i  ] = list[i+1];
			        list[i+1] = tmp;
            
            // bubble up to the top
            if ( i > 0 )
                i--;
        }
        else
            // move forward in list
            i++;
    }    
}