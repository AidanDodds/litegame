#pragma once
#include "../../libUtility/source/lg_types.h"

#include "../../libMut/mut.h"

namespace lg_map2d
{
	// generic map info
	struct info_t
	{
		u32   w, h;		// size of the map (in tiles)
		byte *mask;		// byte per tile
		byte  tileSize;	// size of a tile in pixels
	};

	// XXX: the map aabb should be in map space rather then
	//		in pixel space?

	// axis alligned bounding box
	// all values in pixel space not tile space
	struct aabb_t
	{
		float x1, y1;		// min
		float x2, y2;		// max
		float rx, ry;		// resolution vector

		// apply any resolution
		void doResolve( void )
		{
			x1 += rx; x2 += rx;
			y1 += ry; y2 += ry;
		}
	};

	//
	struct rayHit_t
	{
		int   tx, ty;
		float hx, hy;
		int   face;
	};

	// toggle a wall on or off
	// x,y in tile space
	void setWall
        ( lg_map2d::info_t &map, const mut::vec2i &p, bool add );

	// query if a tile is a wall or not
	// x,y in tile space
	bool isWall
        ( lg_map2d::info_t &map, const mut::vec2i &p );

	// create a map
	void create
        ( lg_map2d::info_t &map, const mut::vec2i &p, u32 tileSize );

	// create collision information for this map
	void preprocess
        ( lg_map2d::info_t &map );

	// get collision state of an AABB in the map
	bool collide
        ( const lg_map2d::info_t &map, lg_map2d::aabb_t &info );

	// release memory taken up by the map
	void release
        ( lg_map2d::info_t &map );

	// cast a ray in map space
	bool raycast
        ( lg_map2d::info_t &map, const mut::vec2 &a, const mut::vec2 &b, rayHit_t &hit );

	// do we have line of sight
	bool lineOfSight
        ( lg_map2d::info_t &map, const mut::vec2 &a, const mut::vec2 &b );
};

typedef lg_map2d::info_t lg_sMap2d;
typedef lg_map2d::aabb_t lg_sAABB;
typedef lg_map2d::aabb_t lg_srayHit;