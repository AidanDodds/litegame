#pragma once

#include "../../libMut/mut.h"

#include "../../libUtility/source/lg_file.h"
#include "../../libUtility/source/lg_stringRef.h"
#include "../../libUtility/source/lg_stack.h"
#include "../../libUtility/source/lg_image.h"

namespace lg_spritePlan
{
    enum opcode
    {
        OP_END      = 0,    // end
        OP_LOOP     ,       // loop
        OP_OFFSET   ,       // offset x, y
        OP_SPEED    ,       // speed  d
        OP_SIGNAL   ,       // signal s
        OP_FRAME    ,       // frame  frame
        OP_NEXT     ,       // next
        OP_DELAY    ,
        OP_RAND     ,
        OP_SHEET    ,       // sheet  i
    };

    // blueprint for an animation
	//
	struct sAnim
	{
		lg_cStringRef name;
        int           angle;
		byte          code[ 48 ];
	};

	// a blueprint for a sprite
	//
	struct sPlan
	{
		lg_cStringRef        name;
		mut::vec2i           frame;
		lg_cStack<sAnim>     anim;
        int                  nAngles;
		lg_cStack<lg_cImage> sheet;
        mut::vec2i           offset;
        int                  mask;
	};

    // load a plan from a file
    extern bool load( const lg_cFile &file );
    
    // add a sprite template to the template list
    extern bool add( sPlan *t );

    // search for a loaded plan
    extern sPlan * find( const lg_cStringRef & ref );

};

typedef lg_spritePlan::sPlan lg_sSpritePlan;
typedef lg_spritePlan::sAnim lg_sSpriteAnim;