#pragma once

#include "Source/lg_map2d.h"
#include "Source/lg_entity.h"
#include "Source/lg_factory.h"
#include "Source/lg_sprite.h"
#include "Source/lg_basicCollector.h"

namespace lg_liteGame
{

	extern bool start( void );
    extern bool tick ( void );
	extern void stop ( void );

}; // namespace liteGame