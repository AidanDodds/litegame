#pragma once

#include "../../libUtility/source/lg_memory.h"
#include "../../libUtility/source/lg_types.h"
#include "../../libUtility/source/lg_image.h"
#include "../../libMut/mut.h"

namespace lg_canvas
{

	class cCanvas
	{
	public:

		cCanvas( void )
			: pixels( )
			, width ( 0 )
			, height( 0 )
		{ }

		void clear
			( rgb_t colour );

		void plot
			( s32 x, s32 y, rgb_t c )
		{
			if ( x < 0 || x >= (s32)width || y < 0 || y >= (s32)height )
				return;
			pixels[ x + y * width ] = c;
		}

		void circle
			( s32 x, s32 y, s32 r, rgb_t c );

		void line
			( const mut::vec2i &a, const mut::vec2i &b, rgb_t c );
		
		void rect
			( s32 x, s32 y, s32 w, s32 h, rgb_t c );

		void printf
			( const mut::vec2i &pos, char *text, ... );

        //
        // todo:
        //      validate the frame
        //      also make frame optional?
        //      add frame to img for fast switching
        //  

        // draw a masked region of an image
		void maskBlit
			( const lg_cImage & img, const mut::vec2i &dst, const recti &frame );

        // draw an unmasked region of an image
		void fastBlit
			( const lg_cImage & img, const mut::vec2i &dst, const recti &frame );

        // draw a masked image region with a fill colour
		void shadeBlit
			( const lg_cImage & img, const mut::vec2i &dst, const recti &frame, u32 colour );

		lg_memAlloc<u32> pixels;
		u32  width, height;
	};
	
	// canvas lifetime
	extern
	bool create
		( cCanvas &info, int w, int h );

	extern
	void release
		( cCanvas &info );
	
	// generic blitting routines
	extern
	void blitx1
		( cCanvas &info, u32 *dst, u32 dx, u32 dy );
	
	extern
	void blitx2
		( cCanvas &info, u32 *dst, u32 dx, u32 dy );

};

typedef lg_canvas::cCanvas lg_cCanvas;