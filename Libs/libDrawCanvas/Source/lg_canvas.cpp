#include "lg_canvas.h"

#include "../../libUtility/source/lg_memory.h"

namespace lg_canvas
{

bool create
	( cCanvas &info, int w, int h )
{
	info.width  = w;
	info.height = h;

	release( info );

    // allocate space for the canvas
    info.pixels.alloc( w * h );

	return info.pixels.get() != nullptr;
}

void release
	( cCanvas &info )
{
    info.pixels.release( );
}

void cCanvas::clear
	( rgb_t c )
{
	for ( u32 i=0; i < width*height; i++ )
		pixels[i] = c;
}

void cCanvas::circle
	( int x0, int y0, int radius, rgb_t c )
{
	int error  =  1 - radius;
	int errorY =  1;
	int errorX = -2 * radius;

	int x = radius, y = 0;
 
	plot(x0, y0 + radius, c);
	plot(x0, y0 - radius, c);
	plot(x0 + radius, y0, c);
	plot(x0 - radius, y0, c);
 
	while(y < x)
	{
		if(error > 0) { x--; errorX += 2; error += errorX; }
		                y++; errorY += 2; error += errorY;    
		plot(x0 + x, y0 + y, c);
		plot(x0 - x, y0 + y, c);
		plot(x0 + x, y0 - y, c);
		plot(x0 - x, y0 - y, c);
		plot(x0 + y, y0 + x, c);
		plot(x0 - y, y0 + x, c);
		plot(x0 + y, y0 - x, c);
		plot(x0 - y, y0 - x, c);
	}
}

void cCanvas::line
	( const mut::vec2i &a, const mut::vec2i &b, rgb_t c )
{
	s32 decInc, j=0, incrementVal, endVal;
	s32 shortLen = b.y-a.y, longLen = b.x-a.x;
	bool yLonger = false;
	if ( mut::absv<int>( shortLen ) > mut::absv<int>( longLen ) )
	{
		int swap = shortLen;
		shortLen = longLen;
		longLen  = swap;
		yLonger  = true;
	}
	endVal=longLen;
	if ( longLen < 0 )
	{
		incrementVal = -1;
		longLen      = -longLen;
	}
	else
		incrementVal = 1;
	if ( longLen == 0 )
		decInc = 0;
	else
		decInc = (shortLen << 16) / longLen;
	if ( yLonger )
		for ( s32 i=0; i!=endVal; i+=incrementVal )
		{
			plot( a.x + (j >> 16), a.y + i, c );
 			j += decInc;
		}
	else
		for ( s32 i=0; i!=endVal; i+=incrementVal )
		{
			plot( a.x + i, a.y + (j >> 16), c );
			j += decInc;
		}
}

void cCanvas::rect( int x, int y, int w, int h, rgb_t c )
{
	// clamp to screen space
	int x1 = mut::clampv<int>( 0, x  , width  );
	int y1 = mut::clampv<int>( 0, y  , height );
	int x2 = mut::clampv<int>( 0, x+w, width  );
	int y2 = mut::clampv<int>( 0, y+h, height );

	// loop over all pixels
	for ( int y=y1; y<y2; y++ )
	for ( int x=x1; x<x2; x++ )
	{
		plot( x, y, c );
	}
}

#if 0
//TODO: super optimize!!
void cCanvas::maskBlit( const lg_cImage * img, const vec2i &dst, const recti &frame )
{
    // use fast plot when entirely in screen
    // cull when entirely offscreen

	assert( img != nullptr );

	u32 _m = img->getMask( );
    u32 _y = 0;
    u32 _x = 0;
	for ( int y=frame.y1; y<frame.y2; y++ )
	{
        _x = 0;
		for ( int x=frame.x1; x<frame.x2; x++ )
		{
			u32 c = img->getPixel( x, y );

			if ( c != _m )
				plot( _x + dst.x, _y + dst.y, c );

			_x++;
		}
        _y++;
	}
}
#else
void cCanvas::maskBlit
    ( const lg_cImage & img, const mut::vec2i &dst, const recti &frame )
{
#if 1
    // cull when entirely offscreen
    bool cull  = frame.x2 <  0;
         cull |= frame.x1 >= width;
         cull |= frame.y2 <  0;
         cull |= frame.y1 >= height;
    if ( cull )
        return;
#endif

    // check if it is entirely onscreen
    bool fast  = (dst.x + frame.x1) >= 0;
         fast &= (dst.x + frame.x2) <  width;
         fast &= (dst.y + frame.y1) >= 0;
         fast &= (dst.y + frame.y2) <  height;
    
#if 0
         fast &= frame.x1 >= 0;
         fast &= frame.y1 >= 0;
         fast &= frame.x2 < imgSize.x;
         fast &= frame.y2 < imgSize.y;
#endif

    // image mask colour
	u32 _m = img.getMask( );

    // screen space pixel drawing positions
    s32 _x = dst.x;
    s32 _y = dst.y;

    // take the fast drawing path when not clipping
    if ( fast )
    {
        u32 *ipx = img.getPixels();
        u32  wid = img.getSize().x;

	    for ( u32 y=frame.y1; y<frame.y2; y++ )
	    {
		    for ( u32 x=frame.x1; x<frame.x2; x++ )
		    {
                u32 _c = ipx[ x + y * wid ];
			    if ( _c != _m )
    		        pixels[ _x + _y * width ] = _c;
			    _x++;
		    }
            _x = dst.x;
            _y++;
	    }
    }
    // take the slow path when clipping
    else
    {
	    for ( u32 y=frame.y1; y<frame.y2; y++ )
	    {
		    for ( u32 x=frame.x1; x<frame.x2; x++ )
		    {
                u32 _c = img.getPixel( x, y );
			    if ( _c != _m )
    		        plot( _x, _y, _c );
			    _x++;
		    }
            _x = dst.x;
            _y++;
	    }
    }
}
#endif

void cCanvas::fastBlit
    ( const lg_cImage & img, const mut::vec2i &dst, const recti &frame )
{
    u32 _y = 0;
    u32 _x = 0;
	for ( u32 y=frame.y1; y<frame.y2; y++ )
	{
        _x = 0;
		for ( u32 x=frame.x1; x<frame.x2; x++ )
		{
			u32 c = img.getPixel( x, y );
		    plot( _x + dst.x, _y + dst.y, c );
			_x++;
		}
        _y++;
	}
}

// draw this sprite masking all pixels with a colour
void cCanvas::shadeBlit
    ( const lg_cImage & img, const mut::vec2i &dst, const recti &frame, rgb_t colour )
{
    // cull when entirely offscreen
    bool cull  = frame.x2 <  0;
         cull |= frame.x1 >= width;
         cull |= frame.y2 <  0;
         cull |= frame.y1 >= height;
    if ( cull )
        return;
        
    // image mask colour
	u32 _m = img.getMask( );
    u32 _y = 0;
    u32 _x = 0;
	for ( u32 y=frame.y1; y<frame.y2; y++ )
	{
        _x = 0;
		for ( u32 x=frame.x1; x<frame.x2; x++ )
		{
			if ( img.getPixel( x, y ) == _m )
    		    plot( _x + dst.x, _y + dst.y, colour );
			_x++;
		}
        _y++;
	}
}

void cCanvas::printf
    ( const mut::vec2i &pos, char *text, ... )
{

}

static inline
void duffs_pixCopy_2x
    ( u32 *s, u32 *d, int w, int l )
{
	u32 c = 0;

	#define PIXCPY2 { c=*s; *(d)=c; *(d+1)=c; *(d+l)=c; *(d+l+1)=c; d+=2; s+=1; }

    int n = (w+7) / 8;
    switch( w % 8 )
    {
       case 0: do { PIXCPY2;
       case 7:      PIXCPY2;
       case 6:      PIXCPY2;
       case 5:      PIXCPY2;
       case 4:      PIXCPY2;
       case 3:      PIXCPY2;
       case 2:      PIXCPY2;
       case 1:      PIXCPY2;
        } while ( --n > 0 );
    }
}

extern
void blitx1
	( cCanvas &info, u32 *dst, u32 w, u32 h )
{
    lg_assert(! "not impemented" );
}

extern
void blitx2
	( cCanvas &info, u32 *dst, u32 w, u32 h )
{
	u32 xmin = mut::minv<u32>( w / 2, info.width  );
	u32 ymin = mut::minv<u32>( h / 2, info.height );

	u32* src = info.pixels.get();

	for ( u32 y=0; y < ymin; y++ )
	{
		// copy one scanline with duffs device
		duffs_pixCopy_2x( src, dst, xmin, w );
		// move on to next row
		src += info.width;
		dst += w * 2;
	}
}

}; // namespace lg_canvas