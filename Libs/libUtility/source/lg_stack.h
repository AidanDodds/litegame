#pragma once
#include "lg_types.h"
#include "lg_assert.h"
#include "lg_memory.h"

//
//
//
template <typename T>
class lg_cStack
{
private:

    lg_memAlloc<T*> list;
//	T ** list;
	int  head;
	int  capacity;

public:
	
	lg_cStack( void )
        : head    ( 0 )
        , capacity( 0 )
        , list()
    { }
	
	~lg_cStack( )
	{
        list.release();
	}
	
	//
	// get the number of items on the stack
	//
	int size( void ) const
	{
		return head;
	}

	//
	// push an item onto the stack
	//
	void push( T * item )
	{
		if ( head >= capacity )
			resize( );
		lg_assert( capacity > head );
		list[ head++ ] = item;
	}
	
	//
	// pop the top item off the stack
	//
	T * pop( void )
	{
		lg_assert( head > 0 );
		// grab this item off the stack
		T *out = list[ --head ];
		// scrub it
		list[ head ] = nullptr;
		return out;;
	}
	
	// 
	// discard n items from the top of the stack
	//
	void discard( const int i )
	{
		lg_assert( head >= i );
		for ( int j=1; j<=i; j++ )
			list[ head-j ] = nullptr;
		head -= i;
	}

	//
	// return the topmost item
	//
	T * peek( const int lb = 0 )
	{
		int ix = (head - 1) - lb;
		// check index is in range
		lg_assert( ix >= 0 && ix < head );
		// grab this item off the stack
		return list[ ix ];
	}

	//
	// peek or index any item on the stack
	//
	T * operator [] ( const int index ) const
	{
		if ( index < 0 || index >= head )
			return nullptr;
		return list[ index ];
	}

	//
	// clear the entire list
	//
	void clear( void )
	{
		head = 0;
	}

private:

	//
	// double the size of the current stack
	//
	void resize( void )
	{
#if 0
		lg_assert( capacity >= 0 );
		if ( capacity == 0 )
		{
			// we may need to free
			if ( list != nullptr )
				delete [] list;
			// starting capacity
			capacity = 8;
			// create new list
			T ** newList = new T* [ capacity ];
			lg_assert( newList != nullptr );
			// swap to new list
			list = newList;
		}
		else
		{
			capacity = capacity * 2;
			// create new list
			T ** newList = new T* [ capacity ];
			lg_assert( list != nullptr );
			// copy over to new list
			for ( int i=0; i<capacity; i++ )
				if ( i < head )
					newList[i] = list[i];
				else
					newList[i] = nullptr;
			// release the old list
			lg_assert( list != nullptr );
			delete [] list;
			// swap to new list
			list = newList;
		}
#else
        if ( capacity == 0 )
        {
            capacity = 8;
            list.alloc( capacity );
        }
        else
        {
            capacity *= 2;
            list.resize( capacity );
        }
#endif
	}
	
};
