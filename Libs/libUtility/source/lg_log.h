#pragma once

namespace lg_log
{

	// write text to a log file in the same way
	// that printf would
	extern void print( char *text, ... );

}; // namespace log