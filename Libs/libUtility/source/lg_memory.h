#pragma once

#include <new.h>

#include "./../../libUtility/source/lg_assert.h"

class lg_memory
{
public:
    
    struct sStats
    {
        sStats( void )
            : outgoing( 0 )
        {}

        int outgoing;
    };

private:

    lg_memory( void )
        : stats( )
    {}

    lg_memory( const lg_memory &mem );

    // singleton instance
    static lg_memory * _inst;

    // allocation statistics
    struct sStats stats;

public:

    //
    // memory module singleton
    //

    static lg_memory & inst( void )
    {
        lg_assert( inst != nullptr );
        return *_inst;
    }

    static bool start( void );
    static bool stop ( void );

    //
    // instance methods
    //
    
    // generic memory for long term allocations
    void *memAlloc  ( int size, bool zero );
    void  memRelease( void * );

    // scratch memory for short term allocations
    void *scratchAlloc  ( int size, bool zero );
    void  scratchRelease( void * );

    // get memory usage
    const sStats & getStats( void );

};

// scratch memory
class lg_scratchMem
{
    void *data;

    // disable void constructor
    lg_scratchMem( void )
    {}
    // disable copy
    lg_scratchMem( const lg_scratchMem & );

public:
    // create scratch memory
    lg_scratchMem( const int aSize, bool zero )
        : data( nullptr )
    {
        data = lg_memory::inst().scratchAlloc( aSize, zero );
    }

    // free when goes out of scope
    ~lg_scratchMem( )
    {
        lg_assert( data != nullptr );
        lg_memory::inst().scratchRelease( data );
    }

    // access the scratch data
    void *get( void ) const
    {
        return data;
    }
};

// generic memory allocator
template <typename T>
class lg_memAlloc
{
    T   *data;
    int  nItems;
    int  nBytes;

    lg_memAlloc( const lg_memAlloc & );

public:

    lg_memAlloc( void )
        : data( nullptr )
        , nItems( 0 )
        , nBytes( 0 )
    {}

    ~lg_memAlloc( )
    {
        release( );
    }

    // allocate a generic memory region
    void alloc( const int anItems )
    {
        // check size is valid
        lg_assert( anItems > 0 );
        // free any old memory we have alloced
        release();
        // allocate new memory
        nItems = anItems;
        nBytes = nItems * sizeof( T );
        data = (T*) lg_memory::inst().memAlloc( nBytes, false );
        // check allocation waas ok
        lg_assert( data != nullptr );
    }

    // release this allocation
    void release( void )
    {
        // bomb out when no data exits to free
        if ( data == nullptr )
            return;
        lg_memory::inst().memRelease( (void*) data );
        data   = nullptr;
        nItems = 0;
        nBytes = 0;
    }
    
    T *get( void ) const
    {
        return data;
    }

    T & operator [] ( const int index ) const
    {
        lg_assert( data != nullptr );
        return data[ index ];
    }

    // resize this allocation
    void resize( const int newnItems )
    {
        lg_assert( data != nullptr );
        int newnBytes = newnItems * sizeof( T );
        int minSize   = newnItems < nItems ? newnItems : nItems;
        // create new memory region
        T* newData    = (T*) lg_memory::inst().memAlloc( newnBytes, false );
        // copy over old data
        for ( int i=0; i<minSize; i++ )
            newData[i] = data[i];
        // release old memory
        lg_memory::inst().memRelease( data );
        // copy new state
        data   = newData;
        nItems = newnItems;
        nBytes = newnBytes;
    }

};

// allocated placement new
template <class T>
extern T * lg_memory_new( bool zero )
{
    // get size of type
    int size = sizeof( T );
    // allocate space for new type
    void *data = lg_memory::inst().memAlloc( size, zero );
    lg_assert( data != nullptr );
    
    // placement new on type
    T *item = new (data) T;
    
    return item;
}