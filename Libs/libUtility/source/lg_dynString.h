#pragma once
#include "lg_types.h"
#include "lg_assert.h"

namespace lg_string
{

	class cDynString
	{
	public:

		/* ctor */  cDynString( void );
		/* ctor */  cDynString( const char *ref );
		/* dtor */ ~cDynString( );

		// 
		void append( const char   );
		void append( const char * );
		void append( const cDynString & );

		// printf style
	//	void print( const char *str, ... );

		int findRight( char ch );
		int findLeft ( char ch );

		// pre-allocate space in the string
		void resize( int minSize );
	
		// reduce the length of this string
		void crop( int length );

		// make string zero length
		void clear( void )
		{
			if ( _data == 0 || _space == 0 )
				return;
			_length  = 0;
			_data[0] = '\0';
		}

		// access random (always valid) element
		char& operator [] ( int index )
		{
			// assure index is always positive
			lg_assert( index >= 0 );
			// if out of scope we must allocate space
			if ( index+2 > _space )
			{
				resize( index+2 );
				// check we have data etc
				lg_assert( _data   != 0 );
				lg_assert( _space  != 0 );
			}
			// make sure we have enough space		
			lg_assert( _space >= index+2 );
			// if length has increased
			if ( index >= _length )
			{
				// update length
				_length = index+1;
				// insert new terminal char
				_data[ index+1 ] = '\0'; 
			}
			// return this location
			return _data[ index ];
		}

		// cast to char
		operator char*     ( void ) { return _data; }
				 char* cstr( void ) { return _data; }
			 
		// if we have written to _data externaly, then
		// length will not track an potential changes.
		// we can force _length to be valid with this
		// call.
		void validate( void );

		// return the string length
		int getLength( void )
		{
			return _length;
		}


	private:

		// start of data
		char *_data;

		// allocated space
		int   _space;

		// string length
		int   _length;

	};

}; // namespace lg_string

typedef lg_string::cDynString lg_cDynString;