
#include <assert.h>
#include <stdio.h>

#include "lg_assert.h"

#ifndef NO_GUARD
    #define WIN32_LEAN_AND_MEAN
    #define VC_EXTRALEAN
	// remove FOURCC conflict
    #define MMNOMMIO
    #include <Windows.h>
    #undef assert
#endif

#include "lg_stack.h"
#include "lg_coroutine.h"

// compile time feature selection
//	#define VIRTUAL_THREAD_ALLOC
//	#define MESSAGE_ON_OVERFLOW
	#define HANDLER_USE_RECOVER
	#define CLEAR_ON_ALLOC

namespace lg_coroutine
{
    // memory debugging info
    static int _allocs = 0;

    // number of full integers that can fit on the stack
    const int c_shared_stack_size = 1024 * 128;	// 128k
    const int c_thread_stack_size = 1024;		// 1k
    
    // a large stack that is shared amongst the running threads.  
    static void*       _sharedStack = nullptr;
    // stores the return context used in yield
    static int         _esp_save    = 0;
    // signals abborant thread behaviour
    static bool        _overflow    = false;
    static bool        _terminate   = false;
    // pointer to the top of the stack
    static byte*       _stackTop    = nullptr;
    // stores the last thread to be executed to reduce memory copies
    static sCoroutine* _lastThread  = nullptr;
    // stores a handle to the vectored exception handler
    static void*       _handler     = nullptr;

    static sLibInfo    _libInfo     = { 0, 0 };

    // offset X by Y bytes
    #define offset( X, Y ) ((char*)(X) + (Y))
    #define setstatus( X ) { if ( status != nullptr ) *status = (X); }

// the thread yield function, used to swap contexts
//
// to make things more thread safe, we could give each coroutine a batton that
// it must use to yield.  it would be that batton that stores the old stack info.
extern
void __declspec( naked ) yield( void )
{
	__asm
	{
		// save important registers from current context
		push ebx;
		push ebp;
		push esi;
		push edi;
		// swap over stack pointers
		mov eax, esp;
		mov esp, _esp_save;
		mov _esp_save, eax;
		// load important registers from new context
		pop edi;
		pop esi;
		pop ebp;
		pop ebx;
		// return to caller address on stack
		ret;
	};
}

// the thread recover function
// this simply loads the context once more, when ESP is already
// set for the new context.  this is where execution resumes after
// the exception handler has handled a rogue thread.
static
void __declspec( naked ) recover( void )
{
	__asm
	{
		// load important registers that were saved
		pop edi;
		pop esi;
		pop ebp;
		pop ebx;
		// jump back to caller address
		ret;
	};
}

// memory clear routine
static inline
void _clearMem( void *dst, int size )
{
	const int c_value = 0;
#ifdef WIN32
	memset( dst, c_value, size );
#else
	// naive reference version
	for ( int i=0; i<size; i++ )
		((char*)dst)[i] = c_value;
#endif
}

// memory copy
static inline
void _copy( void *src, void *dst, int size )
{
#ifdef WIN32
	memcpy( dst, src, size );
#else
	// naive reference version
	for ( int i=0; i<size; i++ )
		((char*)dst)[i] = ((char*)src)[i];
#endif
}

// called when the guard page on our shared stack is written to or read from
//
// it should be noted that this exception handler is still called from the microthread
// stack so we have to be carefull not to overflow with the 4k we know we have.
static
LONG WINAPI guardHandler
(
    _EXCEPTION_POINTERS *info
)
{
	// check that it was a guard page that triggered this exception
	if ( info->ExceptionRecord->ExceptionCode != 0x80000001 )
	    return EXCEPTION_CONTINUE_SEARCH;

	// get the offending address
	int oaddr = info->ExceptionRecord->ExceptionInformation[1];

	// if it was this guard page that triggered the exeption
	if ( oaddr < ((int)_sharedStack + 4096) && (oaddr >= (int)_sharedStack) )
	{
		// signal that we have overflown
		_overflow = true;

		// switch out of the micro thread context
		//
		// here is a strange condition.  since we are still in the context of the microthread
		// we need to exit from it.  however, if we exit using yield(), then we just
		// dissapear from the normal vectored exception handler process.  That may cause problems
		// for the OS, I dont rightly know.
		// an alternative is to set the context using _EXCEPTION_POINTERS and then say we have
		// handled the problem and return.  this way we can regain the normal context as well as
		// exit the way we want.
		info->ContextRecord->Esp = _esp_save;

		// jump to the recovery routine (what about 64bit targets? here is 32bit DWORD??)
		info->ContextRecord->Eip = (DWORD)recover;
		
#ifndef HANDLER_USE_RECOVER
//		yield();
#else
		// continue execution at recover
		return EXCEPTION_CONTINUE_EXECUTION;
#endif
	}
	//
    return EXCEPTION_CONTINUE_SEARCH;
}

// this function will be called if we return from the micro thread function
// from within that thread.  here we can either setup the thread for another
// execution of the microthread function, or kill the thread.
static
void cliff( void )
{
	// here we need to do something to address the issue that the stack pointer
	// lies outside the region saved for personal thread space.  We can kill
	// such a thread, or expand its personal space.
	_terminate = true;
	//
	yield( );
}

//
static
void guardStack( void )
{
	// use VirtualProtectEx(  ) to create a guard region at the bottom of the stack to
	// inform us of stack overflows.  This is because stacks grow downwards.
//	DWORD newProtect = PAGE_NOACCESS;
	DWORD newProtect = PAGE_READWRITE | PAGE_GUARD;
	DWORD oldProtect = 0;
	BOOL res = VirtualProtect( _sharedStack, 1, newProtect, &oldProtect );
}

//
static
void createSharedStack( void )
{
	// allocate a large shared stack space using virtual alloc
	// we also get nice page alignment charicteristics
	_sharedStack = VirtualAlloc( nullptr, c_shared_stack_size, MEM_COMMIT, PAGE_READWRITE );
    _libInfo.allocs++;
	_clearMem( _sharedStack, c_shared_stack_size );

	// set the guard for our stack
	guardStack( );

	if ( _handler == nullptr )
		// add a vectored exeption handler to deal with the situation that the
		// stack is overflowed and a write is issued to the guard page

		// 1 = make this handler the first one called
		// 0 = make this the last handler called
		_handler = AddVectoredExceptionHandler( 1, guardHandler );

	// find the starting copy point for the thread stack
	_stackTop = (byte*)_sharedStack + c_shared_stack_size;
}

//
static
void createThreadStack( sCoroutine &thread )
{
	// create the private thread stack
    thread.stack.size = c_thread_stack_size;
	thread.stack.data = new char[ thread.stack.size ];
    _libInfo.allocs++;

#ifdef CLEAR_ON_ALLOC
	// clear the memory when we are done, however this in not strictly neccesary
	_clearMem( thread.stack.data, thread.stack.size );
#endif
}

//
extern
bool create( sCoroutine &thread, void (*func)(void*), void *param )
{
	// create the shared stack if it doesnt exist
	if ( _sharedStack == nullptr )
		createSharedStack( );

    // assumptions
    lg_assert( thread.stack.data == nullptr );
    lg_assert( thread.stack.size == 0 );
    lg_assert( thread.esp == 0 );

	// 
	createThreadStack( thread );

	// 
	int *a = (int*) offset( thread.stack.data, thread.stack.size );
	// make room for the default stack
	a -= 7;
	a[0] = (int ) 0;
	a[1] = (int ) 0;
	a[2] = (int ) 0;
	a[3] = (int ) 0;
	// the return address is our microthread function
	a[4] = (int ) func;
	// the micro threat return address
	a[5] = (int ) cliff;
	// pass our initial parameter to the function
	a[6] = (int ) param;
	// prepare our default stack pointer location
	thread.esp = (int)_sharedStack + c_shared_stack_size - (7 * 4);

    _libInfo.threads++;
	return true;
}

// 
static
bool espIsValid( sCoroutine &thread )
{
	int guard_high = (int)_sharedStack + (c_shared_stack_size);
	int guard_low  = guard_high - thread.stack.size;
	// check that stack pointer is in personal thread stack space
	return (_esp_save < guard_high) && (_esp_save > guard_low);
}

// if a stack is too small to be saved by the time that a new thread will be
// swapped in we must enlarge it so that it can contain the full stack
// up to the current esp
static
bool enlarge( sCoroutine &thread )
{
    int stackTop  = (int)_sharedStack + (c_shared_stack_size);
    int spillSize = stackTop - _esp_save;

    // round up to a multiple of 1024
    thread.stack.size = (spillSize | 0x3ff) + 1;
    lg_assert( thread.stack.size < c_shared_stack_size );
    delete [] thread.stack.data;
    thread.stack.data = new byte[ thread.stack.size ];
    lg_assert( thread.stack.data != nullptr );

    return true;
}

//
extern
void run( sCoroutine &thread, int &status )
{
	// check that the thread we have is valid
	if ( thread.stack.data == nullptr )
	{
		status = eState::invalid;
		return;
	}
	
	// if we are switching micro threads
	if ( _lastThread != &thread )
	{
		// copy this threads state into its thread object
		if ( _lastThread != nullptr )
		{
			// save the stack pointer into the owning thread object
			_lastThread->esp = _esp_save;

			// copy this stack back to personal stack space before we make
			// a switch to another thread
            void *rp = _stackTop - _lastThread->stack.size;
			_copy( rp, _lastThread->stack.data, _lastThread->stack.size );
		}

		// copy over the new threads state
        void *wp = _stackTop - thread.stack.size;
		_copy( thread.stack.data, wp, thread.stack.size );

		// prep the stack pointer for the switch
		_esp_save = thread.esp;

		// update the last processed record to this thread
		_lastThread = &thread;
	}

	// check if the micro thread has a valid state
	if (! espIsValid( thread ) )
	{
		status = eState::invalid;
		return;
	}

	// this is the magic of this technique. here we
	// switch over our stacks and allow the new thread a
	// chance to run until it yields
	yield( );

    // has this thread overflowed the stack
	if ( _overflow )
	{
		// we need to guard the stack once more since it is removed by each
		// exception that we receive
		guardStack( );
		// mark this thread as rogue and free it
		free( thread );
		//
#ifdef MESSAGE_ON_OVERFLOW
		MessageBoxA( nullptr, "Overflow in microThread!\n", "Warning!", MB_OK ); 
#endif
		//
		status = eState::overflowed;
		// we have handled the overflow so lets just continue
		_overflow = false;
		return;
	}

	// has this thread completed
	if ( _terminate )
	{
		// mark the thread status
		status = eState::terminated;
		// mark this thread as rogue and free it
		free( thread );
		//
		_terminate = false;
		return;
	}

	// has the context been swapped but the stack has grown past containment per thread
	// then we have two options.  terminate the thread, or enlarge its stack.
	if (! espIsValid( thread ) )
	{
		// enlarge the stack to make room for its new size
		if (! enlarge( thread ) )
        {
#ifdef MESSAGE_ON_OVERFLOW
		    MessageBoxA( nullptr, "Overflow in microThread!\n", "Warning!", MB_OK ); 
#endif
            free( thread );
            status = eState::overflowed;
            return;
        }
	}

	// everything went fine and we returned here after the micro thread yielded
	status = eState::yielding;
}

// 
extern
void free( sCoroutine &thread )
{
	// clear our record of the old thread
	if ( _lastThread == &thread )
		_lastThread = nullptr;

//	lg_assert( thread.stack.data != nullptr )
    if ( thread.stack.data == nullptr )
        return;

    delete [] thread.stack.data;
    thread.stack.data = nullptr;
    _libInfo.allocs--;
    thread.stack.size = 0;
	thread.esp        = 0;

    _libInfo.threads--;
}

// 
extern
void dumpstack( sCoroutine &thread )
{
	//
	printf( "stack dump\n{\n" );
	//
	unsigned char *data = (unsigned char*)thread.stack.data;
	//
	for ( int i=0; i<c_thread_stack_size; i++ )
	{
		// 
		if ( (i%16 == 0) && (i>0) )
			putchar( '\n' );
		// 
		int a = (data[i]) &  0xF;
		int b = (data[i]) >> 4;
		//
		if ( b <  10 ) putchar( b + '0' );
		if ( b >= 10 ) putchar( b + 'a' - 10 );
		if ( a <  10 ) putchar( a + '0' );
		if ( a >= 10 ) putchar( a + 'a' - 10 );
		putchar( ' ' );
	}
	putchar( '\n' );
	//
	printf( "};\n" );
}

// 
extern
void cleanup( void )
{
	// free the large shared stack
	if ( _sharedStack != nullptr )
	{
		VirtualFree( _sharedStack, 0, MEM_RELEASE );
		_sharedStack = nullptr;
        _libInfo.allocs--;
	}
	// remove the vectored exception handler
	if ( _handler != nullptr )
	{
		RemoveVectoredExceptionHandler( _handler );
		_handler = nullptr;
	}
}

//
extern
const sLibInfo & getLibInfo( void )
{
	return _libInfo;
}

}; // namespace lg_coroutine