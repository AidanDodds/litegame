#include <cstdio>
#include "lg_assert.h"
#include "lg_dir.h"
#include "lg_dynString.h"

#include <Windows.h>

// will become the executable directory
static char exeDir[ MAX_PATH ] = { '\0' };

static
void getFullPath( void )
{
	int size = GetModuleFileNameA( NULL, exeDir, MAX_PATH );
	lg_assert( size > 0 );
	lg_assert( exeDir[0] != '\0' );

	int lastSlash = 0;
	for ( int i=0; i<size; i++ )
		if ( exeDir[i] == '\\' || exeDir[i] == '/' )
			lastSlash = i;

	lg_assert( lastSlash != 0 );
	lg_assert( lastSlash < size );

	exeDir[ lastSlash+1 ] = '\0';
}

extern
void lg_dir::search( char *search, callback cb )
{
	WIN32_FIND_DATAA findData;
	ZeroMemory( &findData, sizeof( findData ) );
	HANDLE find = FindFirstFileA( search, &findData );

	// find full exe path if required
	if ( exeDir[0] == '\0' )
		getFullPath( );

	if ( find == INVALID_HANDLE_VALUE )
		return;

	lg_cDynString folder( search );
	folder.crop( folder.findRight( '\\' ) );

	do
	{
		// construct a full path
		lg_cDynString path( exeDir );
		path.append( folder );
		path.append( "\\" );
		path.append( findData.cFileName );

		// pass the path to the callback
		cb( (char*) path );
	}
	while ( FindNextFileA( find, &findData ) == TRUE );

	FindClose( find );
}
