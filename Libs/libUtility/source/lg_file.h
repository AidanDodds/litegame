#pragma once

#include "lg_memory.h"
#include "lg_types.h"

namespace lg_file
{

	class cFile
	{
	public:

		// construct unloaded file
		cFile( void );

		// construct and load from disk
		cFile( const char *path );

		// unload when object is destroyed
		~cFile( void )
		    { unload( ); }

		// free the loaded file
		void unload( void );

		// load a file from disk
		bool load( const char *path );

		// check if file is already loaded
		bool isOpen( void ) const;

		// return the file data, null terminated
		byte* getData( void ) const
		    { return data.get(); }

		// get the number of bytes in this file
		int getSize( void )
		    { return size; }

		// read inferred data types
		bool read( u32  & out );
		bool read( u16  & out );
		bool read( byte & out );
        bool read( byte * dst, int nBytes );

        // skip n bytes
        bool skip  ( int nBytes  );

        // set alsolute position
        bool setPos( u32 nOffset );

        // read primative data types
        u32  read_u32 ( void ) { u32  a=0; read( a ); return a; }
        u16  read_u16 ( void ) { u16  a=0; read( a ); return a; }
        byte read_byte( void ) { byte a=0; read( a ); return a; }

        // check the error status
        bool hasError( void )
            { return error; }

	private:

        bool error;
		u32 size;
		u32 head;

		lg_memAlloc<byte> data;
	};

}; // namespace lg_file

typedef lg_file::cFile lg_cFile;