/*
 * note:
 *	This library is ironically not thread safe, so all operation must happen
 *	within one true OS thread.  This is because of the use of static data in
 *	the library.
 *
 *	One micro thread function may not trigger another micro thread to run
 *	at all, so uThread_run() must not be called by a micro thread.
 *
 *	A micro thread function must be declared as follows:
 *
 *	void microThread( void *parameter )
 *	{
 *      ...
 *		uThread_yield( );
 *      ...
 *	}
 */
#pragma once

namespace lg_coroutine
{
	// this structure gets passed to 
	struct sBaton
	{
		void yeild( void );
		void *_oldStack;
	};

    // general library info
    struct sLibInfo
    {
        int threads;
        int allocs;
    };

	typedef void (*cbCoroutineFunc)( void *args );

    // one of the return types from uThread_run indicating the
    // status of a thread post at the point it last called uThread_yield()
    // or terminated.
    namespace eState
    {
	    enum _eState
	    {
		    unspecified = 0,
		    terminated  ,
		    yielding    ,
		    overflowed  ,
		    invalid     ,
	    };
    };

	struct sStack
	{
        sStack( void )
            : data( nullptr )
            , size( 0 )
        { }

    	void *data;
		int   size;
	};

    // both members in this structure are private and must not be modified
    // under any circumstance.
    struct sCoroutine
    {
        sCoroutine( void )
            : stack( )
            , esp( 0 )
        { }

		sStack stack;
		int    esp;
    };

    // create a micro thread and store it in the passed 'thread' structure.
    // the thread_param pointer is also passed to the micro thread function upon first execution. 
    extern bool create   ( sCoroutine &thread, cbCoroutineFunc threadFunc, void *thread_param );

    // called from within a micro thread, this will cause a task switch to the
    // main calling thread again returning from uThread_run
    extern void yield    ( void );

    // status is a return value indicating the staus of the thread post execution
    extern void run      ( sCoroutine &thread, int &status );

    // terminate and free the resources of a thread
    extern void free     ( sCoroutine &thread );

    // dump a threads private stack to standard out for debugging purpouses
    extern void dumpstack( sCoroutine &thread );

    // free all resources allocated by the uThread module.
    // all individual threads must be unallocated with uThread_free() however.
    extern void cleanup  ( void );

    // test if this micro thread is alive
    extern bool isAlive  ( sCoroutine &thread );

    // get stats about the coroutine library
    extern const sLibInfo & getLibInfo( void );

};

typedef lg_coroutine::sCoroutine lg_sCoroutine;
typedef lg_coroutine::sLibInfo   lg_sCoroutineLibInfo;