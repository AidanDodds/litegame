#pragma once

#include "lg_types.h"
#include "lg_stringRef.h"

namespace lg_parser
{

	class cParser
	{
	public:

		cParser( void *stream )
			: _ptr   ( (byte*) stream )
			, _origin( (byte*) stream )
			, _line  ( 0 )
            , _fail  ( false )
		{ }
		
		bool atEOF        ( void );
		void reset        ( void );
		int  getLineNumber( void );
        bool isOk         ( void );

		// comsume if found and return true otherwise false
		bool found( char *string );

        // fails when cant make a match
        bool expect( char *string );

		// read primative data types
        bool read( bool  &out );
		bool read( int   &out );
		bool read( float &out );
		bool read( char *dst, int space );
        bool read( lg_cStringRef & ref );

        // read an entire line
        bool readLine( char *dst, int maxSpace );

		//
		int readint( void );

        // set the fail flag
        void markFail( void );

	private:

		// skip whitespace, newlines and comments
		void advance( void );

        bool  _fail;
		byte *_origin;
		byte *_ptr;
		int   _line;

	};

}; // namespace lg_parser

typedef lg_parser::cParser lg_cParser;