#include "lg_parser.h"
#include "lg_memory.h"

namespace lg_parser
{

// skip over crap
void cParser::advance( void )
{
    bool comment = false;
    
    for ( ; _ptr!='\0' ; _ptr++ )
    {
        char ch = *_ptr;

		// keep track of lines
		_line += (ch=='\n');

        // enter a comment
		if (ch == '#')
        {
            comment = true;
            continue;
        }
            
		// new lines
        if ( ch == '\n' || ch == '\r' )
		{
			comment = false;
            continue;
		}

		// inside a comment so skip characters
        if ( comment )
            continue;

		// spaces
	    if ( ch == ' '  || ch == '\t' )
		    continue;

        break;
    }
}

static
bool readHex( char *dst, int space )
{

}

// read a string
bool cParser::read( char *dst, int space )
{
    advance( );
    
    int i=0;
    for ( ; ; _ptr++, i++ )
    {
        // space including null term
        if ( i >= (space-1) )
            return ! (_fail = true);

        char ch = *_ptr;

        bool valid  = ( ch > ' ' && ch < 127 );
             valid &= ch != ',';
             valid &= ch != ':';
		// visible ascii characters
        if (! valid )
			break;

		dst[i] = ch;
    }

    // insert null term
    dst[i] = '\0';
    return ! ( _fail |= (i==0) );
}

// match a string
bool cParser::found( char *string )
{
    advance( );

	byte *x = _ptr;
	for ( ;; )
	{
		if ( *string == '\0' )
		{
			_ptr = x;
			return true;
		}

		if ( *x == '\0' )		break;
		if ( *x != *string )	break;

		x++;
		string++;
	}
    // do not fail here since we just want to know
    return false;
}

bool cParser::expect( char *string )
{
    if ( found( string ) )
        return true;
    // failed since we have no match
    return ! ( _fail=true);
}

// read a boolean 'true' or 'false'
bool cParser::read( bool  &out )
{
    if ( found( "true" ) )
    {
        out = true;
        return true;
    }
    if ( found( "false" ) )
    {
        out = false;
        return true;
    }
    return ! (_fail = true);
}

// read an integer
bool cParser::read( int &out )
{
	advance( );

    bool hex  = false;
    int  base = 10;

    // check if this is a hex value
    if ( _ptr[0] == '0' )
        if ( _ptr[1] == 'x' )
        {
            base  = 16;
            _ptr += 2;
            hex   = true;
        }

	int v = 0;
    int i = 0;
	for ( ; ; _ptr++, i++ )
	{
		char c = *_ptr;

		if ( c >= '0' && c <= '9' )
        {
            v *= base;
			v +=  c - '0';
        }
        else
        if ( (c >= 'a' && c <= 'f') && hex )
        {
            v *= base;
            v += (c - 'a') + 10;
        }
		else
        {
            if ( i==0 )
                _fail = true;

            out = v;

            return ! _fail;
        }
	}
    
}

// read a floating point number
bool cParser::read( float &out )
{
	advance( );

	int  invert = 1;
	s64  value  = 0;
	u64  scale  = 0;
	if ( *_ptr == '-' ) { invert=-1; _ptr++; }

	for ( int i=0; ; _ptr++, i++ )
	{
		char c = *_ptr;
		if ( c >= '0' && c <= '9' )
		{
			value  = (value * 10) + (c - '0');
			scale *= 10;
			continue;
		}
		if ( c == '.' && scale == 0 )
		{
			scale = 1;
			continue;
		}
#if 0
		// scientific notation
        if ( c == 'e' && _ptr[1] == '-' )
        {
            int exp = _ptr[2] - '0';
            for ( int i=0; i<exp; i++ )
				scale = (scale * 10);
            _ptr += 2;
            continue;
        }
#endif
		// strange things in the stream
        if ( i==0 )
            return !(_fail = true);
        break;
	}

    double v = ((double)(value)) * invert;
        
	if ( scale == 0 )
		out = (float) v;
    else
		out = (float)(v / ((double)scale));

	return true;
}

// at the end of a file
bool cParser::atEOF( void )
{
	return *_ptr == '\0';
}

// go back to the start of the file
void cParser::reset( void )
{
	_ptr  = _origin;
	_line = 0;
    _fail = false;
}

// get the current line number
int cParser::getLineNumber( void )
{
    // make base 1
	return _line + 1;
}

bool cParser::isOk( void )
{
    return !_fail;
}

void cParser::markFail( void )
{
    _fail |= true;
}

int cParser::readint( void )
{
	int val = 0; read( val ); return val;
}

bool cParser::read( lg_cStringRef &ref )
{
    //XXX: seems horrible do something better
    lg_shortString * str = lg_memory_new<lg_shortString>( true );

    if (! read( str->data, sizeof( lg_shortString ) ) )
        return false;
    
    ref.set( str->data );

    return true;
}

bool cParser::readLine( char *dst, int maxSpace )
{
    int i=0;
    for ( i=0; *_ptr != '\0'; i++ )
    {
        if (i >= (maxSpace-1))
            return false;

        char ch = *(_ptr++);

        if ( ch == '\r' )
        {
            i--;
            continue;
        }

        if ( ch == '\n' )
        {
            _line++;
            break;
        }

        // move into destination buffer
        dst[i] = ch;
    }

    // push null char
    dst[i] = '\0';

    return true;
}

};