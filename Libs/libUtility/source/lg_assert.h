#pragma once

// simple assert
#ifdef _DEBUG
# define lg_assert( X, ... ) { if (! (X) ) __asm { int 3 } }
#else
# define lg_assert( X, ... )
#endif

