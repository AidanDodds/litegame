#pragma once

typedef unsigned char      byte;
typedef unsigned int       u32;
typedef   signed int       s32;
typedef unsigned long long u64;
typedef   signed long long s64;
typedef unsigned short     u16;

typedef unsigned int rgb_t;

/*
typedef   signed int	sint32;
typedef unsigned int	uint32;
typedef unsigned char	uint8;
 */

template <typename T>
struct rect
{
	rect( void )
	{ }

	rect( T _x1, T _y1, T _x2, T _y2 )
		: x1( _x1 )
		, y1( _y1 )
		, x2( _x2 )
		, y2( _y2 )
	{ }

	T x1, y1, x2, y2;
};

typedef rect<s32>   recti;
typedef rect<float> rectf;