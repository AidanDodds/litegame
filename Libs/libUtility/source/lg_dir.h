#pragma once

namespace lg_dir
{
	typedef void (*callback)( char *file );

	void search( char *folder, callback cb );
};