#include <stdio.h>
#include "lg_file.h"
#include "lg_assert.h"
#include "lg_memory.h"

namespace lg_file
{

// construct unloaded file
cFile::cFile( void )
    : data ( )
    , size ( 0       )
    , head ( 0       )
    , error( false   )
{ }

// construct and load from disk
cFile::cFile( const char *path )
    : data ( )
    , size ( 0       )
    , head ( 0       )
    , error( false   )
{
	load( path );
}

bool cFile::load( const char *path )
{
	unload( );

	// load the obj file
	FILE *file = nullptr;
	fopen_s( &file, path, "rb" );
	if ( file == nullptr )
		return false;

	// find the file size
	fpos_t sizet = 0;
	fseek  ( file, 0, SEEK_END );
	fgetpos( file, &sizet );
	fseek  ( file, 0, SEEK_SET );
	size = (int)sizet;

	// allocate data for this file
    data.alloc( size + 1 );
    lg_assert( data.get() != nullptr );

    if ( data.get() != nullptr )
    {
	    // read in the entire file
	    if ( fread( data.get(), 1, size, file ) != size )
		    unload( );
        else
	        // add an EOF character
    	    data[ size ] = '\0';
    }
    else
        error = true;
	
	// close this file after we are done with it
    if ( file != nullptr )
    	fclose( file );

	return !error;
}

void cFile::unload( void )
{
    data.release();
	size = 0;
}

bool cFile::isOpen( void ) const
{
	return data.get() != nullptr;
}

bool cFile::skip( int nBytes  )
{
    if ( (head+nBytes) < 0 || (head+nBytes) >= size )
        return ! (error = true);

    head += nBytes;
    return !error;
}

bool cFile::setPos( u32 offset )
{
    if ( offset < 0 || offset >= size )
        return ! (error = true);

    head = offset;
    return !error;
}

// read primative data types
bool cFile::read( u32  & out )
{
    const int ds = sizeof( u32 );
    if ( head+ds > size )
        return !( error = true );
    out = *((u32*) & data[head]);
    head += ds;
    return !error;
}

bool cFile::read( u16  & out )
{
    const int ds = sizeof( u16 );
    if ( head+ds > size )
        return !( error = true );
    out = *((u16*) & data[head]);
    head += ds;
    return !error;
}

bool cFile::read( byte & out )
{
    if ( head+1 > size )
        return !( error = true );
    out = data[ head++ ];
    return !error;
}

bool cFile::read( byte * dst, int nBytes )
{
    if ( size < 0 || (head+nBytes) >= size )
        return ! (error = true);
    for ( int i=0; i<nBytes; i++ )
        dst[i] = data[head + i];
    head += nBytes;
    return !error;
}

}; // namespace lg_file