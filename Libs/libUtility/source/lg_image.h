#pragma once

#include "../../libMut/mut.h"

#include "lg_types.h"
#include "lg_assert.h"
#include "lg_file.h"
#include "lg_memory.h"

//
// XXX: Add RLE image support
// XXX: How to handle sub frames?
//
namespace lg_image
{

    class cImage
    {
        
//        u32        *data;
        lg_memAlloc<u32> data;
        mut::vec2i  size;
        u32         mask;
        recti       frame;

        bool loadBitmap( lg_cFile & file );

    public:
        
        cImage( void )
            : data( )
            { }

        ~cImage( void )
            { unload( ); }

        // create blank texture
        bool create( const mut::vec2i &size );

        // load from file
        bool load  ( const char *path );

        // free resources
        void unload( void );

        // get the size of an image
        const mut::vec2i & getSize( void ) const
        {
            return size;
        }

        //
        const recti & getFrame( void ) const
        {
            return frame;
        }

        // set a pixel colour
        bool setPixel( int x, int y, u32 c );

        // get a pixels value
        u32  getPixel( int x, int y ) const
		{
            lg_assert( data.get() != nullptr );
			if ( x<0 || x >= size.x || y<0 || y >= size.y )
				return false;
			return data[ x + y * size.x ];
		}

        // get the raw pixels
        u32 *getPixels( void ) const
        {
            return data.get();
        }

        // 
		void setMask( u32 aMask );

		u32 getMask( void ) const
		{
			return mask;
		}

    };

};

typedef lg_image::cImage lg_cImage;