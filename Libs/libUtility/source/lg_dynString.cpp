#include <string.h>

#include "lg_types.h"
#include "lg_dynString.h"

const int chunkSize = 64;

int fitSize( int min )
{
	int x  = ((min+1) / chunkSize);
		x += 1;
	return x * chunkSize;
}

lg_cDynString::~cDynString( )
{
	if ( _data != NULL )
		delete [] _data;
	_data   = NULL;
	_space  = 0;
	_length = 0;
}

lg_cDynString::cDynString( void ) :
	_data  ( NULL ),
	_space ( 0    ),
	_length( 0    )
{
}

lg_cDynString::cDynString( const char *ref ) :
	_data  ( NULL ),
	_space ( 0    ),
	_length( 0    )
{
	append( ref );
}

inline void lg_cDynString::resize( int minSize )
{
	// room needed
	int newSize = fitSize( minSize );
	// stupid unless we realy need more _space
	if ( newSize <= _space )
		return;
	// allocate new _space
	char *newData = new char [ newSize ];
	lg_assert( newData != 0 );
	newData[ 0 ] = '\0';
	// copy if needed
	if ( _length > 0 )
	{
		lg_assert( _data != NULL );
		strcpy_s( newData, newSize, _data );
//		lg_assert( newData[0] != '\0' );
	}
	// delete old _data buffer
	if ( _data != 0 )
	{
		// in future store for reuse
		delete [] _data;
		_data = NULL;
	}
	// copy over usefull values
	_data  = newData;
	_space = newSize;
	
	lg_assert( _data   != 0 );
	lg_assert( _space  != 0 );
}

void lg_cDynString::append( const char *str )
{
	lg_assert( str != 0 );
	// get the input string _length
	int len = strlen( str );
	// try to resize, will fail if not needed
	resize( _length + len );
	// should always be enough _space now
	lg_assert( _space > (_length+len) );
	// there is enough _space so simple append
	memcpy( _data+_length, str, len );
	// _length just increased
	_length += len;
	// make sure to write a terminal
	_data[ _length ] = '\0';
}

void lg_cDynString::append( const lg_cDynString &in )
{
	// get the input string _length
	if ( in._length <= 0 )
		return;
	// try to resize, will fail if not needed
	resize( _length + in._length );
	// should always be enough _space now
	lg_assert( _space > (_length+in._length) );
	// there is enough _space so simple append
	memcpy( _data+_length, in._data, in._length );
	// _length just increased
	_length += in._length;
	// make sure to write a terminal
	_data[ _length ] = '\0';
}

void lg_cDynString::validate( void )
{
	if ( _data == 0 )
	{
		_length = 0;
		_space  = 0;
	}
	else
	{
		// _space should never be invalid
		_length = strlen( _data );
	}
}

void lg_cDynString::append( const char ch )
{
	resize( _length+1 );
	_data[ _length++ ] = ch;
}

int lg_cDynString::findRight( char ch )
{
	int i = _length-1;
	for ( ;i >= 0; i-- )
		if ( _data[ i ] == ch )
			return i;
	return -1;
}

int lg_cDynString::findLeft( char ch )
{
	for ( int i=0; i <_length; i++ )
		if ( _data[ i ] == ch )
			return i;
	return -1;
}

void lg_cDynString::crop( int nLen )
{
	if ( nLen >= _length )	return;
	if ( nLen <  0       )	return;
	//
//	for ( int i=nLen; i<_length; i++ )
//		_data[ i ] = ' ';
	// 
	_data[ nLen ] = '\0';
	_length = nLen;
}

/*
void lg_cDynString::print( const char *str, ... )
{
	char buf[ 2 ] = { 0, '\0' };

	for ( ;str[0] != '\0'; str++ )
	{
		if ( str[0] == '%' )
			switch ( str[1] )
			{
			case ( 's' ):

			}
		else
			append( buf );
	}
}
*/