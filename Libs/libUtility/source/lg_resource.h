#pragma once

#include "lg_types.h"
#include "lg_assert.h"

namespace util
{

typedef unsigned int resource_t;

enum
{
    INVALID_RESOURCE_ID = -1
};

// resource manager to decouple ownership of objects
template <class T, int nItems>
class lg_cResource
{
    T   *data[ nItems ];
    int  refs[ nItems ];

    lg_cResource( const lg_cResource & );

    lg_cResource( void )
    {
        for ( int i=0; i<nItems; i++ )
        {
            refs[ i ] = 0;
            data[ i ] = nullptr;
        }
    }

public:

    // singleton instance interface
    static lg_cResource &inst( void )
    {
        static lg_cResource instance;
        return instance;
    }

    // add a resource to the system
    resource_t add( T *item )
    {
        lg_assert( item != nullptr );
        for ( int i=0; i<nItems; i++ )
        {
            if ( refs[ i ] > 0 )
                continue;
            data[ i ] = item;
            refs[ i ] = 1;
            return (resource_t)i;
        }
        return INVALID_RESOURCE_ID;
    }

    T * find( resource_t id )
    {
        lg_assert( id>=0 && id<nItems );
        lg_assert( data[ id ] != nullptr );
        lg_assert( refs[id] > 0 );
        return data[ id ];
    }
    
    void release( resource_t id )
    {
        lg_assert( id>=0 && id<nItems );
        lg_assert( data[ id ] != nullptr );
        lg_assert( refs[ id ] > 0 );
        refs[ id ]--;
    }

    //
    void aquire( resource_t id )
    {
        lg_assert( id>=0 && id<nItems );
        lg_assert( data[ id ] != nullptr );
        lg_assert( refs[ id ] > 0 );
        refs[ id ]++;
    }

};

}; /* namespace util */