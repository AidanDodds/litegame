#include <intrin.h>
#include <SDL/SDL.h>
#include "lg_profile.h"
#include "lg_log.h"

// neat infos:
//		void* _rdteb( );
//		returns a pointer to the thread environment block

// cycles per millisecond
static u32 _cpms = 0;

static unsigned __int64 _lcap = 0;

static inline unsigned __int64 rdtscp( void )
{
	unsigned int y = 0;
	return __rdtscp( &y );
//	return __rdtsc( );
}

bool lg_profile::init( void )
{
	// check if rdtsc is supported
	{
		// check bit 27 of CPUInfo[3]
		int x[] = { 0, 0, 0, 0 };
		// InfoType = 0x80000001
		int y = 0x80000001;
		__cpuid( x, y );
		// check if rtsc is supported
		if (! (x[3] & (1<<27)) )
		{
			lg_log::print( "rdtscp not supported by CPU" );
			return false;
		}
		else
			lg_log::print( "rdtscp is supported by CPU" );
	}

	// work out aproximate cycles per ms
	{
		unsigned __int64 x = 0;
		Uint32 delta = 0;
		
		x     = rdtscp( );
		delta = SDL_GetTicks();

		// stall for ~100ms
		SDL_Delay( 100 );

		x     = rdtscp( ) - x;
		delta = SDL_GetTicks() - delta;

		// finaly arrive at a cycles per ms aproximation
		_cpms = (Uint32)(x / delta);

		// print this to the log
		lg_log::print( "cycles per ms = %d", _cpms );
	}

	return true;
}

void lg_profile::enter( void )
{
	_lcap = rdtscp( );
}

u64 lg_profile::leave( void )
{
	return rdtscp() - _lcap;
}