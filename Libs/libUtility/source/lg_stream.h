#pragma once
#include "lg_types.h"

namespace lg_stream
{

    class cStream
    {
    public:

	     cStream( void );
         cStream( const u32 size );
        ~cStream( );

	    char * get( void );

	    void printf( const char *, ... );

	    void reset( void );

    private:

	    char * data;
	    int    capacity;
	    int    head;

    };

};

typedef lg_stream::cStream lg_cStream;