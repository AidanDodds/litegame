#pragma once
#include <stdarg.h>
#include <stdio.h>

#include "lg_types.h"
#include "lg_log.h"

static char *logName = "log.txt";

namespace lg_log
{

// buffer for constructing log messages from arguments
static char _logBuffer[ 1024 ] = { '\0' };

// output log file handler
static FILE *_logFile = NULL;

// print to log file
void print( char *text, ... )
{
	if ( _logFile == NULL )
	{
		if ( fopen_s( &_logFile, logName, "wb" ) != 0 )
			return;
	}

	// open argument list
	va_list args;
	va_start( args, text );

	// print variable arg list to _logBuffer
	int cw = vsnprintf_s( _logBuffer, sizeof( _logBuffer ), text, args );

	// print _logBuffer to the log file
	fputs( _logBuffer, _logFile );
	
	// enforce that we must have a new line at the end
	if ( cw >= 0 )
	{
		if ( _logBuffer[cw] != '\n' )
			fputc( '\n', _logFile );
	}

	// close the argument list
	va_end( args );
}

// utility class for creating and deleting log file
// at initalization time
struct logInitalizer
{
	logInitalizer( const char *path )
	{
	}

	~logInitalizer( )
	{
		if ( _logFile != NULL )
		{
			// flush the stream
			fflush( _logFile );
			// close the log file
			fclose( _logFile );
		}
		_logFile = NULL;
	}
};

// create the log at initalization time
static logInitalizer _logInit( "log.txt" );

}; // namespace log