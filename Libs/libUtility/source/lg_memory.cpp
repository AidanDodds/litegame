// #include <new.h>

#include "lg_memory.h"

lg_memory * lg_memory::_inst = nullptr;

bool lg_memory::start( void )
{
    lg_assert( _inst == nullptr );
    _inst = new lg_memory( );
    return true;
}

bool lg_memory::stop( void )
{
    lg_assert( _inst != nullptr );
    delete _inst;
    return true;
}

// allocate memory
void *lg_memory::memAlloc( int size, bool zero )
{
    lg_assert( size > 0 );

    // allocate new memory
    char *mem = new char[ size ];
    lg_assert( mem != nullptr );
    // zero if requested
    if ( zero )
        for ( int i=0; i<size; i++ )
            mem[ i ] = 0;
    // return the memory
    return (void*) mem;
}

void lg_memory::memRelease( void *ptr )
{
    lg_assert( ptr != nullptr );
    
    delete [] ptr;
}

// scratch memory for short term allocations
void *lg_memory::scratchAlloc( int size, bool zero )
{
    //XXX:  for now just use generic allocator
    //      later switch to preallocated chunk thing
    return memAlloc( size, zero );
}

void lg_memory::scratchRelease( void *ptr )
{
    memRelease( ptr );
}

// get memory usage
const lg_memory::sStats & lg_memory::getStats( void )
{
    return stats;
}
