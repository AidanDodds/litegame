#pragma once
#include "lg_types.h"

namespace lg_string
{

    struct cShortString
    {
        char data[ 32 ];
    };

    class cStringRef
	{
    private:

        const char *_string;
        u32 _hash;

        void genHash( void )
        {
            const char *s = _string;
            u32 c = 0;
            while (*s != '\0')
            {
                c = *s++;
                _hash = c + (_hash << 6) + (_hash << 16) - _hash;
            }
        }

	public:
        
        cStringRef( void )
            : _string( nullptr )
            , _hash  ( 0 )
        { }

		cStringRef( const char *str )
            : _string( str )
            , _hash  ( 0 )
		{
            genHash( );
		}

        bool equals( const cStringRef &ident ) const
        {
            if ( _hash != ident._hash )
                return false;

            const char *a = _string;
            const char *b = ident._string;

            // pointer comparison
            if ( a==b )
                return true;

            // null check
            if ( a==nullptr || b==nullptr )
                return false;

            // final string compare (unroll?)
            for ( int i=0 ;; i++ )
            {
                if ( a[i] != b[i] ) return false;
                if ( a[i] == '\0' ) return true;
            }
        };

        void set( const char *str )
        {
            _string = str;
            genHash( );
        }

        bool operator == ( const cStringRef &other ) const 
        {
            return equals( other );
        }

        bool operator != ( const cStringRef &other ) const 
        {
            return !equals( other );
        }

        u32 hash( void ) const
        {
            return _hash;
        }

        const char * get( void ) const
        {
            return _string;
        }

    };

#if 0
    static inline shortString_t & shortString( const char *str )
    {
        shortString_t ss = pooledAlloc<shortString_t>( false );
        int size = 
        for ( int i=0; i<sizeof( ss ); i++ )
    }
#endif

}; // lg_string

typedef lg_string::cStringRef   lg_cStringRef;
typedef lg_string::cShortString lg_shortString;
