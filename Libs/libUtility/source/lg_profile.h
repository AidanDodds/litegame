#pragma once

#include "lg_types.h"

namespace lg_profile
{
	bool init ( void );
	void enter( void );
	u64  leave( void );
};