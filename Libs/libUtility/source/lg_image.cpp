#include "lg_image.h"
#include "lg_memory.h"

//
bool lg_cImage::loadBitmap( lg_cFile &file )
{
    file.setPos( 0 );
    // check header field
    if ( file.read_u16( ) != 'MB' ) // header field
        return false;

    u32 bsz  = file.read_u32(    ); // bitmap size
               file.skip    ( 4  );
    u32 offs = file.read_u32(    ); // pixel offset

               file.setPos  ( 14 );
               file.skip    ( 4  );
    size.x   = file.read_u32(    ); // image size
    size.y   = file.read_u32(    );
               file.skip    ( 2  );
    u32 bpp  = file.read_u16(    ); // bits per pixel
    u32 comp = file.read_u32(    ); // compression method

    // check compression method is BI_RGB
    // 0 = BI_RGB
    // 6 = BI_ALPHABITFIELDS
    if ( comp != 0 || (bpp!=24 && bpp!=32) )
        return false;

    // jump to start of pixel data
    file.setPos( offs );
    // find number of pixels
    u32 nPixels = size.x * size.y;
    lg_assert( nPixels > 0 );
    data.alloc( nPixels );
    lg_assert( data.get() != nullptr );
    
    // copy pixels in as 32bits
    switch ( bpp )
    {
    case ( 24 ):
        {
            // the size of each row must be a multiple of 4 bytes
            int padding = (4 - ((size.x * 3) & 3)) % 4;
            // 
            for ( int y=size.y-1; y>=0; --y )
            {
                // read a row
                for ( int x=0; x<size.x; ++x )
                {
                    u32 c  = ((u32)file.read_byte( )) << 0;
                        c |= ((u32)file.read_byte( )) << 8;
                        c |= ((u32)file.read_byte( )) << 16;
                    setPixel( x, y, c );
                }
                // read any padding bytes
                for ( int i=0; i<padding; i++ )
                    file.read_byte( );
            }
            break;
        }
    case ( 32 ):
        {
            for ( int y=size.y-1; y>=0    ; --y )
            for ( int x=0       ; x<size.x; ++x )
            {
                u32 c  = ((u32)file.read_byte( )) << 0;
                    c |= ((u32)file.read_byte( )) << 8;
                    c |= ((u32)file.read_byte( )) << 16;
                    c |= ((u32)file.read_byte( )) << 24;
                setPixel( x, y, c );
            }
            break;
        }
    }

    // make a default frame
    frame.x1 = 0;
    frame.y1 = 0;
    frame.x2 = size.x;
    frame.y2 = size.y;

    return !file.hasError( );
}

// create blank texture
bool lg_cImage::create( const mut::vec2i &s )
{
    unload( );

    if ( s.x <= 0 || s.y <= 0 )
        return false;

    size = s;
    int nPixels = s.x * s.y;
//    data = new u32[ s.x * s.y ];
    data.alloc( nPixels );
    lg_assert( data.get() != nullptr );

    return true;
}

// load from file
bool lg_cImage::load( const char *path )
{
    // load the image file
    lg_cFile file( path );
    if (! file.isOpen( ) )
        return false;

    // load shit here
    bool res = loadBitmap( file );

    // free resources
    file.unload( );
    return res;
}

// free resources
void lg_cImage::unload( void )
{
    if ( data.get() != nullptr )
        data.release();

    size = mut::vec2i( 0, 0 );
}

// pixel manipulation
bool lg_cImage::setPixel( int x, int y, u32 c )
{
    lg_assert( data.get() != nullptr );

    if ( x<0 || x >= size.x || y<0 || y >= size.y )
		return false;

    data[ x + y * size.x ] = c;

    return true;
}

// 
void lg_cImage::setMask( u32 in )
{
    mask = in;
}
