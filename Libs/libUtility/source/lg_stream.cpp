#include <stdarg.h>
#include <stdio.h>

#include "lg_assert.h"
#include "lg_stream.h"

lg_cStream::cStream( void )
	: data    ( nullptr )
	, capacity( 0 )
	, head    ( 0 )
{
}

lg_cStream::cStream( const u32 size )
	: data    ( nullptr )
	, capacity( size )
	, head    ( 0 )
{
    lg_assert( size > 0 );
	data = new char [ size ];
	lg_assert( data != nullptr );
}

lg_cStream::~cStream( )
{
    if ( data != nullptr )
        delete [] data;
    data = nullptr;
}

char * lg_cStream::get( void )
{
	return data;
}

void lg_cStream::printf( const char *str, ... )
{
	va_list args;
	va_start( args, str );

	int space   = capacity-head;
	int written = vsprintf_s( &data[ head ], space, str, args );

	lg_assert( written >= 0 );
	lg_assert( capacity > head+written );

	head += written;

    lg_assert( head < capacity );
	data[ head ] = '\0';
}

void lg_cStream::reset( void )
{
	head = 0;
	data[ 0 ] = '\0';
}
