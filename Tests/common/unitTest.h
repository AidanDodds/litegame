#pragma once

#define check( X ) { if (! (X) ) return false; }