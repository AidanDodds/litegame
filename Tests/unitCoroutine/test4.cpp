
#include "../common/unitTest.h"
#include "libUtility/source/lg_coroutine.h"

static
unsigned int rfib( unsigned int n )
{
	if (n == 0) return 0;
	if (n == 1) return 1;
	return rfib(n - 1) + rfib(n - 2);
}

static
unsigned int fib( unsigned int n )
{
    lg_coroutine::yield( );

	if (n == 0) return 0;
	if (n == 1) return 1;

	return fib(n - 1) + fib(n - 2);
}

static
void worker( void *x )
{
	int *y = (int*)x;
	*y = fib( *y );
}

extern bool test4( void )
{
	const int input = 25;
	int result = rfib( input );

	int param = input;

    lg_sCoroutine t1;
    check( lg_coroutine::create( t1, worker, (void*) &param ) );

	while ( true )
	{
		int status;
		lg_coroutine::run( t1, status );
		if ( status == lg_coroutine::eState::yielding )
			continue;
		if ( status == lg_coroutine::eState::terminated )
			break;
		// else must be an error
		return false;
	}

	check( param == result );

	return true;
}