
#include "../common/unitTest.h"
#include "libUtility/source/lg_coroutine.h"

static
void thread1( void *param )
{
    int *p = (int*) param;

    *p = 1;
    lg_coroutine::yield( );

    *p = 2;
    lg_coroutine::yield( );

    *p = 3;
    lg_coroutine::yield( );
}

extern bool test1( void )
{
    lg_sCoroutine coroutine;
    
    int status = 0;
    int param  = 0;

    if (! lg_coroutine::create( coroutine, thread1, &param ) )
        return false;

    check( param == 0 );

    lg_coroutine::run( coroutine, status );
    check( status == lg_coroutine::eState::yielding );
    check( param == 1 );

    lg_coroutine::run( coroutine, status );
    check( status == lg_coroutine::eState::yielding );
    check( param == 2 );

    lg_coroutine::run( coroutine, status );
    check( status == lg_coroutine::eState::yielding );
    check( param == 3 );

    lg_coroutine::run( coroutine, status );
    check( status == lg_coroutine::eState::terminated );

	lg_coroutine::free( coroutine );

    return true;
}
