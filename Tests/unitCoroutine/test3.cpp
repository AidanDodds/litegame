
#include "../common/unitTest.h"
#include "libUtility/source/lg_coroutine.h"
#include "libMut/mut.h"

#include <stdio.h>

const int nThreads = 40;
lg_sCoroutine thread[ nThreads ];

static
void threadProc( void *param )
{
	int rand = mut::rand::randi( 3, 20 );
	for ( int i=0; i<rand; i++ )
	{
		lg_coroutine::yield( );
	}
}

extern bool test3( void )
{
	// init all threads
	for ( int i=0; i<nThreads; i++ )
		check( lg_coroutine::create( thread[i], threadProc, nullptr ) );

	// just a few itterations
	for ( int i=0; i<100000; i++ )
	{
		// update each thread
		for ( int i=0; i<nThreads; i++ )
		{
			int status = 0;
			lg_coroutine::run( thread[i], status );
			switch ( status )
			{
			// when one dies, we must create another in its place
			case ( lg_coroutine::eState::terminated ):
				{
					lg_coroutine::free( thread[i] );
					check( lg_coroutine::create( thread[i], threadProc, nullptr ) );
				}
				break;

			// if we yeild then thats ok
			case ( lg_coroutine::eState::yielding ):
				continue;

			// if its something else then we have a big problem
			default:
				for ( int i=0; i<nThreads; i++ )
					lg_coroutine::free( thread[i] );

				lg_coroutine::cleanup( );
				return false;
			}
		}

		// we dont want our stuff to be growning
        check( lg_coroutine::getLibInfo().threads == nThreads );
	}
	
	// release all threads
	for ( int i=0; i<nThreads; i++ )
		lg_coroutine::free( thread[i] );
    check( lg_coroutine::getLibInfo().threads == 0 );

	// clean up any extras
	lg_coroutine::cleanup( );

    check( lg_coroutine::getLibInfo().threads == 0 );
    check( lg_coroutine::getLibInfo().allocs  == 0 );
	return true;
}