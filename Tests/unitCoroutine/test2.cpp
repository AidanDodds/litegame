
#include "../common/unitTest.h"
#include "libUtility/source/lg_coroutine.h"

#pragma warning( push )
#pragma warning( disable: 4717 )
static
void overflow( void )
{
	char x[ 123 ];
	for ( int i=0; i<sizeof(x); i++ )
		x[i] = i & 0xff;

	overflow( );
}
#pragma warning( pop )

static
void thread( void *param )
{
	overflow( );
}

extern bool test2( void )
{
    lg_sCoroutine coroutine;
    
    int status = 0;

    if (! lg_coroutine::create( coroutine, thread, nullptr ) )
        return false;

    lg_coroutine::run( coroutine, status );
    check( status == lg_coroutine::eState::overflowed );
	
	lg_coroutine::free( coroutine );

    return true;
}
