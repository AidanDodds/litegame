#include <stdio.h>

#include "../../Libs/libLiteGame/liteGame.h"

#ifdef _MSC_VER
#include <Windows.h>

bool isDebugged = false;

LONG CALLBACK _handler(  PEXCEPTION_POINTERS exInfo )
{
	// signal a faliure
	exit( -1 );
	// continue
	return EXCEPTION_CONTINUE_EXECUTION;
}
#endif

extern bool test1( void );
extern bool test2( void );
extern bool test3( void );
extern bool test4( void );

int main( int argc, char **args )
{
    // check if the debugger is attached
	if ( IsDebuggerPresent( ) == TRUE )
        isDebugged = true;

    if (! isDebugged )
	{
		if ( AddVectoredExceptionHandler( 0, _handler ) == 0 )
        {
			printf( "failed to setup testing...\n" );
            return -1;
        }
	}

    if (! lg_liteGame::start( ) )
        return false;

	bool status = true;
    
	printf( "test1...\n" );
	status &= test1( );
	printf( "test2...\n" );
	status &= test2( );
	printf( "test3...\n" );
	status &= test3( );
	printf( "test4...\n" );
	status &= test4( );

    if ( isDebugged )
    {
        if ( status )   printf( "pass\n" );
        else            printf( "fail\n" );
        getchar( );
    }
    
    lg_liteGame::stop( );

	// return fail if any test fails
	if ( status )	return  1;
	else			return -1;
}