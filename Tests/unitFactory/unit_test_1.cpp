#include "../common/unitTest.h"

#include "libUtility/source/lg_stringRef.h"
#include "libUtility/source/lg_assert.h"

#include "libLiteGame/liteGame.h"

bool error = false;

lg_cBasicCollector collector;

static int nTicks = 0;

class cEntity1 
    : public lg_cEntity
{
public:
	_typeInfo( cEntity1, nullptr );

	cEntity1( void ) : cEntity( ) { };
    virtual ~cEntity1( void ) { }
    
    virtual void onCollide( cEntity &other )    { }
    virtual int  onGetState( void )             { return 0; }

   	virtual void onTick( void )
    { nTicks++; }

    virtual void onDestroy( void )
    { }

	virtual bool onCreate ( void )
	{
        return true;
	}

    //
    static lg_cEntity *create( void )
    {
        return new cEntity1;
    }

    static void destroy( lg_cEntity *e )
    {
        cEntity1 *ce = static_cast<cEntity1*>( e );
        delete ce;
    }

    void kill( void )
    {
        references--;
    }

};


class cEntity2
    : public cEntity1
{
public:
	_typeInfo( cEntity2, &cEntity1::RTTI() );

	cEntity2( void ) : cEntity1( ) { };
    virtual ~cEntity2( void ) { }

    virtual void onCollide( cEntity &other )    { }
    virtual int  onGetState( void )             { return 0; }

	virtual void onTick( void )
	{ nTicks++; }

	virtual void onDestroy( void )
	{ }

	virtual bool onCreate( void )
	{
        return true;
	}

    //
    static lg_cEntity *create( void )
    {
        return new cEntity2;
    }

    static void destroy( lg_cEntity *e )
    {
        cEntity2 *ce = static_cast<cEntity2*>( e );
        delete ce;
    }

    void kill( void )
    {
        references--;
    }

};

class cEntity3 : public cEntity2
{
public:
	_typeInfo( cEntity3, &cEntity2::RTTI() );

};

void cbTraverse( lg_cEntity *a, const lg_sTraverseParams &param )
{
    lg_assert( a->type() != cEntity3::RTTI( ) );

    if ( a->type( ) == cEntity2::RTTI( ) )
    {
        cEntity2 *e = static_cast<cEntity2*>( a );
        e->kill( );
        return;
    }

    if ( a->type( ) == cEntity1::RTTI() )
    {
        cEntity1 *e = static_cast<cEntity1*>( a );
        e->kill( );
        return;
    }

    lg_assert(! "should never get here" );
}

extern bool unit_test_1( void )
{
    lg_factory::setCollector( collector );

    check( collector.size() == 0 );

    // register our creators
    lg_factory::addCreator( cEntity1::RTTI(), cEntity1::create, cEntity1::destroy );
    lg_factory::addCreator( cEntity2::RTTI(), cEntity2::create, cEntity2::destroy );

    // scope for references
    {

        // create a cEntity1
        lg_cEntityRef e1;
        check( lg_factory::create( cEntity1::RTTI(), mut::vec2( ), e1 ) )
        check( e1.get() != nullptr );
        check( e1->type() == cEntity1::RTTI() );
    
        // we expect to have one item in our collection
        check( collector.size() == 1 );

        // create a cEntity2
        lg_cEntityRef e2;
        check( lg_factory::create( cEntity2::RTTI(), mut::vec2( ), e2 ) );
        check( e2.get() != nullptr );
        check( e2->type() == cEntity2::RTTI() );

        // check these types are not the same
        check( e1->type() != e2->type( ) );
    
        // we expect to have two items in our collection
        check( collector.size() == 2 );

        // expect this to fail because cEntity3::RTTI has not been registered
        check(! lg_factory::create( cEntity3::RTTI(), mut::vec2( ) ) );

        // tick both items once
        check( nTicks == 0 );
        collector.tickAll( );
        check( nTicks == 2 );
    }

    // little test of traverse
    {
        lg_sTraverseParams trav;
        collector.traverse( cbTraverse, trav );
        check( collector.size() == 2 );
        collector.clean( );
        check( collector.size() == 0 );
    }

	return true;
}