#include "../common/unitTest.h"

#include "libLiteGame/liteGame.h"

class ea
{
public:
	_typeInfo( ea, nullptr );
};

class eb
    : public ea
{
public:
	_typeInfo( eb, &ea::RTTI() );
};

class ec
    : public eb
{
public:
	_typeInfo( ec, &eb::RTTI() );
};

class ed
{
public:
	_typeInfo( ed, nullptr );
};

extern bool unit_test_rtti( void )
{
    ea *e1 = new ea;
    eb *e2 = new eb;
    ec *e3 = new ec;

    check( e1->type() == ea::RTTI( ) );
    check( e2->type() == eb::RTTI( ) );
    check( e3->type() == ec::RTTI( ) );
    
    check( e1->type() != ec::RTTI( ) );
    check( e1->type() != eb::RTTI( ) );
    check( e3->type() != ed::RTTI( ) );

	return true;
}