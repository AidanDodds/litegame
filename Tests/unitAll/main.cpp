#include <Windows.h>
#include <intrin.h>
#include <stdio.h>
#include <string.h>

#define assert( X ) { if (! (X) ) __debugbreak(); }

static char path[ 256 ];

char *strLastSlash( char *a )
{
	char *x = a;
	for ( ;*a != '\0' ; a++ )
	{
		if ( *a == '\\' || *a == '/' )
			x = a+1;
	}
	return x;
}

bool strMatch( char *a, char *b )
{
	for ( ;; )
	{
		if ( *a == '\0' || *b == '\0' )
			return true;
		// get ascii values
		int ca = *a;
		int cb = *b;
		// to lower case
		if (ca>='A' && ca<='Z') ca = (ca-'A') + 'a';
		if (cb>='A' && cb<='Z') cb = (cb-'A') + 'a';
		// bomb out on mismatch
		if ( ca != cb )
			return false;
		// skip on
		a++; b++;
	}
}

void getPath( void )
{
	int size = GetModuleFileNameA( NULL, path, sizeof( path ) );
	assert( size > 0 );
	assert( path[0] != '\0' );

	char *x = strLastSlash( path );
	if ( x != path )
		*x = '\0';
}

void runTest( char *name )
{
	printf( "> '%s'\n", name );

	STARTUPINFOA startInfo;
	ZeroMemory( &startInfo, sizeof( startInfo ) );
	startInfo.cb = sizeof( STARTUPINFOA );
	
	PROCESS_INFORMATION procInfo;
	ZeroMemory( &procInfo, sizeof( procInfo ) );

	BOOL r = CreateProcessA
	(
		name	,
		"-unit"	,
		NULL	,
		NULL	,
		FALSE	,
		NORMAL_PRIORITY_CLASS,
		NULL	,
		NULL	,
		&startInfo,
		&procInfo
	);

	if ( r == FALSE )
	{
		printf( "!  unable to launch\n" );
		return;
	}
	
	for ( ;; )
	{
		DWORD exitCode = 0;

		r = GetExitCodeProcess( procInfo.hProcess, &exitCode );

		if ( exitCode == STILL_ACTIVE )
			// stall while we wait for this process to finish
			Sleep( 100 );
		else
		{
			// positive exitCode indicates a pass
			if ( exitCode ==  0 ) printf( ".  skipped\n" );
			else
			if ( exitCode == -1 ) printf( "!  failed!\n" );
			else
			if ( exitCode >   0 ) printf( ">  passed!\n" );
			break;
		}
	}

	CloseHandle( procInfo.hProcess );
	CloseHandle( procInfo.hThread );
}

int main( int argc, char **args )
{
	ShowWindow( GetConsoleWindow() , SW_MAXIMIZE);

	printf( "------------------------\n" );
	printf( "  LITE GAME TEST SUITE  \n" );
	printf( "------------------------\n" );
	printf( "\n" );

	// get the current exe path
	getPath( );
	SetCurrentDirectoryA( path );

	// 
	WIN32_FIND_DATAA findData;
	ZeroMemory( &findData, sizeof( findData ) );
	HANDLE find = FindFirstFileA( "*.exe", &findData );

	if ( find == INVALID_HANDLE_VALUE )
	{
		printf( "No tests found!\n" );
		return -1;
	}

	do
	{
		// check we dont run ourselves recursively
		if (! strMatch( findData.cFileName, strLastSlash( args[0] ) ) )
		{
			// 
			runTest( findData.cFileName );
			printf( "\n" );
		}
	}
	while ( FindNextFileA( find, &findData ) == TRUE );

	FindClose( find );

	// quit message
	printf( "\n" );
	printf( "press any key to close...\n" );
	getchar( );

	return 0;
}