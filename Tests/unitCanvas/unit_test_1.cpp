#include <SDL/SDL.h>
#include <math.h>

#include <libMut/mut.h>
#include <libUtility/source/lg_profile.h>
#include <libUtility/source/lg_image.h>
#include <libDrawCanvas/drawCanvas.h>

#pragma comment( lib, "sdl.lib" )

lg_cImage  ball;
lg_cCanvas screen;
SDL_Surface *sdl_screen = nullptr;

mut::vec3 rot;

float scale = 0.5f;

const int nPoints = 1024 * 4;
mut::vec3 pointa[ nPoints ];
mut::vec3 pointb[ nPoints ];

void rotate( mut::vec3 &v, mut::vec3 &t, float ax, float ay, float az )
{ 
    // Rotate about x axis
    float x2 = v.x;
    float y2 = v.y * cosf(ax) - v.z * sinf(ax);
    float z2 = v.y * sinf(ax) + v.z * cosf(ax);
 
    // Rotate about y axis
    float x3 = x2 * cos(ay) + z2 * sin(ay);
    float y3 = y2;
    float z3 =-x2 * sin(ay) + z2 * cos(ay);
 
    // Rotate about z axis
    t.x = x3 * cos(az) - y3 * sin(az);
    t.y = x3 * sin(az) + y3 * cos(az);
    t.z = z3;
}

void zSort( void )
{
	// while this may seem inefficient we must remember
	// that this will almost always be a sorted list anyway

    for ( int i=0; i<nPoints-1; )
    {
        // find depth values
        float za = pointb[i  ].z;
        float zb = pointb[i+1].z;
        
        if ( za > zb )
        {
            // swap the two elements
            mut::swap( pointb[i], pointb[i+1] );
            
            // bubble up to the top
            if ( i > 0 )
                i--;
        }
        else
            // move forward in list
            i++;
    }
}

bool onInit( void )
{
    if ( SDL_Init( SDL_INIT_VIDEO ) != 0 )
        return false;

    sdl_screen = SDL_SetVideoMode( 640, 480, 32, 0 );
    lg_assert( sdl_screen != nullptr );

	for ( int i=0; i<nPoints; i++ )
	{
		pointa[i].x = mut::rand::gaussf( );
		pointa[i].y = mut::rand::gaussf( );
		pointa[i].z = mut::rand::gaussf( );
	}

    return true;
}

bool onTick( void )
{
    SDL_Event e;
    while ( SDL_PollEvent( &e ) )
    {
        if ( e.type == SDL_QUIT )
            return false;
        if ( e.type == SDL_KEYDOWN )
            return false;
    }
    return true;
}

void drawBalls( mut::vec3 &v )
{
	float d = 1.0f + v.z;
	v.x = (v.x / d) * 128.f * scale;
	v.y = (v.y / d) * 128.f * scale;

	int x = (int)( v.x + 160.f);
	int y = (int)( v.y + 120.f);

    mut::vec2i dst( x - 12, y - 12 );
    screen.maskBlit( ball, dst, ball.getFrame( ) );
}

void update( float dt )
{
    const float dy = 0.014f;
	rot.x += sin( dt * 0.0031231f ) * dy;
	rot.y += cos( dt * 0.0024351f ) * dy;
	rot.z += cos( dt * 0.0006973f ) * dy;
}

extern
bool unit_test_1( void )
{
	lg_profile::init( );

    if (! onInit( ) )
        return false;
    
    if (! ball.load( "sprites/ball.bmp" ) )
        return false;
    ball.setMask( 0xff00ff );

    if (! lg_canvas::create( screen, 320, 240 ) )
        return false;

	screen.clear( 0x1f1f1f );

	float dt = 0.0f;

    while ( onTick( ) )
    {
		// update all of the balls
		update( dt );
		
		for ( int i=0; i<nPoints; i++ )
			rotate( pointa[i], pointb[i], rot.x, rot.y, rot.z );

		zSort( );
		for ( int i=0; i<nPoints-1; i++ )
            lg_assert( pointb[i].z <= pointb[i+1].z );

		u64 cycles = 0;
		lg_profile::enter( );
		{
			// draw all of the balls
			for ( int i=nPoints-1; i>=0; --i )
				drawBalls( pointb[ i ] );
		}
		cycles = lg_profile::leave( );
		printf( "\rcpu cycles %012llu", cycles );

        // flip to the screen
        lg_canvas::blitx2( screen, (u32*)sdl_screen->pixels, 640, 480 );
        SDL_Flip( sdl_screen );
		
		screen.clear( 0x1f1f1f );
		dt += 0.0005f;

        scale = 0.5f+sinf( dt * 100.0f ) * 0.3f;
    }

    printf( "\n" );

    ball.unload( );
    lg_canvas::release( screen );

    return true;
}