#include <string.h>

#include "../common/unitTest.h"

#include <libUtility/source/lg_stringRef.h>

extern bool test_staticString( void )
{
	lg_cStringRef a( "my string 1" );
	lg_cStringRef b( "my string 2" );
	lg_cStringRef c( "my string 1" );
	lg_cStringRef d( "my string 3" );

	check( a.get( ) != NULL );
	check( b.get( ) != NULL );

	check( a == c );
	check( a != b );
	check( b != c );
	check( b != d );

	check( strcmp( a.get(), "my string 1" ) == 0 );
	check( strcmp( b.get(), "my string 2" ) == 0 );
	check( strcmp( d.get(), "my string 3" ) == 0 );

	return true;
}
