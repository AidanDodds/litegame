#include <stdio.h>
#include <Windows.h>

#include "../../Libs/libLiteGame/liteGame.h"

LONG CALLBACK _handler( PEXCEPTION_POINTERS exInfo )
{
	// signal a faliure
	exit( -1 );
	//TODO: modify the stack to execute the next test
	// continue
	return EXCEPTION_CONTINUE_EXECUTION;
}

extern bool test_staticString( void );
extern bool test_dynString   ( void );

int main( int argc, char **args )
{
	if ( IsDebuggerPresent( ) == FALSE )
	{
		if ( AddVectoredExceptionHandler( 0, _handler ) == 0 )
			printf( "failed to setup testing...\n" );
	}
    
    if (! lg_liteGame::start( ) )
        return false;

	// run tests
	test_staticString( );
	test_dynString   ( );

    lg_liteGame::stop( );

	// return success
	return 1;
}
