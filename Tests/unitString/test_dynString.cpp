#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "../common/unitTest.h"
#include <libUtility/source/lg_dynString.h>

static char testBuf[ 2048 ] = { 0 };

// fill the test buffer with garbage chars
void fillTestBuf( void )
{
	int len = rand( ) % sizeof( testBuf );
	for ( int i=0; i<len-1; i++ )
	{
		int v = 0;
		do { v = rand() & 0x7f; } while ( v < ' ' );
		testBuf[i] = v;
	}
	testBuf[ len ] = '\0'; 
}

void test_ctor( void )
{
	printf( "  ." );
	for ( int i=0; i<40000; i++ )
	{
		{
			fillTestBuf( );
			lg_cDynString str( testBuf );

			lg_assert( strcmp( str.cstr( ), testBuf ) == 0 );
		}
		if ( i % 6000 == 0 )
			printf( "." );
	}
}

void test_ctor_2( void )
{
	printf( "  ." );
	lg_cDynString str;
	lg_assert( str.getLength() == 0 );
}

void test_len( void )
{
	printf( "  ." );
	for ( int i=0; i<40000; i++ )
	{
		{
			fillTestBuf( );
			lg_cDynString str( testBuf );

			lg_assert( strlen( testBuf ) == str.getLength( ) );
		}
		if ( i % 6000 == 0 )
			printf( "." );
	}
}

void test_append( void )
{
	printf( "  ." );
	for ( int i=0; i<40000; i++ )
	{
		int len = 0;

		fillTestBuf( );
		len += strlen( testBuf );

		lg_cDynString str( testBuf );

		fillTestBuf( );
		len += strlen( testBuf );
		// make the actual append
		str.append( testBuf );
		// check length matches
		lg_assert( strlen( str.cstr( ) ) == len )
		// check for terminal
		lg_assert( str[ len ] == '\0' )
		// check terminal wont appear earlier
		for ( int j=0; j<len; j++ )
			lg_assert( str[j] != '\0' )
		// check end part is intact
		lg_assert( strcmp( str.cstr()+(len-strlen( testBuf )), testBuf ) == 0 );

		if ( i % 6000 == 0 )
			printf( "." );
	}
}

void test_append_2( void )
{
	// test append more
	printf( "  ." );
	for ( int i=0; i<10000; i++ )
	{
		lg_cDynString str;
		lg_assert( str.getLength( ) == 0 )

		for ( int j=0; j<(rand()%20); j++ )
		{
			fillTestBuf( );
			str.append( testBuf );
		}

		if ( i % 6000 == 0 )
			printf( "." );
	}
}

// 
void test_rand_access( void )
{
	printf( "  ." );
	for ( int i=0; i<10000; i++ )
	{
		{
			lg_cDynString string;
			lg_assert( string.getLength( ) == 0 );
		
			int index = rand() % 1024;

			string[ index ] = 'a';

			lg_assert( string.getLength() == (index+1) );

			lg_assert( ((char*)string) != 0 );
			lg_assert( string.cstr()[ index + 1 ] == '\0' );

			for ( int j=0; j<index; j++ )
				string[ j ] = '_';
			
			lg_assert( string.getLength() == (index+1) );

			if ( index > 0 ) // no div by zero
			{
				for ( int j=0; j<(rand()%1000); j++ )
				{
					int rindex = rand() % index;
					string[ rindex ] = 'W';
					lg_assert( string.getLength() == (index+1) );
				}
			}

			lg_assert( string.cstr() != 0 );
			int length = string.getLength();
			lg_assert( length == (index+1) );
			
			lg_assert( string.cstr()[ index + 1 ] == '\0' )
		}

		if ( i % 6000 == 0 )
			printf( "." );
	}
}

// 
void test_rand_access_2( void )
{
	// 9th his this fails
	printf( "  ." );
	for ( int i=0; i<10000; i++ )
	{
		lg_cDynString string;
		lg_assert( string.getLength( ) == 0 );
		int max = 0;

		for ( int j=0; j<100; j++ )
		{
			int index = rand() % 1024;
			if ( index > max )
				max = index;

			string[ index ] = 'Z';			
		}

		lg_assert( string.getLength() == max+1 );

		if ( i % 6000 == 0 )
			printf( "." );
	}
}

// 
extern bool test_dynString( void )
{
	test_ctor         ( ); putchar( '\n' );
	test_ctor_2       ( ); putchar( '\n' );
	test_len          ( ); putchar( '\n' );
	test_append       ( ); putchar( '\n' );
	test_append_2     ( ); putchar( '\n' );
	test_rand_access  ( ); putchar( '\n' );
	test_rand_access_2( ); putchar( '\n' );

	putchar( '\n' );
	return true;
}
