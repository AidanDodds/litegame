#include "../common/unitTest.h"

#include "libLiteGame/liteGame.h"
#include "libDrawCanvas/drawCanvas.h"

extern bool test_static( void )
{
	lg_cCanvas canvas;
    lg_cSprite sprite;

    if (! lg_sprite::create( lg_cStringRef( "ninja" ), sprite ) )
        return false;
    
    if (! sprite.play( lg_cStringRef( "run" ), 0 ) )
        return false;

	if (! lg_canvas::create( canvas, 320, 240 ) )
		return false;

	lg_sSpriteDrawInfo drawInfo;

    for ( int i=0; i<32; i++ )
	{
		sprite.getDrawInfo( drawInfo );
		
		canvas.fastBlit( *drawInfo.image, mut::vec2i( 120, 240 ), drawInfo.frame );

        sprite.tick( );
	}

	lg_canvas::release( canvas );
	lg_sprite::release( sprite );

    return true;
}
