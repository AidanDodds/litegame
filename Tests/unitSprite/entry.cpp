#include <stdio.h>

#include "libLiteGame/liteGame.h"

#ifdef _MSC_VER
#include <Windows.h>

LONG CALLBACK _handler(  PEXCEPTION_POINTERS exInfo )
{
	// signal a faliure
	exit( -1 );
	// continue
	return EXCEPTION_CONTINUE_EXECUTION;
}
#endif

extern bool test_static( void );

int main( int argc, char **args )
{
	if ( IsDebuggerPresent( ) == FALSE )
	{
		if ( AddVectoredExceptionHandler( 0, _handler ) == 0 )
			printf( "failed to setup testing...\n" );
	}

	bool status = true;

	// init all modules (sprite module)
	if (! lg_liteGame::start() )
		return -1;

	// run unit test 1
	status &= test_static( );

    // kill lite game
    lg_liteGame::stop();

	// return fail if any test fails
	if ( status )	return  1;
	else			return -1;
}