#include <libUtility/source/lg_types.h>
#include <libLiteGame/source/lg_map2d.h>

#define _SDL_main_h
#include <SDL/SDL.h>
#include <math.h>

void plot( SDL_Surface *screen, int x, int y, int c )
{
	if ( x<0 || x>=screen->w || y<0 || y>=screen->h )
		return;

	u32 *p = (u32*) screen->pixels;

	p[ x + y * screen->w ] = c;
}

void doRaycast( lg_map2d::info_t &map, SDL_Surface *screen )
{
	static int x=4, y=4;

	int ts = map.tileSize;

	int _x, _y;
	int b = SDL_GetMouseState( &_x, &_y );
	
	int numKeys = 0;
	Uint8 *keys = SDL_GetKeyState( &numKeys );
	
	float mx = ((float)x) / ((float)ts);
	float my = ((float)y) / ((float)ts);

	bool debug = false;

	if ( keys[ SDLK_SPACE ] == 0 )
	{
		if ( b&SDL_BUTTON( 1 ) )
		{
			// raycast from here
			x = _x;
			y = _y;
		}

		if ( b&SDL_BUTTON( 3 ) )
			// debug this tile
			debug = true;
	}
	else
	// add or remove bits of the wall
	{
		if ( b & SDL_BUTTON( 1 ) )
			lg_map2d::setWall( map, mut::vec2i( _x/ts, _y/ts ), true );

		if ( b & SDL_BUTTON( 3 ) )
			lg_map2d::setWall( map, mut::vec2i( _x/ts, _y/ts ), false );

		// update the collision info
		lg_map2d::preprocess( map );
	}

	// tile based hit
	for ( int y=0; y<map.h; y++ )
	{
		for ( int x=0; x<map.w; x++ )
		{
			if ( lg_map2d::isWall( map, mut::vec2i( x, y ) ) )
				continue;

			// debug this tile
			if ( x==(int)mx && y==(int)my && debug )
			{
				__asm { int 3 };
				debug = false;
			}

			SDL_Rect dst = { x*ts, y*ts, ts, ts };

			if ( lg_map2d::lineOfSight( map, mut::vec2( mx, my ), mut::vec2( .5f + (float)x, .5f + (float)y ) ) )
				SDL_FillRect( screen, &dst, 0x801010 );
		}
	}
	
	lg_map2d::rayHit_t hit;

	// exact hit location
	for ( float i=0.0f; i<6.28318530718f; i+=0.01f )
	{
		float tx = mx + sinf( i ) * 16.0f;
		float ty = my + cosf( i ) * 16.0f;

		lg_map2d::raycast( map, mut::vec2( mx, my ), mut::vec2( tx, ty ), hit );

		plot( screen, hit.hx*ts, hit.hy*ts, 0x00FF00 );
	}

}