#include <libUtility/source/lg_types.h>
#include <libLiteGame/source/lg_map2d.h>

#define _SDL_main_h
#include <SDL/SDL.h>

void doCollide( lg_map2d::info_t &map, SDL_Surface *screen )
{
	int ts = map.tileSize;

	int x, y;
	int b = SDL_GetMouseState( &x, &y );

	// add or remove bits of the wall
	{
		if ( b & SDL_BUTTON( 1 ) )
			lg_map2d::setWall( map, mut::vec2i( x/ts, y/ts ), true );

		if ( b & SDL_BUTTON( 3 ) )
			lg_map2d::setWall( map, mut::vec2i( x/ts, y/ts ), false );

		// update the collision info
		lg_map2d::preprocess( map );
	}
			
	lg_map2d::aabb_t aabb = 
	    { (float)x-15.0f, (float)y-15.0f, (float)x+15.0f, (float)y+15.0f };

	// draw aabb before
	SDL_Rect r1 =
	{
		(int)aabb.x1,
		(int)aabb.y1,
		(int)aabb.x2-(int)aabb.x1,
		(int)aabb.y2-(int)aabb.y1
	};
	SDL_FillRect( screen, &r1, 0xc00000 );

	lg_map2d::collide( map, aabb );
	aabb.doResolve( );

	// draw aabb after
	SDL_Rect r2 =
	{
		(int)aabb.x1,
		(int)aabb.y1,
		(int)aabb.x2-(int)aabb.x1,
		(int)aabb.y2-(int)aabb.y1
	};
	SDL_FillRect( screen, &r2, 0x12a074 );
}