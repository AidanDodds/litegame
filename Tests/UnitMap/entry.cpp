#include <libUtility/source/lg_types.h>
#include <libLiteGame/source/lg_map2d.h>

#include "../../Libs/libLiteGame/liteGame.h"

#define _SDL_main_h
#include <SDL/SDL.h>

#pragma comment( lib, "SDL.lib" )

static SDL_Surface *screen = NULL;
static lg_map2d::info_t map;

void app_init( void )
{
	if ( SDL_Init( SDL_INIT_VIDEO ) != 0 )
		exit( -1 );
	screen = SDL_SetVideoMode( 320, 320, 32, 0 );
	if ( screen == NULL )
		exit( -2 );
}

bool app_tick( void )
{
	static bool active = true;

	SDL_Event event;
	while ( SDL_PollEvent( &event ) )
	{
		if ( event.type == SDL_QUIT )
			active = false;

		if ( event.type == SDL_KEYDOWN )
		{
			switch ( event.key.keysym.sym )
			{
			case ( SDLK_ESCAPE ):
				active = false;
				break;
			}
		}
	}
	return active;
}

void drawMap( void )
{
	for ( u32 i=0; i<map.w*map.h; i++ )
	{
		u32 x = i % map.w;
		u32 y = i / map.w;

		SDL_Rect r = { x*8, y*8, 8, 8 };

		if ( lg_map2d::isWall( map, mut::vec2i( x, y ) ) )
			SDL_FillRect( screen, &r, 0x3585a );
		else
			SDL_FillRect( screen, &r, 0x1f1f1f );
	}
}

bool isUnitTest( int argc, char **args )
{
	if ( argc == 1 )
		if ( strcmp( args[0], "-unit" ) == 0 )
			return true;
	return false;
}

extern void doCollide( lg_map2d::info_t &map, SDL_Surface *screen );
extern void doRaycast( lg_map2d::info_t &map, SDL_Surface *screen );

int main( int argc, char **args )
{
	// bomb out if running as a unit test
	if ( isUnitTest( argc, args ) )
		return 0;

	app_init( );
    if (! lg_liteGame::start( ) )
        return false;

	int ts = 8;
	lg_map2d::create( map, mut::vec2i( screen->w/ts, screen->h/ts ), ts );

	for ( int i=0; i<map.w; i++ )
	{
		lg_map2d::setWall( map, mut::vec2i( i,       0 ),  true );
		lg_map2d::setWall( map, mut::vec2i( i, map.h-1 ), true );
	}

	for ( int i=0; i<map.h; i++ )
	{
		lg_map2d::setWall( map, mut::vec2i(       0, i ), true );
		lg_map2d::setWall( map, mut::vec2i( map.w-1, i ), true );
	}

	while ( app_tick( ) )
	{
		drawMap( );
		
		doCollide( map, screen );
//		doRaycast( map, screen );

		SDL_Flip( screen );
	}

	lg_map2d::release( map );
    
    lg_liteGame::stop( );
	return 0;
}