<shader_vertex>

	uniform mat4 uProj;
	uniform mat4 uView;

	attribute vec3 vPosition;
	attribute vec3 vColour;
	attribute vec2 vTexCoord;
	
	varying vec3 col;
	varying vec2 uv;

	void main( void )
	{
		gl_Position =  ( vec4( vPosition, 1.f ) * uView ) * uProj;
		col = vColour;
		uv  = vTexCoord;
	}

</shader_vertex>
<shader_fragment>

	uniform sampler2D texDiff;
	uniform sampler2D texNorm;
	uniform float time;
	
	varying vec3 col;
	varying vec2 uv;

	void main( void )
	{
		float s = sin( time );
		float c = cos( time );
	
		vec4 ld = normalize( vec4( s, c, .5f, 0.f ) );
		vec4 nr = normalize( texture( texNorm, uv ) - vec4( .5f, .5f, .5f, 0.f ) );
	
		float lc = clamp( dot( ld, nr ), 0.f, 1.f );
	
		gl_FragColor = texture( texDiff, uv ) * lc;
	}

</shader_fragment>
