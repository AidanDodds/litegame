# mat4 * vec4 = column order
# vec4 * mat4 = row order

<shader_vertex>
	
	uniform mat4 proj;
	uniform mat4 view;
	uniform vec3 eye;
	
	attribute vec3 vPosition;
	attribute vec2 vTexCoord;
	
	varying vec2 uv;
	varying vec3 eyevec;
	
	void main( void )
	{
		eyevec = vPosition - eye;
	
		gl_Position = ( vec4( vPosition, 1.f ) * view ) * proj;
		uv  = vTexCoord;
	}

</shader_vertex>
<shader_fragment>

	uniform sampler2D texDiff;
	uniform sampler2D texBump;
	
	varying vec2 uv;
	varying vec3 eyevec;

	void main( void )
	{
		float depth = 1.f - texture( texBump, uv ).x;
		
		vec3 n = normalize( eyevec );
		
		vec2 offs  = (n.xy / n.z) * depth;
		
		gl_FragColor = texture( texDiff, uv );
	}

</shader_fragment>
