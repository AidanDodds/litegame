#include "framework.h"
#include "tests.h"

using namespace framework;

static mut::vec2 current1;
static mut::vec2 current2;

void cTest10::init( void )
{
    setTitle( "edge box clipping" );
    setNumPoints( 4 );
}

void cTest10::tick( void ) 
{
    // find the box
    mut::box2 box( mut::minv( point[2], point[3] ), mut::maxv( point[2], point[3] ) );

    draw::colour( 0x5890d0 );
    draw::line( point[0], point[1] );

    mut::edge2 edge( point[0], point[1] );

    mut::edge2 clipped;
    if ( mut::clip( box, edge, clipped ) )
    {
        draw::colour( 0xffffff );
        draw::line( clipped.a, clipped.b );

        draw::circle( mut::circle2( clipped.a, 8 ) );
        draw::circle( mut::circle2( clipped.b, 8 ) );

        draw::colour( 0xff0000 );
    }
    else
        draw::colour( 0x5890d0 );
        
    draw::rect( box );
}
