#include "framework.h"
#include "tests.h"

using namespace framework;

static mut::vec2 current1;
static mut::vec2 current2;

void cTest5::init( void )
{
    setTitle( "circular interpolation test" );
    setNumPoints( 2 );

    current1 = mut::vec2( 0.f, 64.0f );
    current2 = mut::vec2( 0.f, 78.0f );
}

void cTest5::tick( void ) 
{
    mut::vec2 center  = point[0];
    mut::vec2 target  = point[1] - center;

    draw::arrow( center, center+target , 8 );

    mut::vec2 nxt1 = mut::clerp( current1, target, 0.005f );
    current1 = nxt1;
    
    mut::vec2 nxt2 = mut::cnlerp( current2, target, 0.01f );
    current2 = nxt2;
    
    draw::colour( 0x8fdfff );
    draw::arrow( center, center+current1, 8 );

    draw::colour( 0xdfffbf );
    draw::arrow( center, center+current2, 8 );
}
