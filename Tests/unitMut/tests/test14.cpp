#include "framework.h"
#include "tests.h"

using namespace framework;

void cTest14::init( void )
{
    setTitle( "circle circle intersection" );
    setNumPoints( 4 );
}

void cTest14::tick( void ) 
{
    mut::circle2 circ1( point[0], mut::distance( point[0], point[1] ) );
    mut::circle2 circ2( point[2], mut::distance( point[2], point[3] ) );
    
    draw::colour( 0x5890d0 );
    draw::circle( circ1 );
    draw::circle( circ2 );

    mut::vec2 i1, i2;
    if ( mut::intersect( circ1, circ2, i1, i2 ) )
    {
        draw::colour( 0xffffff );
        draw::circle( mut::circle2( i1, 12.f ) );
        draw::circle( mut::circle2( i2, 12.f ) );
    }
}