#include "framework.h"
#include "tests.h"

using namespace framework;

static mut::vec2 current1;
static mut::vec2 current2;

void cTest6::init( void )
{
    setTitle( "inside triangle test" );
    setNumPoints( 4 );
}

void cTest6::tick( void ) 
{
    if ( mut::inside( point, point[3] ) )
        draw::colour( 0xFF0000 );
    else
        draw::colour( 0x5890d0 );


    draw::line( point[0], point[1] );
    draw::line( point[1], point[2] );
    draw::line( point[2], point[0] );
}
