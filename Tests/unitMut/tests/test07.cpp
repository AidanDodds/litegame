#include "framework.h"
#include "tests.h"

using namespace framework;

static mut::vec2 current1;
static mut::vec2 current2;

void cTest7::init( void )
{
    setTitle( "point plane projection" );
    setNumPoints( 3 );
}

void cTest7::tick( void ) 
{
    mut::vec2 dif = normalize( point[1] - point[0] );
    mut::plane2 plane( point[0], point[1] );
    mut::vec2 m = (point[0] + point[1]) * .5f;
    draw::colour( 0xff0000 );
    draw::line( m, m + plane.normal * 8.f );
    
    draw::colour( 0x5890d0 );
    draw::line( point[0], point[1] );

    mut::vec2 proj = mut::project( plane, point[2] );
    
    draw::colour( 0xffffff );
    draw::arrow( point[2], proj, 8 );

    draw::colour( 0x5890d0 );
    draw::line( point[0], point[0]-dif*640 );
    draw::line( point[1], point[1]+dif*640 );
}
