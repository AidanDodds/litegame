#include <stdio.h>

#include "../../Libs/libLiteGame/liteGame.h"

#ifdef _MSC_VER
#include <Windows.h>

LONG CALLBACK _handler(  PEXCEPTION_POINTERS exInfo )
{
	// signal a faliure
	exit( -1 );
	//TODO: modify the stack to execute the next test
	// continue
	return EXCEPTION_CONTINUE_EXECUTION;
}
#endif

int main( int argc, char **args )
{
	//
	if ( IsDebuggerPresent( ) == FALSE )
	{
		if ( AddVectoredExceptionHandler( 0, _handler ) == 0 )
			printf( "failed to setup testing...\n" );
	}

    if (! lg_liteGame::start( ) )
        return false;
	bool status = true;
    


    lg_liteGame::stop( );

	// return fail if any test fails
	if ( status )	return  1;
	else			return -1;
}