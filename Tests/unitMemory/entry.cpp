#include <stdio.h>

#include "../../Libs/libLiteGame/liteGame.h"

#ifdef _MSC_VER
#include <Windows.h>

LONG CALLBACK _handler(  PEXCEPTION_POINTERS exInfo )
{
	// signal a faliure
	exit( -1 );
	//TODO: modify the stack to execute the next test
	// continue
	return EXCEPTION_CONTINUE_EXECUTION;
}
#endif

extern bool test_memory     ( void );
extern bool test_resource   ( void );
extern bool test_stack      ( void );

int main( int argc, char **args )
{
	//
	if ( IsDebuggerPresent( ) == FALSE )
	{
		if ( AddVectoredExceptionHandler( 0, _handler ) == 0 )
			printf( "failed to setup testing...\n" );
	}

    if (! lg_liteGame::start( ) )
        return false;

	bool status = true;
    
	printf( "test1...\n" );
	status &= test_memory( );
	printf( "test1...\n" );
	status &= test_resource( );
	printf( "test1...\n" );
	status &= test_stack( );

    lg_liteGame::stop( );

	// return fail if any test fails
	if ( status )	return  1;
	else			return -1;
}