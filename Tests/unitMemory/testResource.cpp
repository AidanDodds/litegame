#include "../common/unitTest.h"

#include "../../Libs/libUtility/source/lg_types.h"
#include "../../Libs/libUtility/source/lg_memory.h"
#include "../../Libs/libUtility/source/lg_resource.h"

#include "../../Libs/libMut/mut.h"

//
// dummy type a
//

struct sTypeA
{
    char name[ 32 ];
    int  data;
};
typedef util::lg_cResource<sTypeA, 1024> res_sTypeA;
sTypeA *newTypeA( void )
{
    sTypeA *t = (sTypeA*) lg_memory::inst().memAlloc( sizeof( sTypeA ), false );
    check( t != nullptr );
    return t;
}
void freeTypeA( sTypeA *a )
{
    lg_memory::inst().memRelease( (void*) a );
}

//
// dummy type b
//

struct sTypeB
{
    char name[ 32 ];
    int  data;
};
typedef util::lg_cResource<sTypeB, 1024> res_sTypeB;
sTypeB *newTypeB( void )
{
    sTypeB *t = (sTypeB*) lg_memory::inst().memAlloc( sizeof( sTypeB ), false );
    check( t != nullptr );
    return t;
}
void freeTypeB( sTypeB *a )
{
    lg_memory::inst().memRelease( (void*) a );
}

// 
extern bool test_resource( void )
{
    lg_memory &mem = lg_memory::inst();
    check(&mem != nullptr );

    sTypeA *ta1 = newTypeA( );
    sTypeA *ta2 = newTypeA( );
    sTypeA *ta3 = newTypeA( );
    
    sTypeB *tb1 = newTypeB( );
    sTypeB *tb2 = newTypeB( );
    sTypeB *tb3 = newTypeB( );

    util::resource_t ia1 = res_sTypeA::inst().add( ta1 );
    util::resource_t ia2 = res_sTypeA::inst().add( ta2 );
    util::resource_t ia3 = res_sTypeA::inst().add( ta3 );
    
    util::resource_t ib1 = res_sTypeB::inst().add( tb1 );
    util::resource_t ib2 = res_sTypeB::inst().add( tb2 );
    util::resource_t ib3 = res_sTypeB::inst().add( tb3 );

    check( ta1 = res_sTypeA::inst().find( ia1 ) );
    check( ta2 = res_sTypeA::inst().find( ia2 ) );
    check( ta3 = res_sTypeA::inst().find( ia3 ) );
    
    check( tb1 = res_sTypeB::inst().find( ib1 ) );
    check( tb2 = res_sTypeB::inst().find( ib2 ) );
    check( tb3 = res_sTypeB::inst().find( ib3 ) );

    return true;
}