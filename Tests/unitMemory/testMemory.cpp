#include "../common/unitTest.h"

#include "../../Libs/libUtility/source/lg_types.h"
#include "../../Libs/libUtility/source/lg_memory.h"

#include "../../Libs/libMut/mut.h"

static bool test1( void )
{
    lg_memory &mem = lg_memory::inst();
    check(&mem != nullptr );

    int nbUsed = mem.getStats( ).outgoing;

    // make a simple allocation
    byte *al1 = (byte*) mem.memAlloc( 16, true );
    check( al1 != nullptr );
    // check it is zero like we specified
    for ( int i=0; i<16; i++ )
    {
        check( al1[i] == 0 );
        // test if we can write to this memory
        al1[i] = 0xff;
    }
    mem.memRelease( al1 );

    check ( nbUsed == mem.getStats().outgoing );
    return true;
}

static bool test2( void )
{
    lg_memory &mem = lg_memory::inst();
    check(&mem != nullptr );
    int nbUsed = mem.getStats( ).outgoing;

    const int nAllocs = 512;
    const int nBytes  = 256;
    void *alloc[ nAllocs ];

    for ( int i=0; i<nAllocs; i++ )
    {
        alloc[i] = mem.memAlloc( nBytes, false );
        for ( int j=0; j<i; j++ )
        {
            check( alloc[i] != alloc[j] );
        }
        // test we can write to this memory
        for ( int k=0; k<nBytes; k++ )
        {
            byte *p = (byte*) alloc[i];
            p[k] = 0xff;
        }
    }

    for ( int i=0; i<nAllocs; i++ )
    {
        mem.memRelease( alloc[i] );
        alloc[i] = nullptr;
    }

    check( nbUsed == mem.getStats( ).outgoing );
    return true;
}

static bool test3( void )
{
    lg_memory &mem = lg_memory::inst();
    check(&mem != nullptr );
    int nbUsed = mem.getStats( ).outgoing;

    for ( int i=0; i<1024*8; i++ )
    {
        int nSize = mut::rand::randi( 1 + 1024 * 8 );
        byte *a = (byte*) mem.memAlloc( nSize, false );

        for ( int j=0; j<nSize; j++ )
            a[j] = mut::rand::randi( 0xff );

        mem.memRelease( a );
        a = nullptr;
    }
    
    check( nbUsed == mem.getStats( ).outgoing );
    return true;
}

extern bool test_memory( void )
{
    bool status  = test1( );
         status &= test2( );

    return status;
}