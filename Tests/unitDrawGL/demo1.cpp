#include "demo1.h"

#include "../../Libs/libDrawGL/Source/libDrawGL.h"
#include "../../Libs/libMut/mut.h"

#include <stdio.h>

struct sDemoData
{
    // shader handle
    shader_t  shader;
    // uniforms 
    uniform_t uform[4];
    // textures
    texture_t tex1;
    texture_t tex2;
    texture_t tex3;

    float time;
};

cDemo1::cDemo1( void )
    : data( nullptr )
{ }

bool cDemo1::start( void )
{
    lg_assert( data == nullptr );
    data = new sDemoData( );
    lg_assert( data != nullptr );
    
    // create a basic fragment shader
    cLibDrawGL &drawgl = cLibDrawGL::inst( );
    if (! drawgl.shader().loadAndCompile( "shaders/sh1.shader", data->shader ) )
    {
        getchar( );
        return false;
    }

    // find all shader uniforms
    if (! drawgl.shader().findUniforms
        ( data->shader, "uProj;uView;texDiff;texNorm;time;", data->uform, 4 ) )
        return false;
    
    // load in the textures
    if (! drawgl.texture().load( data->tex1, "textures/test512.bmp" ) )
        return false;
    
    if (! drawgl.texture().load( data->tex2, "textures/normal.bmp" ) )
        return false;

    if (! drawgl.texture().load( data->tex3, "textures/normal2.bmp" ) )
        return false;

    return true;
}

bool cDemo1::stop( void )
{
    return true;
}

bool cDemo1::tick( void )
{
    // grab manager objects
    cLibDrawGL  &drawgl  = cLibDrawGL::inst( );
    cManShader  &shader  = drawgl.shader( );
    cManTexture &texture = drawgl.texture( );

    if (! shader.bind( data->shader ) )
        return false;

    // setup view matrix
    sCamera camera;
    camera.setProject2d( 640, 480 );
    
    // bind this texture
    if (! texture.bind( data->tex1, 0 ) )
        return false;

    // bind this texture
    if (! texture.bind( data->tex2, 1 ) )
        return false;
    
    bool res = true;
    res &= shader.setUniform( data->uform[0], camera.projMatrix );
    res &= shader.setUniform( data->uform[1], camera.viewMatrix );
    res &= shader.setUniform( data->uform[2], 0 );
    res &= shader.setUniform( data->uform[3], 1 );
    res &= shader.setUniform( data->uform[4], data->time );
    
    drawgl.draw().quad
    (
        // position
        mut::vec2(  32.f,  32.f ), 
        // size
        mut::vec2( 128.f, 128.f ),
        // tex coords
        mut::vec2( 0.f, 0.f ), 
        mut::vec2( 1.f, 1.f )
    );

    // bind this texture
    if (! texture.bind( data->tex3, 1 ) )
        return false;
    
    drawgl.draw().quad
    (
        // position
        mut::vec2( 128.f, 96.f ), 
        // size
        mut::vec2( 128.f, 128.f ),
        // tex coords
        mut::vec2( 0.f, 0.f ), 
        mut::vec2( 1.f, 1.f )
    );

    // increment time
    float & time = data->time;
    time += 0.01f;
    if ( time > mut::pi2 )
        time -= mut::pi2;

    return true;
}
