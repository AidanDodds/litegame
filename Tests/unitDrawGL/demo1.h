#pragma once

// rendering subsystem
class cDemo1
{
    cDemo1( const cDemo1 & );
    cDemo1( void );

    // hidden data implementation
    struct sDemoData * data;

public:
    
    //
    // singleton interface
    //

    bool start( void );
    bool stop ( void );

    static cDemo1 & inst( void )
    {
        static cDemo1 instance;
        return instance;
    }

    //
    // instance methods
    //

    // per frame tick
    bool tick( void );

};