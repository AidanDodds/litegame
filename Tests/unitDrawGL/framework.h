#pragma once

// rendering subsystem
class cFramework
{
    cFramework( const cFramework & );
    cFramework( void );

    // hidden data implementation
    struct sFrameworkData *data;

public:

    //
    // singleton interface
    //

    bool start( void );
    bool stop ( void );

    static cFramework & inst( void )
    {
        static cFramework instance;
        return instance;
    }
    
    //
    // instance methods
    //

    bool willQuit( void );

    bool CreateGLWindow( int w, int h, bool fullscreen );

    void flip( void );

};