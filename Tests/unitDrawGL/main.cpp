#include "../../Libs/libDrawGL/Source/libDrawGL.h"

#include "framework.h"
#include "demo2.h"

#pragma comment( lib, "opengl32.lib" )

#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>

int main( void )
{
    cFramework &framework = cFramework::inst( );
    cLibDrawGL &render = cLibDrawGL::inst( );
    cDemo2 &demo = cDemo2::inst( );

    int num = 0;
    const Uint8 *keys = SDL_GetKeyboardState( &num );

    // fake exception handler
    for ( ; ; )
    {
        // init sdl framework
        if (! framework.start( ) )
            break;
        // create an opengl window
        if (! framework.CreateGLWindow( 640, 480, false ) )
            break;
    
        // init opengl framework
        if (! render.start( ) )
            break;

        if (! demo.start( ) )
            break;

        // main loop
        while (! framework.willQuit( ) )
        {
            render.draw().clear( );

            demo.tick( );
            
            int mx, my;
            SDL_GetRelativeMouseState( &mx, &my );

            float dx = keys[ SDL_SCANCODE_A ] - keys[ SDL_SCANCODE_D ];
            float dy = keys[ SDL_SCANCODE_SPACE ] - keys[ SDL_SCANCODE_LCTRL ];
            float dz = keys[ SDL_SCANCODE_W ] - keys[ SDL_SCANCODE_S ];

            const float sspeed = 0.005f;
            const float mspeed = 0.015f;

            demo.scroll( ((float)mx)*sspeed, ((float)my)*sspeed );
            demo.move  ( dx * mspeed, dy * mspeed, dz * mspeed );

            framework.flip( );
        }

        break;
    }

    demo.stop( );
    render.stop( );
    framework.stop( );

    return 0;
}