#include "demo2.h"

#include "../../Libs/libDrawGL/Source/libDrawGL.h"
#include "../../Libs/libMut/mut.h"

#include <stdio.h>

struct sDemoData
{
    // shader handle
    shader_t  shader;
    // uniforms 
    uniform_t uform[4];
    // textures
    texture_t tex1;
    texture_t tex2;

    sCamera camera;
};

const float p = 1.f;
const float n =-1.f;
const float z = 0.f;
    
mut::vec3 vData[] =
{    
    mut::vec3( n, n, n ), mut::vec3( p, n, n ),
    mut::vec3( n, p, n ), mut::vec3( p, p, n ),
    mut::vec3( n, n, p ), mut::vec3( p, n, p ),
    mut::vec3( n, p, p ), mut::vec3( p, p, p ),
    mut::vec3( n, n, n ), mut::vec3( n, n, p ),
    mut::vec3( p, n, n ), mut::vec3( p, n, p ),
    mut::vec3( n, p, n ), mut::vec3( n, p, p ),
    mut::vec3( p, p, n ), mut::vec3( p, p, p ),
    mut::vec3( n, p, n ), mut::vec3( n, p, p ),
    mut::vec3( n, n, n ), mut::vec3( n, n, p ),
    mut::vec3( p, p, n ), mut::vec3( p, p, p ),
    mut::vec3( p, n, n ), mut::vec3( p, n, p ),
};

mut::vec2 tData[] =
{
    mut::vec2( z, p ), mut::vec2( p, p ),
    mut::vec2( z, z ), mut::vec2( p, z ),
    mut::vec2( z, p ), mut::vec2( p, p ),
    mut::vec2( z, z ), mut::vec2( p, z ),
    mut::vec2( z, p ), mut::vec2( p, p ),
    mut::vec2( z, z ), mut::vec2( p, z ),
    mut::vec2( z, p ), mut::vec2( p, p ),
    mut::vec2( z, z ), mut::vec2( p, z ),
    mut::vec2( z, p ), mut::vec2( p, p ),
    mut::vec2( z, z ), mut::vec2( p, z ),
    mut::vec2( z, p ), mut::vec2( p, p ),
    mut::vec2( z, z ), mut::vec2( p, z ),
};

int iData[] =
{
     0,  1,  2,  1,  2,  3,
     4,  5,  6,  5,  6,  7,
     8,  9, 10,  9, 10, 11,
    12, 13, 14, 13, 14, 15,
    16, 17, 18, 17, 18, 19,
    20, 21, 22, 21, 22, 23
};

cVertexBuffer cube =
{
      0,   nullptr,
    4*6,   vData,
    6*6,   iData,
    4*6,   tData
};

cDemo2::cDemo2( void )
    : data( nullptr )
{ }

bool cDemo2::start( void )
{
    lg_assert( data == nullptr );
    data = new sDemoData( );
    lg_assert( data != nullptr );
    
    // create a basic fragment shader
    cLibDrawGL &drawgl = cLibDrawGL::inst( );
    if (! drawgl.shader().loadAndCompile( "shaders/sh2.shader", data->shader ) )
    {
        getchar( );
        return false;
    }
    
    bool res = true;

    // find all shader uniforms
    res &= drawgl.shader().findUniforms( data->shader, "texDiff;proj;view;", data->uform, 4 );
    
    // load in the textures
    res &= drawgl.texture().load( data->tex1, "textures/bumpy_a.bmp" );
    
    // set the perspective transform
    data->camera.setProject3d( 640.f / 480.f, mut::pi * .7f );
    data->camera.move( mut::vec3( 0, 0, -4.f ) );

    return true;
}

bool cDemo2::stop( void )
{
    return true;
}

bool cDemo2::tick( void )
{
    // grab manager objects
    cLibDrawGL  &drawgl  = cLibDrawGL::inst( );
    cManShader  &shader  = drawgl.shader( );
    cManTexture &texture = drawgl.texture( );

    drawgl.draw().depthBuffer( true );

    if (! shader.bind( data->shader ) )
        return false;
    
    data->camera.update();

    bool res = true;
    res &= shader.setUniform( data->uform[0], 0 );
    res &= shader.setUniform( data->uform[1], data->camera.projMatrix );
    res &= shader.setUniform( data->uform[2], data->camera.viewMatrix );
    
    // bind this texture
    res &= texture.bind( data->tex1, 0 );

    drawgl.draw().vbuffer( &cube );

    return true;
}

void cDemo2::scroll( float dx, float dy )
{
    data->camera.scroll( mut::vec2( dx, dy ) );
}

void cDemo2::move( float dx, float dy, float dz )
{
    data->camera.move( mut::vec3( dx, dy, dz ) );
}