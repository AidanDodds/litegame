#pragma once

// rendering subsystem
class cDemo2
{
    cDemo2( const cDemo2 & );
    cDemo2( void );

    // hidden data implementation
    struct sDemoData * data;

public:
    
    //
    // singleton interface
    //

    bool start( void );
    bool stop ( void );

    static cDemo2 & inst( void )
    {
        static cDemo2 instance;
        return instance;
    }

    //
    // instance methods
    //

    // per frame tick
    bool tick  ( void );

    void scroll( float dx, float dy );

    void move  ( float dx, float dy, float dz );

};